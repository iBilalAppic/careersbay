<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Admin</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Admin</a>
                        </li>
                        <li class="active">
                            <strong>Add Admin</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="ibox-tools">
                                    <span class="text-danger">( * ) Required</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" name="addadmin">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">First Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" placeholder="Enter First Name" id="firstname" name="firstname" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Last Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" placeholder="Enter Last Name" id="lastname" name="lastname" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Profile Image</label>
                                            <div class="col-sm-10">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" id="profilepic" name="profilepic">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="previewbox">
                                            <label class="col-sm-2 control-label">&nbsp;</label>
                                            <div class="col-sm-10">
                                                <img width="150" src="../assets/images/default-profile.png" id="preview">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Contact Number<span class="text-danger">*</span></label>
                                            <div class="col-sm-2">
                                                <select data-placeholder="ISD" id="isd" name="isd" class="chosen-select"></select>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" placeholder="Enter Contact Number" id="phone" name="phone" class="form-control" autocomplete="off">
                                                <span class="help-block m-b-none phone-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Email Id<span class="text-danger">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" placeholder="Enter Email Id" id="email" name="email" class="form-control" autocomplete="off">
                                                <span class="help-block m-b-none email-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <input type="hidden" id="adminid" name="adminid" value="<?php echo $adminid; ?>" class="form-control">
                                                <button type="reset" class="btn btn-white">Reset</button>
                                                <button type="submit" class="btn btn-primary">Add Admin</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <script>
            $(document).ready(function(){
                document.title = 'Add Admin';
                $('#admin-nav').addClass('active');
                $('#add-admin-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                dqs();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>