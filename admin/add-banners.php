<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- SUMMERNOTE -->
            <link href="../css/plugins/summernote/summernote.css" rel="stylesheet">
            <link href="../css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            .date {padding-right: 14px !important; padding-left: 14px !important;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Banners</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Banners</a>
                        </li>
                        <li class="active">
                            <strong>Add Banners</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="ibox-tools">
                                    <span class="text-danger">&nbsp;</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" name="addbanner">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Banner 1</div>
                                        <div class="panel-body">
                                            <div class="col-lg-4">
                                                <div class="form-group hide" id="bannerpreviewbox0">
                                                    <img width="150" src="" id="bannerpreview0">
                                                </div>
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" id="banner0" name="banner0" data-countnumber="0" onchange="addBannerImage(this);">
                                                    </span>
                                                </div>
                                                <span class="help-block m-b-none">The required dimensions is 2000 w x 1333 h</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <textarea name="bannertext0" id="bannertext0" class="summernote"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="appendmorebanner"></div>
                                    <button type="button" class="btn btn-w-m btn-success" onclick="addmorebanner();">Add More Banners +</button>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="hidden" id="bannercounter" name="bannercounter" value="1">
                                            <button type="reset" class="btn btn-white">Reset</button>
                                            <button type="submit" class="btn btn-primary">Add Banner</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Data picker -->
            <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <!-- Bootstrap Tooltip -->
            <script src="../js/plugins/bootstrap-tooltip/bootstrap-tooltip.js"></script>
            <!-- SUMMERNOTE -->
            <script src="../js/plugins/summernote/summernote.min.js"></script>
            <script>
            $(document).ready(function(){
                document.title = 'Add Banners';
                $('#banners-nav').addClass('active');
                $('#add-banners-nav').addClass('active');
                $('.summernote').summernote({
                    placeholder: 'Enter Banner Description'
                });
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>