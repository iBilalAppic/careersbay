<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- SUMMERNOTE -->
            <link href="../css/plugins/summernote/summernote.css" rel="stylesheet">
            <link href="../css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Job Title</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Job Title</a>
                        </li>
                        <li class="active">
                            <strong>Job Title</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="ibox-tools">
                                    <span class="text-danger">( * ) Required</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" name="addjd">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Job Title Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Enter Job Title Name" id="jdname" name="jdname" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Description<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <textarea name="description" id="description" class="summernote"></textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button type="reset" class="btn btn-white">Reset</button>
                                            <button type="submit" class="btn btn-primary">Add Job Title</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Bootstrap Tooltip -->
            <script src="../js/plugins/bootstrap-tooltip/bootstrap-tooltip.js"></script>
            <!-- SUMMERNOTE -->
            <script src="../js/plugins/summernote/summernote.min.js"></script>
            <script>
            $(document).ready(function(){
                document.title = 'Add JD';
                $('#jd-nav').addClass('active');
                $('#add-jd-nav').addClass('active');
                $('.summernote').summernote({
                    placeholder: 'Description'
                });
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>