<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- Data picker -->
            <link href="../css/plugins/datapicker/datepicker3.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            .date {padding-right: 14px !important; padding-left: 14px !important;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Job</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Job</a>
                        </li>
                        <li class="active">
                            <strong>Add Job</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="ibox-tools">
                                    <span class="text-danger">( * ) Required</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" name="addjob">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Job Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Enter Job Name" id="jobpostname" name="jobpostname" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Client<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <select data-placeholder="Select Client" id="clientid" name="clientid" class="form-control m-b chosen-select">
                                                <option selected="selected" disabled>Select Client</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                          Job Title<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <select data-placeholder="Select Job Title" id="jdid" name="jdid" class="form-control m-b chosen-select">
                                                <option selected="selected" disabled>Select JD</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="data_1">
                                        <label class="col-sm-2 control-label">Expire Date<span class="text-danger">*</span></label>
                                        <div class="col-sm-10 input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" placeholder="Expire Date" id="expiredate" name="expiredate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button type="reset" class="btn btn-white">Reset</button>
                                            <button type="submit" class="btn btn-primary">Add Job</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Data picker -->
            <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>
            <script>
            $(document).ready(function(){
                document.title = 'Add Job';
                $('#job-nav').addClass('active');
                $('#add-job-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                $('#data_1 .input-group.date').datepicker({
                    todayBtn: false,
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: false,
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    multidate: false,
                    startDate: new Date()
                });
                clientJd();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>