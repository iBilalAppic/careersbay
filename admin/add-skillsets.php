<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Skill Sets</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Skill Sets</a>
                        </li>
                        <li class="active">
                            <strong>Add Skill Sets</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="ibox-tools">
                                    <span class="text-danger">( * ) Required</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" name="addskillsets">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Skill Sets Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Enter Skill Sets Name" id="skillsetsname" name="skillsetsname" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button type="reset" class="btn btn-white">Reset</button>
                                            <button type="submit" class="btn btn-primary">Add Skill Sets</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <script>
            $(document).ready(function(){
                document.title = 'Add Skill Sets';
                $('#skillsets-nav').addClass('active');
                $('#add-skillsets-nav').addClass('active');
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>