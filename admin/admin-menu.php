        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                                <?php 
                                if($adminimg!=''){ ?>
                                <img src="../assets/images/profilepic/<?php echo $adminimg; ?>" class="img-circle"/>
                                <?php } else { ?>
                                <img src="../assets/images/default-profile.png" class="img-circle"/>
                                <?php }?>
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle">
                                <span class="clear">
                                    <span class="block m-t-xs">
                                        <strong class="font-bold"><?php echo $adminname; ?></strong>
                                    </span>
                                    <a href="profile.php" class="text-muted text-xs block"><i class="fa fa-gear"></i> Profile</a>
                                    <a href="logout.php" class="text-muted text-xs block"><i class="fa fa-sign-out"></i> Logout</a>
                                </span>
                            </a>
                        </div>
                        <div class="logo-element">
                            CB
                        </div>
                    </li>
                    <li id="dashboard-nav">
                        <a href="home.php"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></span></a>
                    </li>
                    <li id="admin-nav">
                        <a><i class="fa fa-users"></i> <span class="nav-label">Admin</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-admin-nav"><a href="add-admin.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Admin</span></a></li>
                            <li id="view-admin-nav"><a href="admin.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Admin</span></a></li>
                        </ul>
                    </li>
                    <li id="clients-nav">
                        <a><i class="fa fa-handshake-o"></i> <span class="nav-label">Clients</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-clients-nav"><a href="add-clients.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Clients</span></a></li>
                            <li id="view-clients-nav"><a href="clients.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Clients</span></a></li>
                        </ul>
                    </li>
                    <li id="jd-nav">
                        <a><i class="fa fa-clipboard"></i> <span class="nav-label">Job Title</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-jd-nav"><a href="add-jd.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Job Title</span></a></li>
                            <li id="view-jd-nav"><a href="jd.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Job Title</span></a></li>
                        </ul>
                    </li>
                    <li id="job-nav">
                        <a><i class="fa fa-briefcase"></i> <span class="nav-label">Job</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-job-nav"><a href="add-job.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Job</span></a></li>
                            <li id="view-job-nav"><a href="job.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Job</span></a></li>
                        </ul>
                    </li>
                    <li id="resumes-nav">
                        <a><i class="fa fa-file-text"></i> <span class="nav-label">Resumes</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-resumes-nav"><a href="add-resumes.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Resumes</span></a></li>
                            <li id="view-resumes-nav"><a href="resumes.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Resumes</span></a></li>
                            <li id="resumes-shortlist-nav"><a href="shortlist.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Shortlist Resume</span></span></a></li>
                        </ul>
                    </li>
                    <!-- <li id="designation-nav">
                        <a><i class="fa fa-leanpub"></i> <span class="nav-label">Designation</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-designation-nav"><a href="add-designation.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Designation</span></a></li>
                            <li id="view-designation-nav"><a href="designation.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Designation</span></a></li>
                        </ul>
                    </li> -->
                    <li id="industry-nav">
                        <a><i class="fa fa-leanpub"></i> <span class="nav-label">Industry</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-industry-nav"><a href="add-industry.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Industry</span></a></li>
                            <li id="view-industry-nav"><a href="industry.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Industry</span></a></li>
                        </ul>
                    </li>
                    <li id="qualification-nav">
                        <a><i class="fa fa-graduation-cap"></i> <span class="nav-label">Qualification</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-qualification-nav"><a href="add-qualification.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Qualification</span></a></li>
                            <li id="view-qualification-nav"><a href="qualification.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Qualification</span></a></li>
                        </ul>
                    </li>
                    <li id="skillsets-nav">
                        <a><i class="fa fa-book"></i> <span class="nav-label">Skill Sets</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-skillsets-nav"><a href="add-skillsets.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Skill Sets</span></a></li>
                            <li id="view-skillsets-nav"><a href="skillsets.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Skill Sets</span></a></li>
                        </ul>
                    </li>
                    <li id="banners-nav">
                        <a><i class="fa fa-file-image-o"></i> <span class="nav-label">Banners</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li id="add-banners-nav"><a href="add-banners.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">Add Banners</span></a></li>
                            <li id="view-banners-nav"><a href="banners.php"><i class="fa fa-circle-o-notch"></i> <span class="nav-label">View Banners</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>