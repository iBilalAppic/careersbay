/*================================================================================================
	File Name : admin-service.js
	Purpose   : It contains methods for careersbay admin panel
	Create By : Aavin Seth
	Created On: 02-12-2017
================================================================================================*/
/*================================================================================================
	Default initialization
================================================================================================*/

// $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });

/*================================================================================================
 No           : 1                                                                               
 Service Name : admin login
 Purpose      : for login to admin panel
 Create By    : Aavin Seth
 Created On   : 02-12-2017
================================================================================================*/

$("form[name='adminlogin']").validate({
	rules: {
		email: {
			required: true,
			email: true
		},
		password: "required"
	},
	messages: {
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		},
		password: "<span class='text-red'>Please provide a Password</span>"
	},
	submitHandler: function(form) {
		var email = $("#email").val();
		var password = $("#password").val();
		var captcha = grecaptcha.getResponse();
		var datapass = {"email":email,"password":password,"captcha":captcha};

		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=adminlogin',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('welcomenotification', 'yes');
					localStorage.setItem('adminname', value.name);
					if(value.activation=='1')
					{
						window.location.href = 'home.php';
					} else {
						window.location.href = 'profile.php';
						localStorage.setItem('activationpassword', 'yes');
					}
				}
				else
				{
					grecaptcha.reset();
					var msg = value.message;
					$(".alert #msg").html('');
					$(".alert").show();
					$(".alert #msg").append(msg);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 2
	Service Name : add client
	Purpose      : add clients in database
	Create By    : Aavin Seth
	Created On   : 02-12-2017
================================================================================================*/

$("form[name='addclient']").validate({
    rules: {
		clientname: {
			required: true,
			alphanumeric: true
		},
		clientlogo: "required",
		address1: "required",
		city: {
			required: true,
			lettersonly: true
		},
		// pincode: {
		// 	digits: true
		// },
		phone: {
			required: true,
			digits: true
		},
		email: {
			required: true,
			email: true
		},
		website: {
			url: true
		}
	},
	messages: {
		clientname: {
			required: "<span class='text-red'>Please provide a Client Name</span>",
			alphanumeric: "<span class='text-red'>Letters, Numbers and spaces only</span>"
		},
		clientlogo: "<span class='text-red'>Please provide client logo</span>",
		address1: "<span class='text-red'>Please provide Address 1</span>",
		city: {
			required: "<span class='text-red'>Please provide a City</span>",
			lettersonly: "<span class='text-red'>Letters and spaces only</span>"
		},
		// pincode: {
		// 	digits: "<span class='text-red'>Please provide a valid Pincode</span>"
		// },
		phone: {
			required: "<span class='text-red'>Please provide a Phone Number</span>",
			digits: "<span class='text-red'>Please provide a valid Phone Number</span>"
		},
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		},
		website: {
			url: "<span class='text-red'>Please provide a valid URL</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);
		$(".email-error").html('');
		$("#email").removeClass('error');
		$(".phone-error").html('');
		$("#phone").removeClass('error');
		$('#city-error').addClass('hide');

		var clientname = $("#clientname").val();
		var clientlogo = $("#clientpreview").attr('src');
		var clientlogoext = $('#clientlogoext').val()
		var address1 = $("#address1").val();
		var address2 = $("#address2").val();
		var country = $("#country").val();
		var city = $("#city").val();
		var isd = $("#isd").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		var website = $("#website").val();
		var establisheddate = $("#establisheddate").val();
		var countryflag = 1;
		var cityflag = 1;
		var datapass = {"clientname":clientname,"clientlogo":clientlogo,"address1":address1,"address2":address2,"country":country,"city":city,"isd":isd,"phone":phone,"email":email,"website":website,"establisheddate":establisheddate};
		console.log(datapass);

		if(country=='' || country==null)
		{
			$('#country-error').removeClass('hide');
			countryflag = 0;
		}

		if(city=='' || city==null)
		{
			$('#city-error').removeClass('hide');
			cityflag = 0;
		}

		if(cityflag==1 && countryflag==1)
		{
			if(clientlogoext=='jpg' || clientlogoext=='png' || clientlogoext=='jpeg' || clientlogoext=='JPG' || clientlogoext=='PNG' || clientlogoext=='JPEG')
			{
				$.ajax({
					type: 'POST',
					url: 'admin-service.php?servicename=addclient',
					datatype: 'JSON',
					contentType: 'application/json',
					async: true,
					data: JSON.stringify(datapass),
					/*beforeSend: function()
				    {
				        // $("#preloader").show();
				        $('#postloader').append('<div id="preloader"><div id="loaderbody"><div class="sk-spinner sk-spinner-rotating-plane"></div></div></div>');
				    },*/
					success:function(data)
					{
						var value = JSON.parse(data);
						if(value.status == 'success')
				        {
							$(".alert").hide();
							$(".email-error").hide();
							$(".phone-error").hide();

							localStorage.setItem('addclient', 'yes');
							localStorage.setItem('clientname', clientname);

							window.location.href = 'clients.php';
				        } else {
				        	// $("#preloader").hide();
				        	/*$("#postloader #loaderbody").fadeOut("slow");
		        			$("#postloader #preloader").delay(500).fadeOut();
		        			$('#postloader #preloader').remove();*/

							var message = value.message;

							if(value.email == 'failure'){
								var emailerror = value.emailerror;

								$(".email-error").html('');
								$("#email").addClass('error');
								$(".email-error").html(emailerror);
								$(".email-error").show();
							}

							if(value.phone == 'failure'){
								var phoneerror = value.phoneerror;

								$(".phone-error").html('');
								$("#phone").addClass('error');
								$(".phone-error").html(phoneerror);
								$(".phone-error").show();
							}

							$(".form-horizontal button[type=submit]").prop("disabled", false);
				        }
					}
				});
			}
			else
			{
				setTimeout(function() {
			        toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            positionClass: 'toast-bottom-right',
			            showMethod: 'slideDown',
			            timeOut: 4000
			        };
			        toastr.error('only', 'Only jpg, jpeg &amp; png are allowed');
			    }, 1300);
			}
		}
		else
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
		}
    }
});

$("#clientlogo").change(function(){
	clientLogo(this);
});

function clientLogo(input){
	$('#clientpreviewbox').show();
	$('#clientpreviewbox').removeClass('hide');
	var ext = $('#clientlogo').val().split(".").pop();
	$('#clientlogoext').val(ext);

	if(ext=='jpg' || ext=='png' || ext=='jpeg' || ext=='JPG' || ext=='PNG' || ext=='JPEG')
	{
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#clientpreview').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	} else {
		setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.error('only', 'Only jpg, jpeg &amp; png are allowed');
	    }, 1300);
	}
}

$('.fileinput-exists').click(function(){
	$('#previewbox').addClass('hide');
	$('#preview').attr('src', '');
});

/*================================================================================================
	No           : 3
	Service Name : viewclients
	Purpose      : displaying clients in datatable
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

function viewClients(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewclients',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.clients.length>0){
                    var i = new Array();
                    var sr = 1;
                    var clientslist;
                    for(var val=0;val<value.clients.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.clients[val].clientname;
                        i[val][2] = '<img src="../assets/images/profilepic/'+value.clients[val].profilepic+'" height="60px">';
                        i[val][3] = value.clients[val].phone;
                        i[val][4] = value.clients[val].email;
                        i[val][5] = value.clients[val].city;
                        if(value.clients[val].status == '1'){
                            i[val][6] = '<label class="switch switch-success"><input type="checkbox" name="clientstatus" data-clientname="'+value.clients[val].clientname+'" data-clientid="'+value.clients[val].clientid+'" value="0" checked="checked"><span></span></label>';
                        } else {
                            i[val][6] = '<label class="switch switch-success"><input type="checkbox" name="clientstatus" data-clientname="'+value.clients[val].clientname+'" data-clientid="'+value.clients[val].clientid+'" ><span></span></label>';
                        }
                        i[val][7] = '<div class="btn-group"><button type="button" data-clientname="'+value.clients[val].clientname+'" data-clientid="'+value.clients[val].clientid+'" data-viewtype="editable" data-toggle="modal" data-target="#editclientmodal" data-backdrop="static" data-keyboard="false" onclick="editClient(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-clientname="'+value.clients[val].clientname+'" data-clientid="'+value.clients[val].clientid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editclientmodal" data-backdrop="static" data-keyboard="false" onclick="editClient(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-clientname="'+value.clients[val].clientname+'" data-clientid="'+value.clients[val].clientid+'" onclick="deleteClient(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;

                        clientslist += '<option value="'+value.clients[val].clientid+'">'+value.clients[val].clientname+'</option>';
                    }
                    $("#clientslist,#filterclientslist").append(clientslist);
                    $('#clientslist,#filterclientslist').trigger("chosen:updated");
                }
            }

            $('#viewclients').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Clients Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 4
	Service Name : clientstatus
	Purpose      : change client status Enable/Disable
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

$(document).on("change", "input[name='clientstatus']", function(){
    var checkbox = $(this);
    var clientid = checkbox.data('clientid');
    var clientname = checkbox.data('clientname');
    var checked = checkbox.prop('checked');
    var status = '';
    var datapass = {"clientid":clientid,"checked":checked};

    $.ajax({
        type: 'POST',
        url : 'admin-service.php?servicename=updateclientstatus',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
        success: function(data){
        	var value = JSON.parse(data);
			if(value.status == 'success')
	        {
	        	if(checked == true){
	        		status = 'Enabled';
	        	} else {
	        		status = 'Disabled';
	        	}

	        	setTimeout(function() {
			        toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            positionClass: 'toast-bottom-right',
			            showMethod: 'slideDown',
			            timeOut: 4000
			        };
			        toastr.success('Client now '+status, clientname);
			    }, 1300);
	        } else {
	        	setTimeout(function() {
			        toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            positionClass: 'toast-bottom-right',
			            showMethod: 'slideDown',
			            timeOut: 4000
			        };
			        toastr.error('Please Try Again', clientname);
			    }, 1300);
	        }
        }
    });
});

/*================================================================================================
	No           : 5
	Service Name : editclient
	Purpose      : get client details in modal
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

function editClient(ec){
    var clientid = $(ec).data("clientid");
    var clientname = $(ec).data("clientname");
    var viewtype = $(ec).data("viewtype");
    var datapass = {"clientid":clientid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getclient',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var clientid = value.clientid;
			var clientname = value.clientname;
			var profilepic = value.profilepic;
			var address1 = value.address1;
			var address2 = value.address2;
			var city = value.city;
			var country = value.country;
			getCity(country,city);
			var isd = value.isd;
			var phone = value.phone;
			var email = value.email;
			var website = value.website;
			var establisheddate = value.establisheddate;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Client');
				$(".modal-content .text-danger").show();
				$('#data_1 .input-group').addClass('date');
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Client Detail');
				$(".modal-content .text-danger").hide();
				$('#data_1 .input-group').removeClass('date');
			}
			
			$("#clientid").val(clientid);
			$("#clientname").val(clientname);
			$("#oldclientlogo").val(profilepic);
			if(profilepic==''){
				$('#clientpreview').attr('src','../assets/images/default-profile.png');
			} else {
				$('#clientpreview').attr('src','../assets/images/profilepic/'+profilepic);
			}
			$("#address1").val(address1);
			$("#address2").val(address2);
			// $("#city").val(city);
			$("#country").val(country).trigger('chosen:updated');
			$("#isd").val(isd).trigger('chosen:updated');
			$("#oldisd").val(isd);
			$("#phone").val(phone);
			$("#oldphone").val(phone);
			$("#email").val(email);
			$("#oldemail").val(email);
			$("#website").val(website);
			if(establisheddate=='0000-00-00')
			{
				$("#establisheddate").val('');
			} else {
				$("#establisheddate").val(establisheddate);	
			}
		}
    });
}

/*================================================================================================
	No           : 6
	Service Name : saveclient
	Purpose      : save client edit detail in DB
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

$("form[name='saveclient']").validate({
    rules: {
		clientname: {
			required: true,
			alphanumeric: true
		},
		address1: "required",
		// city: {
		// 	required: true,
		// 	lettersonly: true
		// },
		// pincode: {
		// 	digits: true
		// },
		phone: {
			required: true,
			digits: true
		},
		email: {
			required: true,
			email: true
		},
		website: {
			url: true
		}
	},
	messages: {
		clientname: {
			required: "<span class='text-red'>Please provide a Client Name</span>",
			alphanumeric: "<span class='text-red'>Letters, Numbers and spaces only</span>"
		},
		address1: "<span class='text-red'>Please provide Address 1</span>",
		// city: {
		// 	required: "<span class='text-red'>Please provide a City</span>",
		// 	lettersonly: "<span class='text-red'>Letters and spaces only</span>"
		// },
		// pincode: {
		// 	digits: "<span class='text-red'>Please provide a valid Pincode</span>"
		// },
		phone: {
			required: "<span class='text-red'>Please provide a Phone Number</span>",
			digits: "<span class='text-red'>Please provide a valid Phone Number</span>"
		},
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		},
		website: {
			url: "<span class='text-red'>Please provide a valid URL</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);
		$(".email-error").html('');
		$("#email").removeClass('error');
		$(".phone-error").html('');
		$("#phone").removeClass('error');

		var clientid = $("#clientid").val();
		var clientname = $("#clientname").val();
		if($("#clientlogo").val()==''){
			var profilepic = '';
		} else {
			var profilepic = $("#clientpreview").attr('src');
		}
		var oldprofilepic = $("#oldclientlogo").val();
		var address1 = $("#address1").val();
		var address2 = $("#address2").val();
		var country = $("#country").val();
		var city = $("#city").val();
		var isd = $("#isd").val();
		var oldisd = $("#oldisd").val();
		var phone = $("#phone").val();
		var oldphone = $("#oldphone").val();
		var email = $("#email").val();
		var oldemail = $("#oldemail").val();
		var website = $("#website").val();
		var establisheddate = $("#establisheddate").val();
		var datapass = {"clientid":clientid,"clientname":clientname,"profilepic":profilepic,"oldprofilepic":oldprofilepic,"address1":address1,"address2":address2,"country":country,"city":city,"isd":isd,"oldisd":oldisd,"phone":phone,"oldphone":oldphone,"email":email,"oldemail":oldemail,"website":website,"establisheddate":establisheddate};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=saveclient',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					$(".alert").hide();
					$(".email-error").hide();
					$(".phone-error").hide();

					localStorage.setItem('saveclient', 'yes');
					localStorage.setItem('clientname', clientname);

					window.location.href = 'clients.php';
		        } else {
					var message = value.message;

					if(value.email == 'failure'){
						var emailerror = value.emailerror;

						$(".email-error").html('');
						$("#email").addClass('error');
						$(".email-error").html(emailerror);
						$(".email-error").show();
					}

					if(value.phone == 'failure'){
						var phoneerror = value.phoneerror;

						$(".phone-error").html('');
						$("#phone").addClass('error');
						$(".phone-error").html(phoneerror);
						$(".phone-error").show();
					}

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 7
	Service Name : deleteclient
	Purpose      : delete client
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

function deleteClient(dc){
	var clientid = $(dc).data('clientid');
	var clientname = $(dc).data('clientname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete client <span style="font-weight: bold;">'+clientname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"clientid":clientid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deleteclient',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Client <span style="font-weight: bold;">'+clientname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Client <span style="font-weight: bold;">'+clientname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        	// swal('Error', 'Driver '+clientname+' has not deleted', "error");
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 8
	Service Name : getprofile
	Purpose      : get admin details in edit profile
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

function getProfile(id){
    var adminid = id;
    var datapass = {"adminid":adminid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getprofile',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var adminid = value.adminid;
			var firstname = value.firstname;
			var lastname = value.lastname;
			var profilepic = value.profilepic;
			var isd = value.isd;
			var phone = value.phone;
			var email = value.email;
			
			$("#adminid").val(adminid);
			$("#firstname").val(firstname);
			$("#lastname").val(lastname);
			$("#oldprofilepic").val(profilepic);
			if(profilepic==''){
				$('#preview').attr('src','../assets/images/default-profile.png');
			} else {
				$('#preview').attr('src','../assets/images/profilepic/'+profilepic);
			}
			$("#isd").val(isd).trigger("chosen:updated");
			$("#oldisd").val(isd);
			$("#phone").val(phone);
			$("#oldphone").val(phone);
			$("#email").val(email);
			$("#oldemail").val(email);
		}
    });
}

/*================================================================================================
	No           : 9
	Service Name : saveprofile
	Purpose      : save client profile detail in DB
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

$("form[name='saveprofile']").validate({
    rules: {
		firstname: {
			required: true,
			lettersonly: true
		},
		lastname: {
			required: true,
			lettersonly: true
		},
		phone: {
			required: true,
			digits: true
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		firstname: {
			required: "<span class='text-red'>Please provide a First Name</span>",
			lettersonly: "<span class='text-red'>Letters, Numbers and spaces only</span>"
		},
		lastname: {
			required: "<span class='text-red'>Please provide a Last Name</span>",
			lettersonly: "<span class='text-red'>Letters, Numbers and spaces only</span>"
		},
		phone: {
			required: "<span class='text-red'>Please provide a Phone Number</span>",
			digits: "<span class='text-red'>Please enter a valid Phone Number</span>"
		},
		email: "<span class='text-red'>Please enter a Email</span>"
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);
		$(".email-error").html('');
		$("#email").removeClass('error');
		$(".phone-error").html('');
		$("#phone").removeClass('error');

		var adminid = $("#adminid").val();
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if($("#profilepic").val()==''){
			var profilepic = '';
		} else {
			var profilepic = $("#preview").attr('src');
		}
		var oldprofilepic = $("#oldprofilepic").val();
		var isd = $("#isd").val();
		var oldisd = $("#oldisd").val();
		var phone = $("#phone").val();
		var oldphone = $("#oldphone").val();
		var email = $("#email").val();
		var oldemail = $("#oldemail").val();
		var datapass = {"adminid":adminid,"firstname":firstname,"lastname":lastname,"profilepic":profilepic,"oldprofilepic":oldprofilepic,"isd":isd,"oldisd":oldisd,"phone":phone,"oldphone":oldphone,"email":email,"oldemail":oldemail};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=saveprofile',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					$(".alert").hide();
					$(".email-error").hide();
					$(".phone-error").hide();

					localStorage.setItem('saveprofile', 'yes');

					window.location.href = 'profile.php';
		        } else {
		        	$("#preloader").hide();
					var message = value.message;

					if(value.email == 'failure'){
						var emailerror = value.emailerror;

						$(".email-error").html('');
						$("#email").addClass('error');
						$(".email-error").html(emailerror);
						$(".email-error").show();
					}

					if(value.phone == 'failure'){
						var phoneerror = value.phoneerror;

						$(".phone-error").html('');
						$("#phone").addClass('error');
						$(".phone-error").html(phoneerror);
						$(".phone-error").show();
					}

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

$("#profilepic").change(function(){
	adminProfile(this);
});

function adminProfile(input){
	$('#previewbox').show();
	$('#previewbox').removeClass('hide');
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#preview').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$('.fileinput-exists').click(function(){
	$('#previewbox').addClass('hide');
	$('#preview').attr('src', '');
});

/*================================================================================================
	No           : 10
	Service Name : changepassword
	Purpose      : changepassword
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

$("form[name='changepassword']").validate({
    rules: {
		oldpassword: "required",
		newpassword: {
			required: true,
			minlength: 8
		},
		confirmpassword: "required"
	},
	messages: {
		oldpassword: "<span class='text-red'>Please enter Old Password</span>",
		newpassword: {
			required: "<span class='text-red'>Please enter New Password</span>",
			minlength: "<span class='text-red'>Password should be minimum 8 characters</span>"
		},
		confirmpassword: "<span class='text-red'>Please enter Confirm Password</span>"
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var adminid = $("#adminid").val();
		var oldpassword = $("#oldpassword").val();
		var newpassword = $("#newpassword").val();
		var confirmpassword = $("#confirmpassword").val();
		var redirectto = $("#redirectto").val();
		var datapass = {"adminid":adminid,"oldpassword":oldpassword,"newpassword":newpassword,"confirmpassword":confirmpassword};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=changepassword',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('changepassword', 'yes');

					if(redirectto=='profile'){
						window.location.href = 'profile.php';
					} else {
						window.location.href = 'home.php';
					}
		        } else {
		        	$("#preloader").hide();
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error(message, 'Please Try Again');
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 11
	Service Name : forgetpassword
	Purpose      : forget password for admin
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

$("form[name='forgetpassword']").validate({
	rules: {
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		}
	},
	submitHandler: function(form) {
		$("form[name='forgetpassword'] button[type=submit]").prop("disabled", true);

		var email = $("#email").val();
		var datapass = {"email":email};

		$.ajax({
			url: 'admin-service.php?servicename=forgetpassword',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('forgetpassword', 'yes');
					window.location.href = './';
				}
				else
				{
					var msg = value.message;
					$(".alert #msg").html('');
					$(".alert").show();
					$(".alert #msg").append(msg);

					$("form[name='forgetpassword'] button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 12
	Service Name : addbanner
	Purpose      : add banner with text
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

function addBannerImage(input){
	var id = $(input).attr('id');
	var countnumber = $(input).data('countnumber');
	
	$('#bannerpreviewbox'+countnumber).show();
	$('#bannerpreviewbox'+countnumber).removeClass('hide');
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#bannerpreview'+countnumber).attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

var countern = 2;
var counter = 1;
function addmorebanner(){
	if(counter < 3)
	{
		var html = '<div class="panel panel-default">'+
            '<div class="panel-heading">Banner '+countern+'</div>'+
            '<div class="panel-body">'+
                '<div class="col-lg-4">'+
                    '<div class="form-group hide" id="bannerpreviewbox'+counter+'">'+
                        '<img width="150" src="" id="bannerpreview'+counter+'">'+
                    '</div>'+
                    '<div class="fileinput fileinput-new input-group" data-provides="fileinput">'+
                        '<div class="form-control" data-trigger="fileinput">'+
                            '<i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>'+
                        '</div>'+
                        '<span class="input-group-addon btn btn-default btn-file">'+
                            '<span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>'+
                            '<input type="file" id="banner'+counter+'" name="banner'+counter+'" data-countnumber="'+counter+'" onchange="addBannerImage(this);">'+
                        '</span>'+
                    '</div>'+
                    '<span class="help-block m-b-none">Image must be exactly 2000 x 1333 in width &amp; height.</span>'+
                '</div>'+
                '<div class="col-lg-8">'+
                    '<textarea id="bannertext'+counter+'" name="bannertext'+counter+'" class="summernote"></textarea>'
                '</div>'+
            '</div>'+
        '</div>';
		counter++;
		countern++;
		$("#appendmorebanner").append(html);
		$("#bannercounter").val(counter);
		$('.summernote').summernote({
            placeholder: 'Enter Banner Description'
        });
	} else {
		setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.error('Only 3 can be add at a time','');
	    }, 1300);
	}
}

$("form[name='addbanner']").validate({
	rules: {
		banner0: {
			required: true
		}
	},
	messages: {
		banner0: {
			required: "<span class='text-red'>Please select Image</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var bannercounter = $("#bannercounter").val();
		var banners = [];
		for(var i=0;i<bannercounter;i++)
		{
			var image = $("#bannerpreview"+i).attr('src');
			var message = $("#bannertext"+i).val();
			
			banners.push({"image":image,"message":message});
		}
		var datapass = {"banners":banners};

		$.ajax({
			url: 'admin-service.php?servicename=addbanner',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('addbanner', 'yes');
					window.location.href = 'banners.php';
				}
				else
				{
					var msg = value.message;
					$(".alert #msg").html('');
					$(".alert").show();
					$(".alert #msg").append(msg);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 13
	Service Name : viewbanners
	Purpose      : displaying banners in datatable
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

function viewBanners(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewbanners',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.banners.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.banners.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = '<img src="../assets/images/banners/'+value.banners[val].image+'" height="60px" width="60px">';
                        i[val][2] = value.banners[val].message;
                        i[val][3] = '<div class="btn-group"><button type="button" data-bannerid="'+value.banners[val].bannerid+'" data-viewtype="editable" data-toggle="modal" data-target="#editbannersmodal" data-backdrop="static" data-keyboard="false" onclick="editBanners(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-bannerid="'+value.banners[val].bannerid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editbannersmodal" data-backdrop="static" data-keyboard="false" onclick="editBanners(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-bannerid="'+value.banners[val].bannerid+'" onclick="deleteBanners(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewbanners').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Clients Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 14
	Service Name : editbanner
	Purpose      : get banner details in modal
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

function editBanners(eb){
    var bannerid = $(eb).data("bannerid");
    var viewtype = $(eb).data("viewtype");
    var datapass = {"bannerid":bannerid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getbanner',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var bannerid = value.bannerid;
			var bannertext = value.bannertext;
			var image = value.image;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Banner');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Banner Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$("#bannerid").val(bannerid);
			// $("#bannertext").val(bannertext);
			$('#bannertext').summernote('code', bannertext);
			$("#oldbanner").val(image);
			if(image==''){
				$('#bannerpreview').attr('src','../assets/images/default-profile.png');
			} else {
				$('#bannerpreview').attr('src','../assets/images/banners/'+image);
			}
		}
    });
}

/*================================================================================================
	No           : 15
	Service Name : savebanner
	Purpose      : save banner edit detail in DB
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

$("form[name='savebanner']").validate({
    rules: {
		banner: {
			required: false
		}
	},
	messages: {
		banner: {
			required: "<span class='text-red'>Please select Image</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var bannerid = $("#bannerid").val();
		var bannertext = $("#bannertext").val();
		if($("#banner").val()==''){
			var banner = $("#banner").val();
		} else {
			var banner = $("#bannerpreview").attr('src');
		}
		var oldbanner = $("#oldbanner").val();
		var datapass = {"bannerid":bannerid,"bannertext":bannertext,"banner":banner,"oldbanner":oldbanner};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=savebanner',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					$(".alert").hide();
					$(".email-error").hide();
					$(".phone-error").hide();

					localStorage.setItem('savebanner', 'yes');

					window.location.href = 'banners.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

function saveBannerImage(input){
	$('#bannerpreviewbox').show();
	$('#bannerpreviewbox').removeClass('hide');
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#bannerpreview').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

/*================================================================================================
	No           : 16
	Service Name : deletebanner
	Purpose      : delete banner
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

function deleteBanners(db){
	var bannerid = $(db).data('bannerid');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete this banner!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"bannerid":bannerid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deletebanner',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Banner has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Banner has not deleted',
						type: 'error',
						html: true
					});
		        	// swal('Error', 'Driver '+clientname+' has not deleted', "error");
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 17
	Service Name : addjd
	Purpose      : add jd
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

$("form[name='addjd']").validate({
	rules: {
		jdname: {
			required: true
		}
	},
	messages: {
		jdname: {
			required: "<span class='text-red'>Please provide Job Title Name</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var jdname = $("#jdname").val();
		var description = $("#description").val();
		var datapass = {"jdname":jdname,"description":description};

		$.ajax({
			url: 'admin-service.php?servicename=addjd',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('addjd', 'yes');
					window.location.href = 'jd.php';
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 18
	Service Name : viewjd
	Purpose      : displaying jd in datatable
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

function viewJd(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewjd',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.jd.length>0){
                    var i = new Array();
                    var sr = 1;
                    var jdlist;
                    for(var val=0;val<value.jd.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.jd[val].jdname;
                        i[val][2] = '<div class="btn-group"><button type="button" data-jdid="'+value.jd[val].jdid+'" data-viewtype="editable" data-toggle="modal" data-target="#editjdmodal" data-backdrop="static" data-keyboard="false" onclick="editJd(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-jdid="'+value.jd[val].jdid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editjdmodal" data-backdrop="static" data-keyboard="false" onclick="editJd(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-jdid="'+value.jd[val].jdid+'" data-jdname="'+value.jd[val].jdname+'" onclick="deleteJd(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;

                        jdlist += '<option value="'+value.jd[val].jdid+'">'+value.jd[val].jdname+'</option>';
                    }
                    $("#filterjdlist").append(jdlist);
                    $('#filterjdlist').trigger("chosen:updated");
                }
            }

            $('#viewjd').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No JD Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 19
	Service Name : editjd
	Purpose      : get jd details in modal
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

function editJd(eb){
    var jdid = $(eb).data("jdid");
    var viewtype = $(eb).data("viewtype");
    var datapass = {"jdid":jdid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getjd',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var jdid = value.jdid;
			var jdname = value.jdname;
			var description = value.description;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Job Title');
				$(".modal-content .text-danger").show();
				$(".modal-content .modal-body #description").addClass('summernote');
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Job Title Detail');
				$(".modal-content .text-danger").hide();
				$(".modal-content .modal-body #description").removeClass('summernote');
			}
			
			$("#jdid").val(jdid);
			$("#jdname").val(jdname);
			$('#description').summernote('code', description);
		}
    });
}

/*================================================================================================
	No           : 20
	Service Name : savejd
	Purpose      : save jd edit detail in DB
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

$("form[name='savejd']").validate({
    rules: {
		jdname: {
			required: true
		}
	},
	messages: {
		jdname: {
			required: "<span class='text-red'>Please provide JD Name</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var jdid = $("#jdid").val();
		var jdname = $("#jdname").val();
		var description = $("#description").val();
		var datapass = {"jdid":jdid,"jdname":jdname,"description":description};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=savejd',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('savejd', 'yes');

					window.location.href = 'jd.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 21
	Service Name : deletejd
	Purpose      : delete jd
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

function deleteJd(dj){
	var jdid = $(dj).data('jdid');
	var jdname = $(dj).data('jdname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete Job Title <span style="font-weight: bold;">'+jdname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"jdid":jdid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deletejd',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Job Title <span style="font-weight: bold;">'+jdname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Job Title <span style="font-weight: bold;">'+jdname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 22
	Service Name : clientjd
	Purpose      : get client name and jd name
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

function clientJd(){
    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=clientjd',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		success: function(data)
		{
			var value = JSON.parse(data);
			var clientlist;
			var jdlist;

			if(value.status == 'success')
            {
                if(value.client.length>0){
                    for(var val=0;val<value.client.length;val++)
                    {
                        clientlist += '<option value="'+value.client[val].clientid+'">'+value.client[val].clientname+'</option>';
                    }
                    $("#clientid").append(clientlist);
                    $('#clientid').trigger("chosen:updated");
                }

                if(value.jd.length>0){
                    for(var val=0;val<value.jd.length;val++)
                    {
                        jdlist += '<option value="'+value.jd[val].jdid+'">'+value.jd[val].jdname+'</option>';
                    }
                    $("#jdid").append(jdlist);
                    $('#jdid').trigger("chosen:updated");
                }
            }
		}
    });
}

/*================================================================================================
	No           : 23
	Service Name : addjob
	Purpose      : add job
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

$("form[name='addjob']").validate({
	rules: {
		jobpostname: {
			required: true
		},
		expiredate: {
			required: true
		}
	},
	messages: {
		jobpostname: {
			required: "<span class='text-red'>Please provide Job Name</span>"
		},
		expiredate: {
			required: "<span class='text-red'>Please select Expire Date</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var jobpostname = $("#jobpostname").val();
		var clientid = $("#clientid").val();
		var jdid = $("#jdid").val();
		var expiredate = $("#expiredate").val();

		if(clientid==null && jdid==null)
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            positionClass: 'toast-bottom-right',
		            showMethod: 'slideDown',
		            timeOut: 4000
		        };
		        toastr.error('is required', 'Client Name & JD Name');
		    }, 1300);
		}
		else if(clientid=="" || clientid==null)
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            positionClass: 'toast-bottom-right',
		            showMethod: 'slideDown',
		            timeOut: 4000
		        };
		        toastr.error('is required', 'Client Name');
		    }, 1300);
		}
		else if(jdid=="" || jdid==null)
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            positionClass: 'toast-bottom-right',
		            showMethod: 'slideDown',
		            timeOut: 4000
		        };
		        toastr.error('is required', 'JD Name');
		    }, 1300);
		}
		else
		{
			var datapass = {"jobpostname":jobpostname,"clientid":clientid,"jdid":jdid,"expiredate":expiredate};

			$.ajax({
				url: 'admin-service.php?servicename=addjob',
				type: 'POST',
				datatype: 'JSON',
				contentType: 'application/json',
				async: true,
				data: JSON.stringify(datapass),
				success: function(data)
				{
					var value = JSON.parse(data);

					if(value.status == 'success')
					{
						localStorage.setItem('addjob', 'yes');
						window.location.href = 'job.php';
					}
					else
					{
						var message = value.message;
						setTimeout(function() {
					        toastr.options = {
					            closeButton: true,
					            progressBar: true,
					            positionClass: 'toast-bottom-right',
					            showMethod: 'slideDown',
					            timeOut: 4000
					        };
					        toastr.error('Please Try Again', message);
					    }, 1300);

						$(".form-horizontal button[type=submit]").prop("disabled", false);
					}
				}
			});
		}
	}
});

/*================================================================================================
	No           : 24
	Service Name : viewjob
	Purpose      : displaying jd in datatable
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

function viewJob(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewjob',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.jobpost.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.jobpost.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.jobpost[val].jobpostname;
                        i[val][2] = value.jobpost[val].clientname;
                        i[val][3] = value.jobpost[val].jdname;
                        i[val][4] = value.jobpost[val].expiredate;
                        if(value.jobpost[val].expirestatus == '1'){
	                        if(value.jobpost[val].status == '1'){
	                            i[val][5] = '<label class="switch switch-success"><input type="checkbox" name="jobstatus" data-jobpostid="'+value.jobpost[val].jobpostid+'" value="0" checked="checked"><span></span></label>';
	                        } else {
	                            i[val][5] = '<label class="switch switch-success"><input type="checkbox" name="jobstatus" data-jobpostid="'+value.jobpost[val].jobpostid+'" ><span></span></label>';
	                        }
	                    } else {
	                    	i[val][5] = '<span class="label label-default">Expired</span>';
	                    }
	                    if(value.jobpost[val].expirestatus == '1'){
	                        i[val][6] = '<a class="btn btn-default btn-circle facebookshare" href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=https://careersbay.com/jobs.html?title='+value.jobpost[val].joblink+'&pubid=ra-55810ee8392537bf&ct=1&title='+value.jobpost[val].jobpostname+'&pco=tbxnj-1.0" target="_blank"><i class="fa fa-facebook"></i></a>&nbsp;&nbsp;<a class="btn btn-default btn-circle twittershare" href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=https://careersbay.com/jobs.html?title='+value.jobpost[val].joblink+'&screenshot=https://careersbay.com/assets/images/careersbay-340x104.jpg&pco=tbxnj-1.0" target="_blank"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;<a class="btn btn-default btn-circle googleshare" href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=https://careersbay.com/jobs.html?title='+value.jobpost[val].joblink+'&ct=1&title='+value.jobpost[val].jobpostname+'&pco=tbxnj-1.0" target="_blank"><i class="fa fa-google-plus"></i></a>&nbsp;&nbsp;<a class="btn btn-default btn-circle linkedinshare" href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=https://careersbay.com/jobs.html?title='+value.jobpost[val].joblink+'&pubid=ra-55810ee8392537bf&ct=1&title='+value.jobpost[val].jobpostname+'&pco=tbxnj-1.0" target="_blank"><i class="fa fa-linkedin"></i></a>';
	                    } else {
	                    	i[val][6] = 'NA';
	                    }
                        i[val][7] = '<div class="btn-group"><button type="button" data-jobpostid="'+value.jobpost[val].jobpostid+'" data-viewtype="editable" data-toggle="modal" data-target="#editjobmodal" data-backdrop="static" data-keyboard="false" onclick="editJob(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-jobpostid="'+value.jobpost[val].jobpostid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editjobmodal" data-backdrop="static" data-keyboard="false" onclick="editJob(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-jobpostid="'+value.jobpost[val].jobpostid+'" onclick="deleteJob(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewjob').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Job Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 25
	Service Name : jobstatus
	Purpose      : change job status Enable/Disable
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

$(document).on("change", "input[name='jobstatus']", function(){
    var checkbox = $(this);
    var jobpostid = checkbox.data('jobpostid');
    var checked = checkbox.prop('checked');
    var status = '';
    var datapass = {"jobpostid":jobpostid,"checked":checked};

    $.ajax({
        type: 'POST',
        url : 'admin-service.php?servicename=updatejobstatus',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
        success: function(data){
        	var value = JSON.parse(data);
			if(value.status == 'success')
	        {
	        	if(checked == true){
	        		status = 'Enabled';
	        	} else {
	        		status = 'Disabled';
	        	}

	        	setTimeout(function() {
			        toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            positionClass: 'toast-bottom-right',
			            showMethod: 'slideDown',
			            timeOut: 4000
			        };
			        toastr.success('Job now '+status, '');
			    }, 1300);
	        } else {
	        	setTimeout(function() {
			        toastr.options = {
			            closeButton: true,
			            progressBar: true,
			            positionClass: 'toast-bottom-right',
			            showMethod: 'slideDown',
			            timeOut: 4000
			        };
			        toastr.error('Please Try Again', '');
			    }, 1300);
	        }
        }
    });
});

/*================================================================================================
	No           : 26
	Service Name : editjob
	Purpose      : get job details in modal
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

function editJob(eb){
    var jobpostid = $(eb).data("jobpostid");
    var viewtype = $(eb).data("viewtype");
    var datapass = {"jobpostid":jobpostid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getjob',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var jobpostid = value.jobpostid;
			var jobpostname = value.jobpostname;
			var clientid = value.clientid;
			var clientname = value.clientname;
			var jdid = value.jdid;
			var jdname = value.jdname;
			var expiredate = value.expiredate;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Job');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Job Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$('#jobpostid').val(jobpostid);
			$('#jobpostname').val(jobpostname);
			$("#clientid").val(clientid).trigger('chosen:updated');
			$("#jdid").val(jdid).trigger('chosen:updated');
			$("#expiredate").val(expiredate);
		}
    });
}

/*================================================================================================
	No           : 27
	Service Name : savejob
	Purpose      : save job edit detail in DB
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

$("form[name='savejob']").validate({
    rules: {
		jobpostname: {
			required: true
		},
		expiredate: {
			required: true
		}
	},
	messages: {
		jobpostname: {
			required: "<span class='text-red'>Please provide Job Name</span>"
		},
		expiredate: {
			required: "<span class='text-red'>Please select Expire Date</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var jobpostid = $("#jobpostid").val();
		var jobpostname = $("#jobpostname").val();
		var clientid = $("#clientid").val();
		var jdid = $("#jdid").val();
		var expiredate = $("#expiredate").val();
		var datapass = {"jobpostid":jobpostid,"jobpostname":jobpostname,"clientid":clientid,"jdid":jdid,"expiredate":expiredate};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=savejob',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('savejob', 'yes');

					window.location.href = 'job.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 28
	Service Name : deletejob
	Purpose      : delete job
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

function deleteJob(dj){
	var jobpostid = $(dj).data('jobpostid');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete Job!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"jobpostid":jobpostid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deletejob',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Job has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Job has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 29
	Service Name : viewResumes
	Purpose      : displaying resumes in datatable
	Create By    : Aavin Seth
	Created On   : 13-12-2017
================================================================================================*/

function viewResumes(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewresumes',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.resumes.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.resumes.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = '<div class="checkbox m-r-xs resumechkbox'+sr+'"><input type="checkbox" data-resumeid="'+value.resumes[val].resumeid+'" id="resumechkbox'+value.resumes[val].resumeid+'" class="resumechkbox"><label for="resumechkbox'+value.resumes[val].resumeid+'">'+sr+'</label></div>';
                        i[val][1] = value.resumes[val].name;
                        i[val][2] = value.resumes[val].isd;
                        i[val][3] = value.resumes[val].phone;
                        i[val][4] = value.resumes[val].email;
                        i[val][5] = value.resumes[val].country;
                        i[val][6] = value.resumes[val].nationality;
                        i[val][7] = value.resumes[val].clientname;
                        i[val][8] = value.resumes[val].shortlistclient;
                        i[val][9] = value.resumes[val].jobpostname;
                        i[val][10] = value.resumes[val].jdname;
                        if(value.resumes[val].skillsets.length < 15){
                        	i[val][11] = value.resumes[val].skillsets;
                        } else {
                        	i[val][11] = '<span title="'+value.resumes[val].skillsets+'">'+value.resumes[val].skillsets.substring(0,15)+'...</span>';
                        }
                        i[val][12] = value.resumes[val].industryname;
                        i[val][13] = '<a href="../assets/resumes/'+value.resumes[val].resume+'" download="'+value.resumes[val].resume+'">Download</a>';
                        i[val][14] = value.resumes[val].createdate;
                        i[val][15] = '<div class="btn-group"><button type="button" data-name="'+value.resumes[val].name+'" data-resumeid="'+value.resumes[val].resumeid+'" data-toggle="modal" data-target="#editresumemodal" data-backdrop="static" data-keyboard="false" onclick="editResume(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewresumes').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Resumes Found"
                }
                /*initComplete: function () {
		            this.api().columns([6, 8]).every(function () {
		                var column = this;
		                var select = $('<select><option value="">Show All</option></select>').appendTo($(column.footer()).empty()).on('change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
	                        column.search(val ? '^'+val+'$' : '', true, false ).draw();
						});
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append('<option value="'+d+'">'+d+'</option>')
		                });
		            });
		        }*/
            });
        }
    });
}

/*================================================================================================
	No           : 30
	Service Name : editresume
	Purpose      : get candidate details in modal
	Create By    : Aavin Seth
	Created On   : 14-12-2017
================================================================================================*/

function editResume(er){
    var resumeid = $(er).data("resumeid");
    var name = $(er).data("name");
    var datapass = {"resumeid":resumeid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getresume',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var resumeid = value.resumeid;
			var candidatename = value.name;
			var email = value.email;
			var phone = value.phone;
			var isd = value.isd;
			var country = value.country;
			var nationality = value.nationality;
			var designation = value.designation;
			var qualification = value.qualification;
			var skillsetsStr = value.skillsets;
			var skillsets = skillsetsStr.split(",");
			var industryid = value.industryid;
			var resume = value.resume;

			$(".modal-content .modal-title").html('Edit Resume');
			$("#resumeid").val(resumeid);
			$("#candidatename").val(candidatename);
			$("#email").val(email);
			$("#isd").val(isd).trigger("chosen:updated");
			$("#phone").val(phone);
			$("#country").val(country).trigger("chosen:updated");
			$("#nationality").val(nationality).trigger("chosen:updated");
			$("#designation").val(designation).trigger("chosen:updated");
			$("#qualification").val(qualification).trigger("chosen:updated");
			$("#industrylist").val(industryid).trigger("chosen:updated");
			$('#skillsets').chosen('destroy').val(skillsets).trigger("chosen:updated");
			$('#skillsets').chosen();
			$("#oldresume").val(resume);
		}
    });
}

/*================================================================================================
	No           : 31
	Service Name : saveresume
	Purpose      : update candidate resume
	Create By    : Aavin Seth
	Created On   : 14-12-2017
================================================================================================*/

$("form[name='saveresume']").validate({
    rules: {
        candidatename: {
            required: true,
            lettersonly: true
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            digits: true
            // minlength: 8,
            // maxlength: 8
        },
        country: {
            required: true,
            lettersonly: true
        }
    },
    messages: {
        candidatename: {
            required: "<span class='validation-error'>Please provide a Name</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Name</span>"
        },
        email: {
            required: "<span class='validation-error'>Please provide a Email</span>",
            email: "<span class='validation-error'>Please provide a valid Email</span>"
        },
        phone: {
            required: "<span class='validation-error'>Please provide a Mobile Number</span>",
            digits: "<span class='validation-error'>Please provide a valid Mobile Number</span>",
            // minlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>,
            // maxlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>"
        },
        country: {
            required: "<span class='validation-error'>Please provide a Country</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Country</span>"
        }
    },
    submitHandler: function(form) {
        $(".form-horizontal button[type=submit]").prop("disabled", true);
        $('#skillsets-error').addClass('hide');

        var resumeid = $("#resumeid").val();
        var candidatename = $("#candidatename").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var country = $("#country").val();
        var nationality = $("#nationality").val();
        var designation = '0';
        var qualification = $("#qualification").val();
        var skillsets = $("#skillsets").val();
        var industry = $("#industrylist").val();
        var oldresume = $("#oldresume").val();
        var resume = $("#resumebasecode").val();
        var resumeext = $("#resumeext").val();
        var mime = $("#mime").val();
        var skillsetflag = 1;

        if(skillsets=='' || skillsets==null)
        {
        	skillsetflag = 0;
        	$('#skillsets-error').removeClass('hide');
        }

        if(skillsetflag == 1)
        {
	        var datapass = {"resumeid":resumeid,"candidatename":candidatename,"email":email,"phone":phone,"country":country,"nationality":nationality,"designation":designation,"qualification":qualification,"skillsets":skillsets,"industry":industry,"oldresume":oldresume,"resume":resume,"resumeext":resumeext,"mime":mime};

	        $.ajax({
	            type: 'POST',
	            url: 'admin-service.php?servicename=saveresume',
	            datatype: 'JSON',
	            contentType: 'application/json',
	            async: true,
	            data: JSON.stringify(datapass),
	            success: function(data)
	            {
	                var value = JSON.parse(data);

	                if(value.status == 'success')
	                {
	                	localStorage.setItem('saveresume', 'yes');

						window.location.href = 'resumes.php';
	                }
	                else
	                {
	                    var message = value.message;

						setTimeout(function() {
					        toastr.options = {
					            closeButton: true,
					            progressBar: true,
					            positionClass: 'toast-bottom-right',
					            showMethod: 'slideDown',
					            timeOut: 4000
					        };
					        toastr.error('Please Try Again', message);
					    }, 1300);

						$(".form-horizontal button[type=submit]").prop("disabled", false);
	                }
	            }
	        });
		}
		else
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
		}
    }
});

$("#resume").change(function(){
	var baseCode = "";
	var file = document.querySelector('#resume').files[0];
	var mime = document.querySelector('#resume').files[0].type;
	var resume = $('#resume').val();
	var ext = resume.split(".").pop();
	var reader = new FileReader();

	if(ext=='pdf' || ext=='doc' || ext=='docx')
	{
		reader.addEventListener("load", function ()
		{
			baseCode = reader.result;
			$('#resumebasecode').val(baseCode);
			$('#mime').val(mime);
		}, false);

		if(file){
			reader.readAsDataURL(file);
		}
		$('#resumeext').val(ext);
	} else {
		setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.error('Only pdf, doc &amp; docx are allowed', '');
	    }, 1300);
	}
});

/*================================================================================================
	No           : 32
	Service Name : dashboard
	Purpose      : dashboard
	Create By    : Aavin Seth
	Created On   : 18-12-2017
================================================================================================*/

function dashboard(){
    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=dashboard',
		datatype: 'JSON',
		contentType: 'application/json',
		success: function(data)
		{
			var value = JSON.parse(data);

			if(value.status == 'success')
            {
                $('#resumesdash').html(value.resume);
            }
		}
    });
}

/*================================================================================================
	No           : 33
	Service Name : addresume
	Purpose      : add resume
	Create By    : Aavin Seth
	Created On   : 18-12-2017
================================================================================================*/

$("form[name='addresume']").validate({
    rules: {
    	clientid: {
            required: false
        },
        jdid: {
            required: true
        },
        candidatename: {
            required: true,
            lettersonly: true
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            digits: true
            // minlength: 8,
            // maxlength: 8
        },
        country: {
            required: true,
            lettersonly: true
        },
        yearexperience: {
            required: true,
            digits: true
        },
        //resumes: {
        //    required: true
        //}
    },
    messages: {
    	clientid: {
            required: "<span class='validation-error'>Please select Client</span>"
        },
        jdid: {
            required: "<span class='validation-error'>Please select JD</span>"
        },
        candidatename: {
            required: "<span class='validation-error'>Please provide a Name</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Name</span>"
        },
        email: {
            required: "<span class='validation-error'>Please provide a Email</span>",
            email: "<span class='validation-error'>Please provide a valid Email</span>"
        },
        phone: {
            required: "<span class='validation-error'>Please provide a Mobile Number</span>",
            digits: "<span class='validation-error'>Please provide a Number</span>"
            // minlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>",
            // maxlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>"
        },
        country: {
            required: "<span class='validation-error'>Please provide a Country</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Country</span>"
        },
        yearexperience: {
            required: "<span class='validation-error'>Please provide Year of Experience</span>",
            digits: "<span class='validation-error'>Please provide a valid Year of Experience</span>"
        },
        //resumes: {
        //    required: "<span class='validation-error'>Please provide a Resume</span>"
        //}
    },
    submitHandler: function(form) {
        $(".form-horizontal button[type=submit]").prop("disabled", true);
        $(".alert").addClass('hide');
        $(".alert").html('');
        $(".alert").removeClass('alert-danger');

        var jdid = $("#jdid").val();
        var jdname = $("#jdid option:selected").text();
        var clientid = $("#clientid").val();
        var clientname = $("#clientid option:selected").text();
        var candidatename = $("#candidatename").val();
        var email = $("#email").val();
        var isd = $("#isd").val();
        var phone = $("#phone").val();
        var country = $("#country").val();
        var nationality = $("#nationality").val();
        var industry = $("#industry").val();
        var qualification = $("#qualification").val();
        var skillsets = $("#skillsets").val();
        var yearexperience = $("#yearexperience").val();
        var cv = $("#cv").val();
        var resumes = $("#resumebasecode").val();
        if(resumes!='')
        {
            var resume = $("#resumebasecode").val();
        } else {
            var resume = $("#cv").val();
        }
        var resumeext = $("#resumeext").val();
        var mime = $("#mime").val();
        
        if(clientid=="" || clientid==null)
		{
			clientid = 0;
		}
		if(jdid=="" || jdid==null)
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            positionClass: 'toast-bottom-right',
		            showMethod: 'slideDown',
		            timeOut: 4000
		        };
		        toastr.error('JD', 'Please Select');
		    }, 1300);
		}
		else if(skillsets=="" || skillsets==null)
		{
			$(".form-horizontal button[type=submit]").prop("disabled", false);
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            positionClass: 'toast-bottom-right',
		            showMethod: 'slideDown',
		            timeOut: 4000
		        };
		        toastr.error('Skill Sets', 'Choose a');
		    }, 1300);
		}
		else
		{
			var datapass = {"jdid":jdid,"jdname":jdname,"clientid":clientid,"clientname":clientname,"candidatename":candidatename,"email":email,"isd":isd,"phone":phone,"country":country,"nationality":nationality,"industry":industry,"qualification":qualification,"skillsets":skillsets,"yearexperience":yearexperience,"resume":resume,"resumeext":resumeext,"mime":mime};

	        $.ajax({
	            type: 'POST',
	            url: 'admin-service.php?servicename=addresume',
	            datatype: 'JSON',
	            contentType: 'application/json',
	            async: true,
	            data: JSON.stringify(datapass),
	            success: function(data)
	            {
	                var value = JSON.parse(data);

	                if(value.status == 'success')
	                {
	                    localStorage.setItem('addresume', 'yes');
						window.location.href = 'resumes.php';
	                }
	                else
	                {
	                    var msg = value.message;
	                    if(value.attachment == 'failure')
	                	{
	                		$('#resume-error1').html('<span class="validation-error">Only pdf, doc &amp; docx are allowed</span>');
	                	}
	                	else
	                	{
	                		$(".alert").html('');
		                    $(".alert").addClass('alert-danger');
		                    $(".alert").html(msg);
		                    $(".alert").removeClass('hide');
	                	}
	                }
	                $(".form-horizontal button[type=submit]").prop("disabled", false);
	            }
	        });
	    }
    }
});

$("#resumes").change(function(){
	var baseCode = "";
	var file = document.querySelector('#resumes').files[0];
	var mime = document.querySelector('#resumes').files[0].type;
	var resume = $('#resumes').val();
	var ext = resume.split(".").pop();
	var reader = new FileReader();

	if(ext=='pdf' || ext=='doc' || ext=='docx')
	{
		$('#resume-error1').addClass('hide');
		reader.addEventListener("load", function ()
		{
			baseCode = reader.result;
			$('#resumebasecode').val(baseCode);
			$('#mime').val(mime);
		}, false);

		if(file){
			reader.readAsDataURL(file);
		}
		$('#resumeext').val(ext);
	} else {
		$('#resume-error1').removeClass('hide');
	}
});

/*================================================================================================
	No           : 34
	Service Name : adddesignation
	Purpose      : add designation
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='adddesignation']").validate({
	rules: {
		designationname: {
			required: true
		}
	},
	messages: {
		designationname: {
			required: "<span class='text-red'>Please provide Designation Name</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var designationname = $("#designationname").val();
		var datapass = {"designationname":designationname};

		$.ajax({
			url: 'admin-service.php?servicename=adddesignation',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('adddesignation', 'yes');
					window.location.href = 'designation.php';
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 35
	Service Name : viewdesignation
	Purpose      : display designation in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function viewDesignation(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewdesignation',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.designation.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.designation.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.designation[val].designationname;
                        i[val][2] = '<div class="btn-group"><button type="button" data-designationid="'+value.designation[val].designationid+'" data-viewtype="editable" data-toggle="modal" data-target="#editdesignationmodal" data-backdrop="static" data-keyboard="false" onclick="editDesignation(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-designationid="'+value.designation[val].designationid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editdesignationmodal" data-backdrop="static" data-keyboard="false" onclick="editDesignation(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-designationid="'+value.designation[val].designationid+'" data-designationname="'+value.designation[val].designationname+'" onclick="deleteDesignation(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewdesignation').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Designation Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 36
	Service Name : editDesignation
	Purpose      : get designation details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function editDesignation(ed){
    var designationid = $(ed).data("designationid");
    var viewtype = $(ed).data("viewtype");
    var datapass = {"designationid":designationid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getdesignation',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var designationid = value.designationid;
			var designationname = value.designationname;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Designation');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Designation Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$("#designationid").val(designationid);
			$("#designationname").val(designationname);
		}
    });
}

/*================================================================================================
	No           : 37
	Service Name : savedesignation
	Purpose      : update designation detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='savedesignation']").validate({
    rules: {
		designationname: {
			required: true
		}
	},
	messages: {
		designationname: {
			required: "<span class='text-red'>Please provide Designation Name</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var designationid = $("#designationid").val();
		var designationname = $("#designationname").val();
		var datapass = {"designationid":designationid,"designationname":designationname};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=savedesignation',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('savedesignation', 'yes');

					window.location.href = 'designation.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 38
	Service Name : deletedesignation
	Purpose      : delete designation
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function deleteDesignation(dd){
	var designationid = $(dd).data('designationid');
	var designationname = $(dd).data('designationname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete Designation <span style="font-weight: bold;">'+designationname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"designationid":designationid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deletedesignation',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Designation <span style="font-weight: bold;">'+designationname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Designation <span style="font-weight: bold;">'+designationname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 39
	Service Name : addqualification
	Purpose      : add qualification
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='addqualification']").validate({
	rules: {
		qualificationname: {
			required: true
		}
	},
	messages: {
		qualificationname: {
			required: "<span class='text-red'>Please provide Qualification Name</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var qualificationname = $("#qualificationname").val();
		var datapass = {"qualificationname":qualificationname};

		$.ajax({
			url: 'admin-service.php?servicename=addqualification',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('addqualification', 'yes');
					window.location.href = 'qualification.php';
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 40
	Service Name : viewqualification
	Purpose      : display qualification in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function viewQualification(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewqualification',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.qualification.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.qualification.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.qualification[val].qualificationname;
                        i[val][2] = '<div class="btn-group"><button type="button" data-qualificationid="'+value.qualification[val].qualificationid+'" data-viewtype="editable" data-toggle="modal" data-target="#editqualificationmodal" data-backdrop="static" data-keyboard="false" onclick="editQualification(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-qualificationid="'+value.qualification[val].qualificationid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editqualificationmodal" data-backdrop="static" data-keyboard="false" onclick="editQualification(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-qualificationid="'+value.qualification[val].qualificationid+'" data-qualificationname="'+value.qualification[val].qualificationname+'" onclick="deleteQualification(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewqualification').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Qualification Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 41
	Service Name : editqualification
	Purpose      : get qualification details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function editQualification(ed){
    var qualificationid = $(ed).data("qualificationid");
    var viewtype = $(ed).data("viewtype");
    var datapass = {"qualificationid":qualificationid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getqualification',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var qualificationid = value.qualificationid;
			var qualificationname = value.qualificationname;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Qualification');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Qualification Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$("#qualificationid").val(qualificationid);
			$("#qualificationname").val(qualificationname);
		}
    });
}

/*================================================================================================
	No           : 42
	Service Name : savequalification
	Purpose      : update qualification detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='savequalification']").validate({
    rules: {
		qualificationname: {
			required: true
		}
	},
	messages: {
		qualificationname: {
			required: "<span class='text-red'>Please provide Qualification Name</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var qualificationid = $("#qualificationid").val();
		var qualificationname = $("#qualificationname").val();
		var datapass = {"qualificationid":qualificationid,"qualificationname":qualificationname};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=savequalification',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('savequalification', 'yes');

					window.location.href = 'qualification.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 43
	Service Name : deletequalification
	Purpose      : delete qualification
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function deleteQualification(dd){
	var qualificationid = $(dd).data('qualificationid');
	var qualificationname = $(dd).data('qualificationname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete Qualification <span style="font-weight: bold;">'+qualificationname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"qualificationid":qualificationid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deletequalification',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Qualification <span style="font-weight: bold;">'+qualificationname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Qualification <span style="font-weight: bold;">'+qualificationname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 44
	Service Name : addskillsets
	Purpose      : add skillsets
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='addskillsets']").validate({
	rules: {
		skillsetsname: {
			required: true
		}
	},
	messages: {
		skillsetsname: {
			required: "<span class='text-red'>Please provide Skillsets Name</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var skillsetsname = $("#skillsetsname").val();
		var datapass = {"skillsetsname":skillsetsname};

		$.ajax({
			url: 'admin-service.php?servicename=addskillsets',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('addskillsets', 'yes');
					window.location.href = 'skillsets.php';
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 45
	Service Name : viewskillsets
	Purpose      : display skillsets in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function viewSkillsets(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewskillsets',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.skillsets.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.skillsets.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.skillsets[val].skillsetsname;
                        i[val][2] = '<div class="btn-group"><button type="button" data-skillsetsid="'+value.skillsets[val].skillsetsid+'" data-viewtype="editable" data-toggle="modal" data-target="#editskillsetsmodal" data-backdrop="static" data-keyboard="false" onclick="editSkillsets(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-skillsetsid="'+value.skillsets[val].skillsetsid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editskillsetsmodal" data-backdrop="static" data-keyboard="false" onclick="editSkillsets(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-skillsetsid="'+value.skillsets[val].skillsetsid+'" data-skillsetsname="'+value.skillsets[val].skillsetsname+'" onclick="deleteSkillsets(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    }
                }
            }

            $('#viewskillsets').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Skillsets Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            });
        }
    });
}

/*================================================================================================
	No           : 46
	Service Name : editskillsets
	Purpose      : get skillsets details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function editSkillsets(es){
    var skillsetsid = $(es).data("skillsetsid");
    var viewtype = $(es).data("viewtype");
    var datapass = {"skillsetsid":skillsetsid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getskillsets',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var skillsetsid = value.skillsetsid;
			var skillsetsname = value.skillsetsname;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Skillsets');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Skillsets Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$("#skillsetsid").val(skillsetsid);
			$("#skillsetsname").val(skillsetsname);
		}
    });
}

/*================================================================================================
	No           : 47
	Service Name : saveskillsets
	Purpose      : update skillsets detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

$("form[name='saveskillsets']").validate({
    rules: {
		skillsetsname: {
			required: true
		}
	},
	messages: {
		skillsetsname: {
			required: "<span class='text-red'>Please provide Skillsets Name</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var skillsetsid = $("#skillsetsid").val();
		var skillsetsname = $("#skillsetsname").val();
		var datapass = {"skillsetsid":skillsetsid,"skillsetsname":skillsetsname};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=saveskillsets',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('saveskillsets', 'yes');

					window.location.href = 'skillsets.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 48
	Service Name : deleteskillsets
	Purpose      : delete skillsets
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

function deleteSkillsets(ds){
	var skillsetsid = $(ds).data('skillsetsid');
	var skillsetsname = $(ds).data('skillsetsname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete Skillsets <span style="font-weight: bold;">'+skillsetsname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"skillsetsid":skillsetsid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deleteskillsets',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Skillsets <span style="font-weight: bold;">'+skillsetsname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Skillsets <span style="font-weight: bold;">'+skillsetsname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
    No           : 49
    Service Name : dqs
    Purpose      : append designation, qualification and skill set
    Create By    : Aavin Seth
    Created On   : 23-01-2018
================================================================================================*/

function dqs(){
    $.ajax({
        type: 'POST',
        url : 'admin-service.php?servicename=dqs',
        datatype: 'JSON',
        contentType: 'application/json',
        async: true,
        success: function(data)
        {
            var value = JSON.parse(data);
            var designationlist;
            var qualificationlist;
            var skillsetslist;
            var industrylist;
            var countrieslist;
            var phonecodelist;
            var emaillist;

            if(value.status == 'success')
            {
                if(value.designation.length>0)
                {
                    for(var val=0;val<value.designation.length;val++)
                    {
                        designationlist += '<option value="'+value.designation[val].designationid+'">'+value.designation[val].designationname+'</option>';
                    }
                    $('#designation').append(designationlist);
                    $('#designation').trigger("chosen:updated");
                }

                if(value.qualification.length>0)
                {
                    for(var val=0;val<value.qualification.length;val++)
                    {
                        qualificationlist += '<option value="'+value.qualification[val].qualificationid+'">'+value.qualification[val].qualificationname+'</option>';
                    }
                    $('#qualification').append(qualificationlist);
                    $('#qualification').trigger("chosen:updated");
                }

                if(value.skillsets.length>0)
                {
                    for(var val=0;val<value.skillsets.length;val++)
                    {
                        skillsetslist += '<option value="'+value.skillsets[val].skillsetsid+'">'+value.skillsets[val].skillsetsname+'</option>';
                    }
                    $('#skillsets,#filterskillsets').append(skillsetslist);
                    $('#skillsets,#filterskillsets').trigger("chosen:updated");
                }

                if(value.industry.length>0)
                {
                    for(var val=0;val<value.industry.length;val++)
                    {
                        industrylist += '<option value="'+value.industry[val].industryid+'">'+value.industry[val].industryname+'</option>';
                    }
                    $('#industry').append(industrylist);
                    $('#industry').trigger("chosen:updated");
                }

                if(value.country.length>0)
                {
                    for(var val=0;val<value.country.length;val++)
                    {
                        countrieslist += '<option data-countryid="'+value.country[val].countryid+'" value="'+value.country[val].countryname+'">'+value.country[val].countryname+'</option>';
                    }
                    $('#country,#filtercountry').append(countrieslist);
                    $('#country,#filtercountry').trigger("chosen:updated");
                }

                if(value.countries.length>0)
                {
                    for(var val=0;val<value.countries.length;val++)
                    {
                        phonecodelist += '<option data-img-src="http://careersbay.com/img/flags/32/'+value.countries[val].countryname+'.png" value="'+value.countries[val].phonecode+'">'+value.countries[val].phonecode+' ('+value.countries[val].countryname+')</option>';
                    }
                    $('#isd,#filterisd').append(phonecodelist);
                    $('#isd,#filterisd').trigger("chosen:updated");
                }

                if(value.email.length>0)
                {
                    for(var val=0;val<value.email.length;val++)
                    {
                        emaillist += '<option value="'+value.email[val].email+'">'+value.email[val].email+'</option>';
                    }
                    $('#filteremaillist').append(emaillist);
                    $('#filteremaillist').trigger("chosen:updated");
                }
            }
        }
    });
}

/*================================================================================================
    No           : 50
    Service Name : resumesfilter
    Purpose      : filter resumes
    Create By    : Aavin Seth
    Created On   : 24-01-2018
================================================================================================*/

function resumesFilter(dt){
	$("form[name='resumesfilter'] button[type=submit]").prop("disabled", true);

	var isd = $("#filterisd").val();
	var country = $("#filtercountry").val();
	var clientslist = $("#filterclientslist").val();
	var jdlist = $("#filterjdlist").val();
	var skillsets = $("#filterskillsets").val();
	var industry = $("#filterindustrylist").val();
	var email = $("#filteremaillist").val();

	if(isd != '' || country != '' || email != '' || clientslist != '' || jdlist != '' || skillsets != null || industry != '')
	{
		var datapass = {"isd":isd,"country":country,"email":email,"client":clientslist,"jd":jdlist,"skillsets":skillsets,"industry":industry};

		$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=resumesfilter',
	        datatype: 'JSON',
	        contentType: 'application/json',
	        async: true,
	        data: JSON.stringify(datapass),
	        success: function(data)
	        {
	            var value = JSON.parse(data);

	            if(value.status == 'success')
	            {
	                if(value.resumes.length>0){
	                    var i = new Array();
	                    var sr = 1;
	                    for(var val=0;val<value.resumes.length;val++)
	                    {
	                        i[val] = new Array();
	                        i[val][0] = '<div class="checkbox m-r-xs resumechkbox'+sr+'"><input type="checkbox" data-resumeid="'+value.resumes[val].resumeid+'" id="resumechkbox'+value.resumes[val].resumeid+'" class="resumechkbox"><label for="resumechkbox'+value.resumes[val].resumeid+'">'+sr+'</label></div>';
	                        i[val][1] = value.resumes[val].name;
	                        i[val][2] = value.resumes[val].isd;
	                        i[val][3] = value.resumes[val].phone;
	                        i[val][4] = value.resumes[val].email;
	                        i[val][5] = value.resumes[val].country;
	                        i[val][6] = value.resumes[val].nationality;
	                        i[val][7] = value.resumes[val].clientname;
	                        i[val][8] = value.resumes[val].shortlistclient;
	                        i[val][9] = value.resumes[val].jobpostname;
	                        i[val][10] = value.resumes[val].jdname;
	                        if(value.resumes[val].skillsets.length < 15){
	                        	i[val][11] = value.resumes[val].skillsets;
	                        } else {
	                        	i[val][11] = '<span title="'+value.resumes[val].skillsets+'">'+value.resumes[val].skillsets.substring(0,15)+'...</span>';
	                        }
	                        i[val][12] = value.resumes[val].industryname;
	                        i[val][13] = '<a href="../assets/resumes/'+value.resumes[val].resume+'" download="'+value.resumes[val].resume+'">Download</a>';
	                        i[val][14] = value.resumes[val].createdate;
	                        i[val][15] = '<div class="btn-group"><button type="button" data-name="'+value.resumes[val].name+'" data-resumeid="'+value.resumes[val].resumeid+'" data-toggle="modal" data-target="#editresumemodal" data-backdrop="static" data-keyboard="false" onclick="editResume(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button></div>';
	                        sr++;
	                    }
	                }
	            }
	            else
	            {
	                $('#viewresumes').empty();
	            }
	            $("form[name='resumesfilter'] button[type=submit]").prop("disabled", false);

	            $('#viewresumes').DataTable({
	                "aaData": i,
	                "bDestroy": true,
	                "paging": true,
	                "responsive": true,
	                "lengthChange": true,
	                "pageLength": 10,
	                "searching": true,
	                "ordering": true,
	                "info": true,
	                "autoWidth": false,
	                "order": [[ 0, "asc" ]],
	                "language": {
	                    "emptyTable": "No Resumes Found"
	                }
	                /*dom: '<"html5buttons"B>lTfgitp',
	                buttons: [
	                    {extend: 'copy'},
	                    {extend: 'csv'},
	                    {extend: 'excel', title: 'Clients'},
	                    {extend: 'pdf', title: 'Clients'},
	                    {extend: 'print',
	                        customize: function (win){
	                            $(win.document.body).addClass('white-bg');
	                            $(win.document.body).css('font-size', '10px');
	                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	                        }
	                    }
	                ]*/
	            }); 
	        }
	    });
	}
	else
	{
		setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.error('Select atleast one field','');
	    }, 1300);
	}
}

function resetResumesFilter(){
	$('#filterisd').val('').trigger("chosen:updated");
	$('#filtercountry').val('').trigger("chosen:updated");
	$('#filteremaillist').val('').trigger("chosen:updated");
	$('#filterclientslist').val('').trigger("chosen:updated");
	$('#filterjdlist').val('').trigger("chosen:updated");
	$('#filterskillsets').val('').trigger("chosen:updated");
	$('#filterindustrylist').val('').trigger("chosen:updated");
	$('#viewresumes').empty();
	viewResumes();
}

/*================================================================================================
    No           : 51
    Service Name : referresumeclients
    Purpose      : refer resumes to clients
    Create By    : Aavin Seth
    Created On   : 25-01-2018
================================================================================================*/

var resumecounting = 0;
function shortList(dt){
	$(".form-horizontal button[type=submit]").prop("disabled", true);
	var resumecount = $(dt).attr('data-resumecount');
	var clientid = $('#clientslist').val();
	$('#shortlist-error').addClass('hide');
	$('#shortlist-error').html('');

	if(clientid=='' && resumecount=='0')
	{
		$('#shortlist-error').html('Please select client and atleast one resume');
		$('#shortlist-error').removeClass('hide');
		$(".form-horizontal button[type=submit]").prop("disabled", false);
	}
	else if(clientid=='')
	{
		$('#shortlist-error').html('Please select client');
		$('#shortlist-error').removeClass('hide');
		$(".form-horizontal button[type=submit]").prop("disabled", false);
	}
	else if(resumecount=='0')
	{
		$('#shortlist-error').html('Please select atleast one resume');
		$('#shortlist-error').removeClass('hide');
		$(".form-horizontal button[type=submit]").prop("disabled", false);
	}
	else
	{
		var resumes = [];
		$('.resumechkbox:checked').each(function()
		{
			var resumeid = $(this).data('resumeid');
			resumes.push({"resumeid":resumeid});
		});

		var datapass = {"clientid":clientid,"resumes":resumes};

		$.ajax({
			url: 'admin-service.php?servicename=shortlist',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.success('Resume shortlisted Successfully!','');
				        localStorage.removeItem('shortlist');
				    }, 1300);
				    $('.resumechkbox:checked').prop('checked', false);
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
}

$(document).on("change", ".resumechkbox", function(){
    if(this.checked){
        resumecounting++;                 
    } else {
        resumecounting--;
    }
    $('#resumecount').attr('data-resumecount',resumecounting);
});

/*================================================================================================
	No           : 52
	Service Name : viewshortlist
	Purpose      : displaying resumes in datatable
	Create By    : Aavin Seth
	Created On   : 25-01-2018
================================================================================================*/

function viewShortListResumes(dt){
	$('#client-error').addClass('hide');
	var clientid = $("#clientslist").val();

	if(clientid=='')
	{
		$('#client-error').removeClass('hide');
		$('#referbtn').addClass('hide');
		$('#shortlisttable').addClass('hide');
	}
	else
	{
		$('#shortlisttable').removeClass('hide');
		$('#referbtn').removeClass('hide');
		var datapass = {"clientid":clientid};

		$.ajax({
	        type: 'POST',
	        url: 'admin-service.php?servicename=viewshortlist',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success:function(data)
	        {
	            var value = JSON.parse(data);

	            if(value.status == 'success')
	            {
	            	// $('#viewshortlist').empty();
	                if(value.resumes.length>0){
	                    var i = new Array();
	                    var sr = 1;
	                    for(var val=0;val<value.resumes.length;val++)
	                    {
	                        i[val] = new Array();
	                        i[val][0] = sr;
	                        i[val][1] = value.resumes[val].name;
	                        i[val][2] = value.resumes[val].isd;
	                        i[val][3] = value.resumes[val].phone;
	                        i[val][4] = value.resumes[val].email;
	                        i[val][5] = value.resumes[val].country;
	                        i[val][6] = value.resumes[val].nationality;
	                        i[val][7] = value.resumes[val].clientname;
	                        i[val][8] = value.resumes[val].jobpostname;
	                        i[val][9] = value.resumes[val].jdname;
	                        if(value.resumes[val].skillsets.length < 15){
	                        	i[val][10] = value.resumes[val].skillsets;
	                        } else {
	                        	i[val][10] = '<span title="'+value.resumes[val].skillsets+'">'+value.resumes[val].skillsets.substring(0,15)+'...</span>';
	                        }
	                        i[val][11] = value.resumes[val].industryname;
	                        i[val][12] = '<a href="../assets/resumes/'+value.resumes[val].resume+'" download="'+value.resumes[val].resume+'">Download</a>';
	                        i[val][13] = value.resumes[val].createdate;
	                        i[val][14] = '<div class="btn-group"><button type="button" data-resumeshortlistid="'+value.resumes[val].resumeshortlistid+'" onclick="deleteShortlistResume(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
	                        sr++;
	                    }
	                }
	            }
	            else
	            {
	            	$('#viewshortlist').empty();
	            }

	            $('#viewshortlist').DataTable({
	                "aaData": i,
	                "bDestroy": true,
	                "paging": true,
	                "responsive": true,
	                "lengthChange": true,
	                "pageLength": 10,
	                "searching": true,
	                "ordering": true,
	                "info": true,
	                "autoWidth": false,
	                "order": [[ 0, "asc" ]],
	                "language": {
	                    "emptyTable": "No Resumes Found"
	                }
	            });
	        }
	    });
	}
}

/*================================================================================================
	No           : 53
	Service Name : deleteShortlistResume
	Purpose      : delete Shortlist Resume
	Create By    : Aavin Seth
	Created On   : 25-01-2018
================================================================================================*/

function deleteShortlistResume(ds){
	var resumeshortlistid = $(ds).data('resumeshortlistid');
    swal({
        title: 'Are you sure?',
        text: 'You want to remove',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, remove it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"resumeshortlistid":resumeshortlistid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deleteshortlistresume',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Resume has been removed',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Resume has not removed',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 54
	Service Name : referShortlistResume
	Purpose      : refer Shortlist Resume to client
	Create By    : Aavin Seth
	Created On   : 27-01-2018
================================================================================================*/

function referShortlistResume(){
	var clientid = $('#clientslist').val();
	var clientname = $("#clientslist option:selected").text();
    swal({
        title: 'Are you sure?',
        text: 'You want to refer this resumes to '+clientname,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, refer it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"clientid":clientid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=refershortlistresume',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Refered!',
				        text: 'Resume has been refered',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	window.location.href = 'resumes.php';
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Resume has not refered',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 55
	Service Name : addindustry
	Purpose      : add industry
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

$("form[name='addindustry']").validate({
	rules: {
		industryname: {
			required: true
		}
	},
	messages: {
		industryname: {
			required: "<span class='text-red'>Please provide Industry Name</span>"
		}
	},
	submitHandler: function(form) {
		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var industryname = $("#industryname").val();
		var datapass = {"industryname":industryname};

		$.ajax({
			url: 'admin-service.php?servicename=addindustry',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('addindustry', 'yes');
					window.location.href = 'industry.php';
				}
				else
				{
					var message = value.message;
					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 56
	Service Name : viewindustry
	Purpose      : display industry in datatable
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

function viewIndustry(){
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewindustry',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.industry.length>0){
                    var i = new Array();
                    var sr = 1;
                    var industrylist;
                    for(var val=0;val<value.industry.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.industry[val].industryname;
                        i[val][2] = '<div class="btn-group"><button type="button" data-industryid="'+value.industry[val].industryid+'" data-viewtype="editable" data-toggle="modal" data-target="#editindustrymodal" data-backdrop="static" data-keyboard="false" onclick="editIndustry(this);" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i></button><button type="button" data-industryid="'+value.industry[val].industryid+'" data-viewtype="noneditable" data-toggle="modal" data-target="#editindustrymodal" data-backdrop="static" data-keyboard="false" onclick="editIndustry(this);" class="btn btn-info"><i class="fa fa-search"></i></button><button type="button" data-industryid="'+value.industry[val].industryid+'" data-industryname="'+value.industry[val].industryname+'" onclick="deleteIndustry(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        sr++;
                    	industrylist += '<option value="'+value.industry[val].industryid+'">'+value.industry[val].industryname+'</option>';
                    }
                    $("#industrylist,#filterindustrylist").append(industrylist);
                    $('#industrylist,#filterindustrylist').trigger("chosen:updated");
                }
            }

            $('#viewindustry').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Industry Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 57
	Service Name : editindustry
	Purpose      : get industry details in modal
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

function editIndustry(ed){
    var industryid = $(ed).data("industryid");
    var viewtype = $(ed).data("viewtype");
    var datapass = {"industryid":industryid};

    $.ajax({
		type: 'POST',
		url : 'admin-service.php?servicename=getindustry',
		datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
		success: function(data)
		{
			var value = JSON.parse(data);

			var industryid = value.industryid;
			var industryname = value.industryname;

			if(viewtype == 'editable'){
				$(".modal-content .modal-body input").prop("readonly", false);
				$(".modal-content .modal-body select").prop("disabled", false);
				$(".modal-content .modal-footer button").show();
				$(".modal-content .modal-footer button").prop("disabled", false);
				$(".modal-content .modal-title").html('Edit Industry');
				$(".modal-content .text-danger").show();
			} else {
				$(".modal-content .modal-body input").prop("readonly", true);
				$(".modal-content .modal-body select").prop("disabled", true);
				$(".modal-content .modal-footer button[type=submit]").hide();
				$(".modal-content .modal-title").html('Industry Detail');
				$(".modal-content .text-danger").hide();
			}
			
			$("#industryid").val(industryid);
			$("#industryname").val(industryname);
		}
    });
}

/*================================================================================================
	No           : 58
	Service Name : saveindustry
	Purpose      : update industry detail
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

$("form[name='saveindustry']").validate({
    rules: {
		industryname: {
			required: true
		}
	},
	messages: {
		industryname: {
			required: "<span class='text-red'>Please provide Industry Name</span>"
		}
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);

		var industryid = $("#industryid").val();
		var industryname = $("#industryname").val();
		var datapass = {"industryid":industryid,"industryname":industryname};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=saveindustry',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					localStorage.setItem('saveindustry', 'yes');

					window.location.href = 'industry.php';
		        } else {
					var message = value.message;

					setTimeout(function() {
				        toastr.options = {
				            closeButton: true,
				            progressBar: true,
				            positionClass: 'toast-bottom-right',
				            showMethod: 'slideDown',
				            timeOut: 4000
				        };
				        toastr.error('Please Try Again', message);
				    }, 1300);

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 59
	Service Name : deleteindustry
	Purpose      : delete industry
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

function deleteIndustry(dd){
	var industryid = $(dd).data('industryid');
	var industryname = $(dd).data('industryname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete industry <span style="font-weight: bold;">'+industryname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"industryid":industryid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deleteindustry',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Industry <span style="font-weight: bold;">'+industryname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Industry <span style="font-weight: bold;">'+industryname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        }
	        }
	    });
    });
}

/*================================================================================================
	No           : 60
	Service Name : appendcity
	Purpose      : append city based on state
	Create By    : Aavin Seth
	Created On   : 23-02-2018
================================================================================================*/

function appendCity(dis) {
	var countryname = $(dis).val();
	var countryid = $(dis).find(':selected').attr('data-countryid');

	var datapass = {"countryid":countryid};
	$.ajax({
        type: 'POST',
        url : 'admin-service.php?servicename=appendcity',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
        success: function(data){
        	var value = JSON.parse(data);
        	var citylist;

			if(value.status == 'success')
	        {
	        	$('#city').find("option:gt(0)").remove();
			    if(value.cities.length>0){
                    for(var val=0;val<value.cities.length;val++)
                    {
                        citylist += '<option data-cityid="'+value.cities[val].cityid+'" value="'+value.cities[val].cityname+'">'+value.cities[val].cityname+'</option>';
                    }
                    $("#city").append(citylist);
                    $('#city').trigger("chosen:updated");
                }
	        } else {
	        	$('#city').find("option:gt(0)").remove();
	        }
        }
    });
}

/*================================================================================================
	No           : 61
	Service Name : getcity
	Purpose      : get city based on state
	Create By    : Aavin Seth
	Created On   : 23-02-2018
================================================================================================*/

function getCity(country,city) {
	var datapass = {"country":country};
	if(country!='')
	{
		$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=getcity',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
	        	var citylist;

				if(value.status == 'success')
		        {
		        	$('#city').find("option:gt(0)").remove();
				    if(value.cities.length>0){
	                    for(var val=0;val<value.cities.length;val++)
	                    {
	                        citylist += '<option data-cityid="'+value.cities[val].cityid+'" value="'+value.cities[val].cityname+'">'+value.cities[val].cityname+'</option>';
	                    }
	                    $("#city").append(citylist);
	                    $('#city').val(city).trigger("chosen:updated");
	                }
		        } else {
		        	$('#city').find("option:gt(0)").remove();
		        }
	        }
	    });
	}
}

/*================================================================================================
	No           : 62
	Service Name : addadmin
	Purpose      : add admin
	Create By    : Aavin Seth
	Created On   : 24-02-2018
================================================================================================*/

$("form[name='addadmin']").validate({
    rules: {
		firstname: {
			required: true,
			lettersonly: true
		},
		lastname: {
			required: true,
			lettersonly: true
		},
		phone: {
			required: true,
			digits: true
		},
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		firstname: {
			required: "<span class='text-red'>Please provide a First Name</span>",
			lettersonly: "<span class='text-red'>Letters and spaces only</span>"
		},
		lastname: {
			required: "<span class='text-red'>Please provide a Last Name</span>",
			lettersonly: "<span class='text-red'>Letters and spaces only</span>"
		},
		phone: {
			required: "<span class='text-red'>Please provide a Phone Number</span>",
			digits: "<span class='text-red'>Please enter a valid Phone Number</span>"
		},
		email: "<span class='text-red'>Please enter a Email</span>"
	},
	submitHandler: function(form) {

		$(".form-horizontal button[type=submit]").prop("disabled", true);
		$(".email-error").html('');
		$("#email").removeClass('error');
		$(".phone-error").html('');
		$("#phone").removeClass('error');

		var adminid = $("#adminid").val();
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if($("#profilepic").val()==''){
			var profilepic = '';
		} else {
			var profilepic = $("#preview").attr('src');
		}
		var isd = $("#isd").val();
		var phone = $("#phone").val();
		var email = $("#email").val();
		var datapass = {"adminid":adminid,"firstname":firstname,"lastname":lastname,"profilepic":profilepic,"isd":isd,"phone":phone,"email":email};
		
		$.ajax({
			type: 'POST',
			url: 'admin-service.php?servicename=addadmin',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			/*beforeSend: function()
		    {
		        $("#preloader").show();
		    },*/
			success:function(data)
			{
				var value = JSON.parse(data);
				
				if(value.status == 'success')
		        {
					$(".alert").hide();
					$(".email-error").hide();
					$(".phone-error").hide();

					localStorage.setItem('addadmin', 'yes');

					window.location.href = 'admin.php';
		        } else {
		        	$("#preloader").hide();
					var message = value.message;

					if(value.email == 'failure'){
						var emailerror = value.emailerror;

						$(".email-error").html('');
						$("#email").addClass('error');
						$(".email-error").html(emailerror);
						$(".email-error").show();
					}

					if(value.phone == 'failure'){
						var phoneerror = value.phoneerror;

						$(".phone-error").html('');
						$("#phone").addClass('error');
						$(".phone-error").html(phoneerror);
						$(".phone-error").show();
					}

					$(".form-horizontal button[type=submit]").prop("disabled", false);
		        }
			}
		});
    }
});

/*================================================================================================
	No           : 63
	Service Name : viewadmin
	Purpose      : displaying admin in datatable
	Create By    : Aavin Seth
	Created On   : 24-02-2018
================================================================================================*/

function viewAdmin(adminid){
	var currentadminid = adminid;
	$.ajax({
        type: 'POST',
        url: 'admin-service.php?servicename=viewadmin',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.admin.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.admin.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.admin[val].firstname+' '+value.admin[val].lastname;
                        i[val][2] = value.admin[val].phone;
                        i[val][3] = value.admin[val].email;
                        if(currentadminid!=value.admin[val].adminid)
                        {
                        	if(value.admin[val].admintype=='0')
		                    {
		                    	i[val][4] = '<div class="btn-group"><button type="button" data-adminname="'+value.admin[val].firstname+' '+value.admin[val].lastname+'" data-adminid="'+value.admin[val].adminid+'" data-currentadminid="'+currentadminid+'" onclick="deleteAdmin(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
		                    } else {
		                    	i[val][4] = '';
		                    }
                        } else {
                        	i[val][4] = '';
                        }
                        // if(currentadminid!=value.admin[val].adminid || value.admin[val].admintype=='0')
                        // {
                        // 	i[val][4] = '<div class="btn-group"><button type="button" data-adminname="'+value.admin[val].firstname+' '+value.admin[val].lastname+'" data-adminid="'+value.admin[val].adminid+'" data-currentadminid="'+currentadminid+'" onclick="deleteAdmin(this);" class="btn btn-danger"><i class="fa fa-trash"></i></button></div>';
                        // } else {
                        // 	i[val][4] = '';
                        // }
                        sr++;
                    }
                }
            }

            $('#viewadmin').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Admin Found"
                }
                /*dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Clients'},
                    {extend: 'pdf', title: 'Clients'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]*/
            }); 
        }
    });
}

/*================================================================================================
	No           : 64
	Service Name : deleteadmin
	Purpose      : delete admin
	Create By    : Aavin Seth
	Created On   : 28-02-2018
================================================================================================*/

function deleteAdmin(dc){
	var adminid = $(dc).data('adminid');
	var currentadminid = $(dc).data('currentadminid');
	var adminname = $(dc).data('adminname');
    swal({
        title: 'Are you sure?',
        text: 'You want to delete admin <span style="font-weight: bold;">'+adminname+'</span>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        html: true
    }, function () {
    	var datapass = {"adminid":adminid,"currentadminid":currentadminid};
    	$.ajax({
	        type: 'POST',
	        url : 'admin-service.php?servicename=deleteadmin',
	        datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
	        success: function(data){
	        	var value = JSON.parse(data);
				if(value.status == 'success')
		        {
				    swal({
				        title: 'Deleted!',
				        text: 'Admin <span style="font-weight: bold;">'+adminname+'</span> has been deleted',
				        type: 'success',
				        closeOnConfirm: false,
				        html: true
				    }, function () {
				    	location.reload();
				    });
		        } else {
		        	swal({
						title: 'Error',
						text: 'Admin <span style="font-weight: bold;">'+adminname+'</span> has not deleted',
						type: 'error',
						html: true
					});
		        	// swal('Error', 'Driver '+clientname+' has not deleted', "error");
		        }
	        }
	    });
    });
}

/*================================================================================================
	Functions: on ready functions
================================================================================================*/
$(document).ready(function() {
	var adminname = localStorage.getItem("adminname");

	$('body').addClass('fixed-sidebar');
	$(".footer").addClass('fixed');
    $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });

    //Login Welcome Message
    var welcomenotification = localStorage.getItem("welcomenotification");
    if (welcomenotification == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Welcome to CAREERSBAY', adminname);
	        localStorage.removeItem('welcomenotification');
	    }, 1300);
	}

	//Add client confirmation message
	var addclient = localStorage.getItem("addclient");
	var clientname = localStorage.getItem("clientname");
    if (addclient == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Client Added Successfully!',clientname);
	        localStorage.removeItem('addclient');
	        localStorage.removeItem('clientname');
	    }, 1300);
	}

	//Save client confirmation message
	var saveclient = localStorage.getItem("saveclient");
	var clientname = localStorage.getItem("clientname");
    if (saveclient == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Client Update Successfully!',clientname.toUpperCase());
	        localStorage.removeItem('saveclient');
	        localStorage.removeItem('clientname');
	    }, 1300);
	}

	//admin change password confirmation message
	var changepassword = localStorage.getItem("changepassword");
    if (changepassword == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Password Update Successfully!','');
	        localStorage.removeItem('changepassword');
	    }, 1300);
	}

	//forget password confirmation message
	var forgetpassword = localStorage.getItem("forgetpassword");
    if (forgetpassword == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('New Password Send Successfully!','');
	        localStorage.removeItem('forgetpassword');
	    }, 1300);
	}

	//activation or changepassword check
	var activationpassword = localStorage.getItem("activationpassword");
    if (activationpassword == 'yes') {
    	$('#nav-tab-1').removeClass('active');
    	$('#tab-1').removeClass('active');
    	$('#nav-tab-2').addClass('active');
    	$('#tab-2').addClass('active');
    	$('#redirectto').val('dashboard');
	    localStorage.removeItem('activationpassword');
	}

	//Add banners confirmation message
	var addbanner = localStorage.getItem("addbanner");
    if (addbanner == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Banner Added Successfully!','');
	        localStorage.removeItem('addbanner');
	    }, 1300);
	}

	//Save banners confirmation message
	var savebanner = localStorage.getItem("savebanner");
    if (savebanner == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Banner Update Successfully!','');
	        localStorage.removeItem('savebanner');
	    }, 1300);
	}

	//Add job title confirmation message
	var addjd = localStorage.getItem("addjd");
    if (addjd == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Job Title Added Successfully!','');
	        localStorage.removeItem('addjd');
	    }, 1300);
	}

	//Save job title confirmation message
	var savejd = localStorage.getItem("savejd");
    if (savejd == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Job Title Update Successfully!','');
	        localStorage.removeItem('savejd');
	    }, 1300);
	}

	//Add job confirmation message
	var addjob = localStorage.getItem("addjob");
    if (addjob == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Job Added Successfully!','');
	        localStorage.removeItem('addjob');
	    }, 1300);
	}

	//Save job confirmation message
	var savejob = localStorage.getItem("savejob");
    if (savejob == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Job Update Successfully!','');
	        localStorage.removeItem('savejob');
	    }, 1300);
	}

	//Save job confirmation message
	var saveresume = localStorage.getItem("saveresume");
    if (saveresume == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Resume Update Successfully!','');
	        localStorage.removeItem('saveresume');
	    }, 1300);
	}

	//Save job confirmation message
	var addresume = localStorage.getItem("addresume");
    if (addresume == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Resume added Successfully!','');
	        localStorage.removeItem('addresume');
	    }, 1300);
	}

	//Add designation confirmation message
	var adddesignation = localStorage.getItem("adddesignation");
    if (adddesignation == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Designation Added Successfully!','');
	        localStorage.removeItem('adddesignation');
	    }, 1300);
	}

	//Save designation confirmation message
	var savedesignation = localStorage.getItem("savedesignation");
    if (savedesignation == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Designation Update Successfully!','');
	        localStorage.removeItem('savedesignation');
	    }, 1300);
	}

	//Add qualification confirmation message
	var addqualification = localStorage.getItem("addqualification");
    if (addqualification == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Qualification Added Successfully!','');
	        localStorage.removeItem('addqualification');
	    }, 1300);
	}

	//Save qualification confirmation message
	var savequalification = localStorage.getItem("savequalification");
    if (savequalification == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Qualification Update Successfully!','');
	        localStorage.removeItem('savequalification');
	    }, 1300);
	}

	//Add skillsets confirmation message
	var addskillsets = localStorage.getItem("addskillsets");
    if (addskillsets == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Skill Sets Added Successfully!','');
	        localStorage.removeItem('addskillsets');
	    }, 1300);
	}

	//Save skillsets confirmation message
	var saveskillsets = localStorage.getItem("saveskillsets");
    if (saveskillsets == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Skill Sets Update Successfully!','');
	        localStorage.removeItem('saveskillsets');
	    }, 1300);
	}

	//Add industry confirmation message
	var addindustry = localStorage.getItem("addindustry");
    if (addindustry == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Industry Added Successfully!','');
	        localStorage.removeItem('addindustry');
	    }, 1300);
	}

	//Save industry confirmation message
	var saveindustry = localStorage.getItem("saveindustry");
    if (saveindustry == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Industry Update Successfully!','');
	        localStorage.removeItem('saveindustry');
	    }, 1300);
	}

	//Add admin confirmation message
	var addadmin = localStorage.getItem("addadmin");
    if (addadmin == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Admin Added Successfully!','');
	        localStorage.removeItem('addadmin');
	    }, 1300);
	}

	//Save admin confirmation message
	var saveadmin = localStorage.getItem("saveadmin");
    if (saveadmin == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Admin Update Successfully!','');
	        localStorage.removeItem('saveadmin');
	    }, 1300);
	}
});

//Textfield validation for letters and space
jQuery.validator.addMethod("lettersonly", function(value, element) 
{
	return this.optional(element) || /^[a-z ']+$/i.test(value);
}, "Letters and spaces only");

//Textfield validation for alphanumeric and space
jQuery.validator.addMethod("alphanumeric", function(value, element) 
{
	return this.optional(element) || /^[a-z0-9 ']+$/i.test(value);
}, "Letters, Numbers and spaces only");