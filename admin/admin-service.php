<?php
session_start();

/*================================================================================================
	File Name : admin-service.php
	Purpose   : It contains methods for careersbay admin panel
	Create By : Aavin Seth
	Created On: 02-12-2017
================================================================================================*/

header("Access-Control-Allow-Origin:*");

/*================================================================================================
										Database Configuration
================================================================================================*/

include('../config.php');

/*================================================================================================
	No           : 1
	Service Name : adminlogin
	Purpose      : admin login code
	Create By    : Aavin Seth
	Created On   : 02-12-2017
================================================================================================*/

if($_GET['servicename'] == 'adminlogin')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$email = $json['email'];
	$password = $json['password'];
	$response = $json["captcha"];
	$cdate = date('Y-m-d H:i:s');

	// Google reCaptcha verify
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$privatekey = '6Lc6tDwUAAAAAGWSZAKejwGpvGTAtr3eKAoPAa-P';
	$verifyurl = $url."?secret=".$privatekey."&response=".$response."&remoteip=".$_SERVER['REMOTE_ADDR'];
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $verifyurl
	));
	$resp = curl_exec($curl);
	curl_close($curl);
	$data = json_decode($resp);

	$jsonarray = array();

	if(isset($data->success) AND $data->success==true)
	{
		$quead = "SELECT * FROM admin WHERE email='$email' AND accessed='1'";
		$excad = db($db,$quead);
		if(mysqli_num_rows($excad) > 0)
		{
			$rowad = mysqli_fetch_assoc($excad);

			if($rowad['password'] == $password)
			{
				$queadu = "UPDATE admin SET lastlogin='$cdate' WHERE email='$email'";
				$excadu = db($db,$queadu);

				$jsonarray['adminid'] = $rowad['adminid'];
				$jsonarray['email'] = $rowad['email'];
				$jsonarray['name'] = ucfirst($rowad['firstname']).' '.ucfirst($rowad['lastname']);
				$jsonarray['profilepic'] = $rowad['profilepic'];
				$_SESSION['adminprofile'] = $jsonarray;
				$jsonarray['status'] = 'success';
				$jsonarray['activation'] = $rowad['activation'];
				$jsonarray['message'] = 'Login Successfully';
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['message'] = 'Email ID/Password does not match!';
			}	
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Email ID/Password does not match!';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Please Verify Captcha';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 2
	Service Name : addclient
	Purpose      : add client
	Create By    : Aavin Seth
	Created On   : 02-12-2017
================================================================================================*/

if($_GET['servicename'] == 'addclient')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientname = mysqli_real_escape_string($db,$json['clientname']);
	$profilepic = $json['clientlogo'];
	$address1 = mysqli_real_escape_string($db,$json['address1']);
	$address2 = mysqli_real_escape_string($db,$json['address2']);
	$country = mysqli_real_escape_string($db,$json['country']);
	$city = $json['city'];
	$isd = $json['isd'];
	$phone = $json['phone'];
	$email = $json['email'];
	$website = $json['website'];
	$establisheddate = $json['establisheddate'];
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = substr(str_shuffle($chars),0,8);
	$cdate = date('Y-m-d H:i:s');
	$subject = 'Welcome to Careers Bay';

	if($profilepic != "")
	{
		$type1 = substr($profilepic, 11, strpos($profilepic, ';')-11);
		$profilepicname = uniqid().".".$type1;
	} else {
		$profilepicname = '';
	}

	$jsonarray = array();

	$queuue = "SELECT email FROM client WHERE email='$email' AND accessed='1'";
	$excuue = db($db,$queuue);
	if(mysqli_num_rows($excuue) == 0)
	{
		$emailflag = '1';
		$jsonarray['email'] = 'success';
	}
	else
	{
		$emailflag = '';
		$jsonarray['email'] = 'failure';
		$jsonarray['emailerror'] = 'Email Id Already Registered';
	}

	$queuum = "SELECT phone FROM client WHERE isdcode='$isd' AND phone='$phone' AND accessed='1'";
	$excuum = db($db,$queuum);
	if(mysqli_num_rows($excuum) == 0)
	{
		$phoneflag = '1';
		$jsonarray['phone'] = 'success';
	}
	else
	{
		$phoneflag = '';
		$jsonarray['phone'] = 'failure';
		$jsonarray['phoneerror'] = 'Mobile Number Already Registered';
	}

	if($emailflag=='1' && $phoneflag=='1')
	{
		$quecli = "INSERT INTO client SET clientname='$clientname',profilepic='$profilepicname',address1='$address1',address2='$address2',country='$country',city='$city',isdcode='$isdcode',phone='$phone',email='$email',password='$password',website='$website',establisheddate='$establisheddate',createdate='$cdate'";
		$exccli = db($db,$quecli);

		if($exccli)
		{
			if($profilepic != '')
			{
				$data = $profilepic;
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				file_put_contents('../assets/images/profilepic/'.$profilepicname, $data);
			}

			$content = '<!DOCTYPE html>
				<html>
				<body>
					<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
						<tr>
							<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
								<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
									<tr>
										<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
											<a href="'.$sitelink.'"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>
										</td>
									</tr>
									<tr>
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">
														'.$subject.'<br>
														<span style="font-size: 12px;">Please find your login credentials for Careers Bay</span>
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">
														<hr size="1" color="#eeeff0">
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Email:</strong> '.$email.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Password:</strong> '.$password.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong><a href="'.$sitelink.'client/">Click Here</a></strong> to login or copy paste '.$sitelink.'client/ in browser.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
														<b>CAREERSBAY</b>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>';

			include('../sendmail.php');
			sendMail($content,$email,$clientname,$subject,'','','');

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Client Added Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Add Client';
		}
	}
	else
	{
		$jsonarray['email'] = 'failure';
		$jsonarray['message'] = 'Check Error';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 3
	Service Name : viewclients
	Purpose      : display clients in datatable
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewclients')
{
	$que = "SELECT * FROM client WHERE accessed='1' ORDER BY clientname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['clients'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['clientid'] = $row['clientid'];
			$ary['clientname'] = ucwords($row['clientname']);
			$ary['profilepic'] = $row['profilepic'];
			$ary['phone'] = $row['phone'];
			$ary['email'] = $row['email'];
			$ary['city'] = ucwords($row['city']);
			$ary['status'] = $row['status'];

			array_push($jsonarray['clients'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Client Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Clients Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 4
	Service Name : updateclientstatus
	Purpose      : update client status
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'updateclientstatus')
{
	$json = file_get_contents('php://input');
	$jsondata = json_decode($json,true);

	$clientid = $jsondata['clientid'];
	if($jsondata['checked']=='true')
	{
		$checked = '1';
	}
	else
	{
		$checked = '0';
	}

	$jsonarray = array();

	$que = "UPDATE client SET status='$checked' WHERE clientid='$clientid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Status updated Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to update status';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 5
	Service Name : getclient
	Purpose      : get client details in modal
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getclient')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];

	$jsonarray = array();

	$que = "SELECT * FROM client WHERE clientid='$clientid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Client Detail Available';
		$jsonarray['clientid'] = $row['clientid'];
		$jsonarray['clientname'] = $row['clientname'];
		$jsonarray['profilepic'] = $row['profilepic'];
		$jsonarray['address1'] = $row['address1'];
		$jsonarray['address2'] = $row['address2'];
		$jsonarray['city'] = $row['city'];
		$jsonarray['country'] = $row['country'];
		$jsonarray['isd'] = $row['isdcode'];
		$jsonarray['phone'] = $row['phone'];
		$jsonarray['email'] = $row['email'];
		$jsonarray['website'] = $row['website'];
		$jsonarray['establisheddate'] = $row['establisheddate'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Client Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 6
	Service Name : saveclient
	Purpose      : update client detail
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'saveclient')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];
	$clientname = mysqli_real_escape_string($db,$json['clientname']);
	$profilepic = $json['profilepic'];
	$oldprofilepic = $json['oldprofilepic'];
	$address1 = mysqli_real_escape_string($db,$json['address1']);
	$address2 = mysqli_real_escape_string($db,$json['address2']);
	$country = $json['country'];
	$city = $json['city'];
	$isd = $json['isd'];
	$oldisd = $json['oldisd'];
	$phone = $json['phone'];
	$oldphone = $json['oldphone'];
	$email = $json['email'];
	$oldemail = $json['oldemail'];
	$website = mysqli_real_escape_string($db,$json['website']);
	$establisheddate = $json['establisheddate'];

	$jsonarray = array();

	if($email != $oldemail)
	{
		$queuue = "SELECT email FROM client WHERE email='$email' AND accessed='1'";
		$excuue = db($db,$queuue);
		if(mysqli_num_rows($excuue) == 0)
		{
			$emailflag = '1';
			$emailchange = '1';
			$jsonarray['email'] = 'success';
		}
		else
		{
			$emailflag = '';
			$jsonarray['email'] = 'failure';
			$jsonarray['emailerror'] = 'Email Id Already Registered';
		}
	}
	else
	{
		$emailflag = '1';
		$emailchange = '0';
	}

	if($phone != $oldphone || $isd != $oldisd)
	{
		$queuum = "SELECT phone FROM client WHERE isdcode='$isd' AND phone='$phone' AND accessed='1'";
		$excuum = db($db,$queuum);
		if(mysqli_num_rows($excuum) == 0)
		{
			$phoneflag = '1';
			$phonechange = '1';
			$jsonarray['phone'] = 'success';
		}
		else
		{
			$phoneflag = '';
			$jsonarray['phone'] = 'failure';
			$jsonarray['phoneerror'] = 'Mobile Number Already Registered';
		}
	}
	else
	{
		$phoneflag = '1';
		$phonechange = '0';
	}

	if($profilepic == "")
	{
		$profilepicname = $oldprofilepic;
	}
	else
	{
		$type1 = substr($profilepic, 11, strpos($profilepic, ';')-11);
		$profilepicname = uniqid().".".$type1;
	}

	if($emailflag=='1' && $phoneflag=='1')
	{
		if($emailchange=='1' && $phonechange=='0')
		{
			$que = "UPDATE client SET clientname='$clientname',profilepic='$profilepicname',address1='$address1',address2='$address2',country='$country',city='$city',email='$email',website='$website',establisheddate='$establisheddate' WHERE clientid='$clientid'";
			$exc = db($db,$que);
		}
		else if($emailchange=='0' && $phonechange=='1')
		{
			$que = "UPDATE client SET clientname='$clientname',profilepic='$profilepicname',address1='$address1',address2='$address2',country='$country',city='$city',isdcode='$isd',phone='$phone',website='$website',establisheddate='$establisheddate' WHERE clientid='$clientid'";
			$exc = db($db,$que);
		}
		else if($emailchange=='1' && $phonechange=='1')
		{
			$que = "UPDATE client SET clientname='$clientname',profilepic='$profilepicname',address1='$address1',address2='$address2',country='$country',city='$city',isdcode='$isd',phone='$phone',email='$email',website='$website',establisheddate='$establisheddate' WHERE clientid='$clientid'";
			$exc = db($db,$que);
		}
		else
		{
			$que = "UPDATE client SET clientname='$clientname',profilepic='$profilepicname',address1='$address1',address2='$address2',country='$country',city='$city',website='$website',establisheddate='$establisheddate' WHERE clientid='$clientid'";
			$exc = db($db,$que);
		}

		if($exc)
		{
			if($profilepic != '')
			{
				if($oldprofilepic != '')
				{
					$oldfile = "../assets/images/profilepic/".$oldprofilepic;
					unlink($oldfile);
				}
				$data = $profilepic;
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				file_put_contents('../assets/images/profilepic/'.$profilepicname, $data);
			}

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Client update Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To update Client';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Check Error';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 7
	Service Name : deleteclient
	Purpose      : delete client
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'deleteclient')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$quecl = "SELECT * FROM client WHERE clientid='$clientid' AND accessed='1'";
	$exccl = db($db,$quecl);
	if(mysqli_num_rows($exccl) > 0)
	{
		$quecld = "UPDATE client SET accessed='0',status='0',deletedate='$cdate' WHERE clientid='$clientid'";
		$exccld = db($db,$quecld);
		if($exccld)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Client Status Updated Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Client';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Client';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 8
	Service Name : getprofile
	Purpose      : get admin details in edit profile
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getprofile')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$adminid = $json['adminid'];

	$jsonarray = array();

	$que = "SELECT * FROM admin WHERE adminid='$adminid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Admin Detail Available';
		$jsonarray['adminid'] = $row['adminid'];
		$jsonarray['firstname'] = $row['firstname'];
		$jsonarray['lastname'] = $row['lastname'];
		$jsonarray['profilepic'] = $row['profilepic'];
		$jsonarray['isd'] = $row['isdcode'];
		$jsonarray['phone'] = $row['phone'];
		$jsonarray['email'] = $row['email'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Admin Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 9
	Service Name : saveprofile
	Purpose      : update admin detail from edit profile
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'saveprofile')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$adminid = $json['adminid'];
	$firstname = mysqli_real_escape_string($db,$json['firstname']);
	$lastname = mysqli_real_escape_string($db,$json['lastname']);
	$profilepic = $json['profilepic'];
	$oldprofilepic = $json['oldprofilepic'];
	$isd = $json['isd'];
	$oldisd = $json['oldisd'];
	$phone = $json['phone'];
	$oldphone = $json['oldphone'];
	$email = $json['email'];
	$oldemail = $json['oldemail'];

	$jsonarray = array();

	if($email != $oldemail)
	{
		$queuue = "SELECT email FROM admin WHERE email='$email'";
		$excuue = db($db,$queuue);
		if(mysqli_num_rows($excuue) == 0)
		{
			$emailflag = '1';
			$emailchange = '1';
			$jsonarray['email'] = 'success';
		}
		else
		{
			$emailflag = '';
			$jsonarray['email'] = 'failure';
			$jsonarray['emailerror'] = 'Email Id Already Registered';
		}
	}
	else
	{
		$emailflag = '1';
		$emailchange = '0';
	}

	if($isd != $oldisd || $phone != $oldphone)
	{
		$queuum = "SELECT phone FROM admin WHERE isdcode='$isd' AND phone='$phone'";
		$excuum = db($db,$queuum);
		if(mysqli_num_rows($excuum) == 0)
		{
			$phoneflag = '1';
			$phonechange = '1';
			$jsonarray['phone'] = 'success';
		}
		else
		{
			$phoneflag = '';
			$jsonarray['phone'] = 'failure';
			$jsonarray['phoneerror'] = 'Mobile Number Already Registered';
		}
	}
	else
	{
		$phoneflag = '1';
		$phonechange = '0';
	}

	if($profilepic == "")
	{
		$profilepicname = $oldprofilepic;
	}
	else
	{
		$type1 = substr($profilepic, 11, strpos($profilepic, ';')-11);
		$profilepicname = uniqid().".".$type1;
	}

	if($emailflag=='1' && $phoneflag=='1')
	{
		if($emailchange=='1' && $phonechange=='0')
		{
			$queclu = "UPDATE admin SET firstname='$firstname',lastname='$lastname',profilepic='$profilepicname',email='$email' WHERE adminid='$adminid'";
			$excclu = db($db,$queclu);
		}
		else if($emailchange=='0' && $phonechange=='1')
		{
			$queclu = "UPDATE admin SET firstname='$firstname',lastname='$lastname',profilepic='$profilepicname',isdcode='$isd',phone='$phone' WHERE adminid='$adminid'";
			$excclu = db($db,$queclu);
		}
		else if($emailchange=='1' && $phonechange=='1')
		{
			$queclu = "UPDATE admin SET firstname='$firstname',lastname='$lastname',profilepic='$profilepicname',isdcode='$isd',phone='$phone',email='$email' WHERE adminid='$adminid'";
			$excclu = db($db,$queclu);
		}
		else
		{
			$queclu = "UPDATE admin SET firstname='$firstname',lastname='$lastname',profilepic='$profilepicname' WHERE adminid='$adminid'";
			$excclu = db($db,$queclu);
		}

		if($excclu)
		{
			if($profilepic != "")
			{
				if($oldprofilepic != '')
				{
					$oldfile = "../assets/images/profilepic/".$oldprofilepic;
					unlink($oldfile);
				}
				$data = $profilepic;
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				file_put_contents('../assets/images/profilepic/'.$profilepicname, $data);
				$_SESSION['adminprofile']['profilepic'] = $profilepicname;
			}

			$_SESSION['adminprofile']['name'] = ucfirst($firstname).' '.ucfirst($lastname);

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Profile update Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To update Profile';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Check Error';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 10
	Service Name : changepassword
	Purpose      : changepassword of client
	Create By    : Aavin seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'changepassword')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$adminid = $json['adminid'];
	$oldpassword = $json['oldpassword'];
	$newpassword = $json['newpassword'];
	$confirmpassword = $json['confirmpassword'];

	$jsonarray = array();

	$que = "SELECT * FROM admin WHERE adminid='$adminid'";
	$exc = db($db,$que);
	$row = mysqli_fetch_assoc($exc);

	if ($row['password'] == $oldpassword)
	{
		if ($row['password'] != $newpassword)
		{
			if($newpassword == $confirmpassword)
			{
				$queadu = "UPDATE admin SET password='$newpassword',activation='1' WHERE adminid='$adminid'";
				$excadu = db($db,$queadu);
				if($excadu)
				{
					$jsonarray['status'] = 'success';
					$jsonarray['message'] = 'Password Updated Successfully';
				}
				else
				{
					$jsonarray['status'] = 'failure';
					$jsonarray['message'] = 'Not Able To Update';
				}
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['message'] = 'New Password and Confirm Password do not match';
			}
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'New password cannot be old password';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Password do not match';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 11
	Service Name : forgetpassword
	Purpose      : forgetpassword
	Create By    : Aavin seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'forgetpassword')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$email = $json['email'];
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = substr(str_shuffle($chars),0,8);
	// $hashpassword = md5($password);
	$subject = 'Password Change Alert';

	$jsonarray = array();

	$quead = "SELECT * FROM admin WHERE email='$email'";
	$excad = db($db,$quead);
	if(mysqli_num_rows($excad) > 0)
	{
		$rowad = mysqli_fetch_assoc($excad);
		if($rowad['email'] == $email)
		{
			$adminname = ucfirst($rowad['firstname']).' '.ucfirst($rowad['lastname']);
			$queadu = "UPDATE admin SET password='$password',activation='0' WHERE email='$email'";
			$excadu = db($db,$queadu);

			$content = '<!DOCTYPE html>
				<html>
				<body>
					<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
						<tr>
							<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
								<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
									<tr>
										<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
											<a href="http://careersbay.com/"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>
										</td>
									</tr>
									<tr>
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">
														'.$subject.'<br>
														<span style="font-size: 12px;">Please find your login credentials for Careers Bay</span>
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">
														<hr size="1" color="#eeeff0">
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Email:</strong> '.$email.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Password:</strong> '.$password.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong><a href="'.$sitelink.'admin/">Click Here</a></strong> to login or copy paste '.$sitelink.'admin/ in browser.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
														<b>CAREERSBAY</b>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>';

			include('../sendmail.php');
			sendMail($content,$email,$adminname,$subject,'','','');

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Password sent to Register Email Id';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Email Id does not match!';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Email Id does not match!';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 12
	Service Name : addbanner
	Purpose      : addbanner
	Create By    : Aavin Seth
	Created On   : 04-12-2017
================================================================================================*/

if($_GET['servicename'] == 'addbanner')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$banners = $json['banners'];

	$jsonarray = array();

	if(count($banners) != 0)
	{
		for($i=0;$i<count($banners);$i++)
		{
			if($banners[$i] != '')
			{
				$image = $banners[$i]['image'];
				$message = mysqli_real_escape_string($db,$banners[$i]['message']);

				$type1 = substr($image, 11, strpos($image, ';')-11);
				$imagename = uniqid().".".$type1;

				$queadi = "INSERT INTO banners SET message='$message',image='$imagename'";
				$excadi = db($db,$queadi);
				if($excadi)
				{
					$data = $image;
					list($type, $data) = explode(';', $data);
					list(, $data)      = explode(',', $data);
					$data = base64_decode($data);
					file_put_contents('../assets/images/banners/'.$imagename, $data);

					$jsonarray['status'] = 'success';
					$jsonarray['message'] = 'Banner Added';
				}
			}
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Check Error';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 13
	Service Name : viewbanners
	Purpose      : display banners in datatable
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewbanners')
{
	$que = "SELECT * FROM banners";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['banners'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['bannerid'] = $row['bannerid'];
			$ary['image'] = $row['image'];
			$ary['message'] = $row['message'];

			array_push($jsonarray['banners'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Banners Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Banners Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 14
	Service Name : getbanner
	Purpose      : get banner details in modal
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getbanner')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$bannerid = $json['bannerid'];

	$jsonarray = array();

	$que = "SELECT * FROM banners WHERE bannerid='$bannerid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Banner Detail Available';
		$jsonarray['bannerid'] = $row['bannerid'];
		$jsonarray['bannertext'] = $row['message'];
		$jsonarray['image'] = $row['image'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Banner Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 15
	Service Name : savebanner
	Purpose      : update banner detail
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'savebanner')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$bannerid = $json['bannerid'];
	$bannertext = mysqli_real_escape_string($db,$json['bannertext']);
	$banner = $json['banner'];
	$oldbanner = $json['oldbanner'];

	$jsonarray = array();

	if($banner == "")
	{
		$bannername = $oldbanner;
	}
	else
	{
		$type1 = substr($banner, 11, strpos($banner, ';')-11);
		$bannername = uniqid().".".$type1;
	}

	$que = "UPDATE banners SET message='$bannertext',image='$bannername' WHERE bannerid='$bannerid'";
	$exc = db($db,$que);
	if($exc)
	{
		if($banner != '')
		{
			if($oldbanner != '')
			{
				$oldfile = "../assets/images/banners/".$oldbanner;
				unlink($oldfile);
			}
			$data = $banner;
			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);
			$data = base64_decode($data);
			file_put_contents('../assets/images/banners/'.$bannername, $data);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Banner update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Banner';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 16
	Service Name : deletebanner
	Purpose      : delete banner
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'deletebanner')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$bannerid = $json['bannerid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM banners WHERE bannerid='$bannerid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);
		$oldbanner = $row['image'];
		$qued = "DELETE FROM banners WHERE bannerid='$bannerid'";
		$excd = db($db,$qued);
		if($excd)
		{
			if($oldbanner != '')
			{
				$oldfile = "../assets/images/banners/".$oldbanner;
				unlink($oldfile);
			}
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Banner deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Banner';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Banner';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 17
	Service Name : addjd
	Purpose      : add job descriptions
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'addjd')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdname = $json['jdname'];
	$description = mysqli_real_escape_string($db,$json['description']);
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO jd SET jdname='$jdname',description='$description',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'JD Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add JD';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 18
	Service Name : viewjd
	Purpose      : display jd in datatable
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewjd')
{
	$que = "SELECT * FROM jd WHERE accessed='1' ORDER BY jdname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['jd'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['jdid'] = $row['jdid'];
			$ary['jdname'] = $row['jdname'];
			$ary['description'] = $row['description'];

			array_push($jsonarray['jd'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'JD Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No JD Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 19
	Service Name : getjd
	Purpose      : get jd details in modal
	Create By    : Aavin Seth
	Created On   : 05-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getjd')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdid = $json['jdid'];

	$jsonarray = array();

	$que = "SELECT * FROM jd WHERE jdid='$jdid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'JD Detail Available';
		$jsonarray['jdid'] = $row['jdid'];
		$jsonarray['jdname'] = $row['jdname'];
		$jsonarray['description'] = $row['description'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'JD Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 20
	Service Name : savejd
	Purpose      : update jd detail
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'savejd')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdid = $json['jdid'];
	$jdname = $json['jdname'];
	$description = mysqli_real_escape_string($db,$json['description']);

	$jsonarray = array();

	$que = "UPDATE jd SET jdname='$jdname',description='$description' WHERE jdid='$jdid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'JD update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update JD';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 21
	Service Name : deletejd
	Purpose      : delete jd
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'deletejd')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdid = $json['jdid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM jd WHERE jdid='$jdid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$qued = "UPDATE jd SET accessed='0',deletedate='$cdate' WHERE jdid='$jdid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'JD deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete JD';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete JD';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 22
	Service Name : clientjd
	Purpose      : get client name and jd name
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'clientjd')
{
	$jsonarray['client'] = array();
	$jsonarray['jd'] = array();

	$jsonarray['status'] = 'success';
	$jsonarray['message'] = 'Data Available';

	$que = "SELECT * FROM client WHERE accessed='1'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['clientid'] = $row['clientid'];
			$ary['clientname'] = $row['clientname'];

			array_push($jsonarray['client'], $ary);
		}
	}

	$que = "SELECT * FROM jd WHERE accessed='1'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['jdid'] = $row['jdid'];
			$ary['jdname'] = $row['jdname'];

			array_push($jsonarray['jd'], $ary);
		}
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 23
	Service Name : addjob
	Purpose      : add job
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'addjob')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jobpostname = $json['jobpostname'];
	$clientid = $json['clientid'];
	$jdid = $json['jdid'];
	$expiredate = $json['expiredate'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO jobpost SET jobpostname='$jobpostname',clientid='$clientid',jdid='$jdid',expiredate='$expiredate',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add Job';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 24
	Service Name : viewjob
	Purpose      : display job in datatable
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewjob')
{
	$cdate = date('Y-m-d');

	$que = "SELECT * FROM jobpost ORDER BY expiredate DESC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['jobpost'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$clientid = $row['clientid'];
			$expiredate = $row['expiredate'];
			$jdid = $row['jdid'];

			$que1 = "SELECT cl.clientname, jd.jdname FROM client AS cl, jd AS jd WHERE cl.clientid='$clientid' AND jd.jdid='$jdid'";
			$exc1 = db($db,$que1);
			$row1 = mysqli_fetch_assoc($exc1);

			$ary['jobpostid'] = $row['jobpostid'];
			$ary['jobpostname'] = $row['jobpostname'];
			$ary['joblink'] = strtolower(str_replace(' ', '-', $row['jobpostname']));
			$ary['clientname'] = $row1['clientname'];
			$ary['jdname'] = $row1['jdname'];
			$ary['expiredate'] = $row['expiredate'];
			$ary['status'] = $row['status'];
			if($expiredate > $cdate){
				$ary['expirestatus'] = '1';
			} else {
				$ary['expirestatus'] = '0';
			}

			array_push($jsonarray['jobpost'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Job Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 25
	Service Name : updatejobstatus
	Purpose      : update job status
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'updatejobstatus')
{
	$json = file_get_contents('php://input');
	$jsondata = json_decode($json,true);

	$jobpostid = $jsondata['jobpostid'];
	if($jsondata['checked']=='true')
	{
		$checked = '1';
	}
	else
	{
		$checked = '0';
	}

	$jsonarray = array();

	$que = "UPDATE jobpost SET status='$checked' WHERE jobpostid='$jobpostid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Status updated Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to update status';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 26
	Service Name : getjob
	Purpose      : get job details in modal
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getjob')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jobpostid = $json['jobpostid'];

	$jsonarray = array();

	$que = "SELECT * FROM jobpost WHERE jobpostid='$jobpostid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$clientid = $row['clientid'];
		$jdid = $row['jdid'];

		$que1 = "SELECT cl.clientname, jd.jdname FROM client AS cl, jd AS jd WHERE cl.clientid='$clientid' AND jd.jdid='$jdid'";
		$exc1 = db($db,$que1);
		$row1 = mysqli_fetch_assoc($exc1);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job Data Available';
		$jsonarray['jobpostid'] = $row['jobpostid'];
		$jsonarray['jobpostname'] = $row['jobpostname'];
		$jsonarray['clientid'] = $clientid;
		$jsonarray['clientname'] = $row1['clientname'];
		$jsonarray['jdid'] = $jdid;
		$jsonarray['jdname'] = $row1['jdname'];
		$jsonarray['expiredate'] = $row['expiredate'];
		$jsonarray['status'] = $row['status'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Job Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 27
	Service Name : savejob
	Purpose      : update jd detail
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'savejob')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jobpostid = $json['jobpostid'];
	$jobpostname = $json['jobpostname'];
	$clientid = $json['clientid'];
	$jdid = $json['jdid'];
	$expiredate = $json['expiredate'];

	$jsonarray = array();

	$que = "UPDATE jobpost SET jobpostname='$jobpostname',clientid='$clientid',jdid='$jdid',expiredate='$expiredate' WHERE jobpostid='$jobpostid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Job';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 28
	Service Name : deletejob
	Purpose      : delete job
	Create By    : Aavin Seth
	Created On   : 06-12-2017
================================================================================================*/

if($_GET['servicename'] == 'deletejob')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jobpostid = $json['jobpostid'];

	$jsonarray = array();

	$que = "SELECT * FROM jobpost WHERE jobpostid='$jobpostid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);
		$qued = "DELETE FROM jobpost WHERE jobpostid='$jobpostid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Job deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Job';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Job';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 29
	Service Name : viewresumes
	Purpose      : display resume in datatable
	Create By    : Aavin Seth
	Created On   : 13-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewresumes')
{
	$cdate = date('Y-m-d');

	$que = "SELECT * FROM resumes ORDER BY resumeid DESC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume Data Available';
		$jsonarray['resumes'] = array();
		$jsonarray['shortlistclient'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$jobpostid = $row['jobpostid'];
			$jdid = $row['jdid'];
			$clientid = $row['clientid'];
			$skillsets = explode(',',$row['skillsetsid']);
			$skillset = '';

			if($jobpostid!=0)
			{
				$que1 = "SELECT jd.jdname, jp.jobpostname FROM jd AS jd, jobpost AS jp WHERE jd.jdid='$jdid' AND jp.jobpostid='$jobpostid'";
			} else {
				$que1 = "SELECT jdname FROM jd WHERE jdid='$jdid'";
			}
			$exc1 = db($db,$que1);
			$row1 = mysqli_fetch_assoc($exc1);

			if($clientid!=0)
			{
				$que5 = "SELECT clientname FROM client WHERE clientid='$clientid'";
				$exc5 = db($db,$que5);
				$row5 = mysqli_fetch_assoc($exc5);
				$clientname = $row5['clientname'];
			} else {
				$clientname = 'NA';
			}

			$resumeid = $row['resumeid'];
			$ary['resumeid'] = $resumeid;
			$ary['name'] = ucwords($row['name']);
			$ary['email'] = $row['email'];
			$ary['isd'] = $row['isdcode'];
			$ary['phone'] = $row['phone'];
			$ary['country'] = $row['country'];
			$ary['nationality'] = ucfirst($row['nationality']);
			$ary['resume'] = $row['resume'];
			$ary['clientname'] = $clientname;

			$que3 = "SELECT clientid FROM resumeshortlist WHERE resumeid='$resumeid' GROUP BY clientid";
			$exc3 = db($db,$que3);
			if(mysqli_num_rows($exc3) > 0)
			{
				$slc = array();

				while($row3 = mysqli_fetch_assoc($exc3)){
					$clientid = $row3['clientid'];

					$que4 = "SELECT clientname FROM client WHERE clientid='$clientid'";
					$exc4 = db($db,$que4);
					$row4 = mysqli_fetch_assoc($exc4);

					array_push($slc, $row4['clientname']);
				}

				$ary['shortlistclient'] = implode(', ', $slc);

			} else {
				$ary['shortlistclient'] = 'NA';
			}

			if($jobpostid!=0)
			{
				$ary['jobpostname'] = $row1['jobpostname'];
			} else {
				$ary['jobpostname'] = 'NA';
			}
			$ary['jdname'] = $row1['jdname'];
			foreach ($skillsets as $key => $value) {
				$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
				$exc2 = db($db,$que2);
				$row2 = mysqli_fetch_assoc($exc2);

				$skillset = $skillset.' '.$row2['skillsetsname'].',';
			}
			$ary['skillsets'] = rtrim($skillset,',');
			$industryid = $row['industryid'];
			if($industryid!='0')
			{
				$que2 = "SELECT industryname FROM industry WHERE industryid='$industryid'";
				$exc2 = db($db,$que2);
				$row2 = mysqli_fetch_assoc($exc2);
				$ary['industryname'] = $row2['industryname'];
			} else {
				$ary['industryname'] = 'NA';
			}
			$ary['createdate'] = $row['createdate'];

			array_push($jsonarray['resumes'], $ary);
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Resume Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 30
	Service Name : getresume
	Purpose      : get resume details in modal
	Create By    : Aavin Seth
	Created On   : 14-12-2017
================================================================================================*/

if($_GET['servicename'] == 'getresume')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$resumeid = $json['resumeid'];

	$jsonarray = array();

	$que = "SELECT * FROM resumes WHERE resumeid='$resumeid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume Detail Available';
		$jsonarray['resumeid'] = $row['resumeid'];
		$jsonarray['name'] = $row['name'];
		$jsonarray['email'] = $row['email'];
		$jsonarray['isd'] = $row['isdcode'];
		$jsonarray['phone'] = $row['phone'];
		$jsonarray['country'] = $row['country'];
		$jsonarray['nationality'] = $row['nationality'];
		$jsonarray['designation'] = $row['designationid'];
		$jsonarray['qualification'] = $row['qualificationid'];
		$jsonarray['skillsets'] = $row['skillsetsid'];
		if($row['industryid']!='0')
		{
			$jsonarray['industryid'] = $row['industryid'];
		} else {
			$jsonarray['industryid'] = '';
		}
		$jsonarray['resume'] = $row['resume'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Resume Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 31
	Service Name : saveresume
	Purpose      : update candidate resume
	Create By    : Aavin Seth
	Created On   : 14-12-2017
================================================================================================*/

if($_GET['servicename'] == 'saveresume')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$resumeid = $json['resumeid'];
	$name = $json['candidatename'];
	$email = $json['email'];
	$phone = $json['phone'];
	$country = $json['country'];
	$nationality = $json['nationality'];
	$designation = $json['designation'];
	$qualification = $json['qualification'];
	$skillsets = implode(',',$json['skillsets']);
	$industry = $json['industry'];
	$oldresume = $json['oldresume'];
	$resume = $json['resume'];
	$resumeext = $json['resumeext'];
	$mime = $json['mime'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	if($resume == "")
	{
		$resumename = $oldresume;
	}
	else
	{
		$resumename = uniqid().".".$resumeext;
	}

	$que = "UPDATE resumes SET name='$name',email='$email',phone='$phone',country='$country',nationality='$nationality',designationid='$designation',qualificationid='$qualification',skillsetsid='$skillsets',industryid='$industry',resume='$resumename' WHERE resumeid='$resumeid'";
	$exc = db($db,$que);

	if($exc)
	{
		$skillsetse = explode(',',$skillsets);
		$quei1 = "DELETE FROM resumesskillsets WHERE resumeid='$resumeid'";
		$exci1 = db($db,$quei1);

		foreach ($skillsetse as $key => $value) {
			$quei = "INSERT INTO resumesskillsets SET resumeid='$resumeid',skillsetsid='$value',createdate='$cdate'";
			$exci = db($db,$quei);
		}

		if($resume != "")
		{
			if($oldresume != '')
			{
				$oldfile = "../assets/resumes/".$oldresume;
				unlink($oldfile);
			}
			$data = $resume;
			list($type, $data) = explode(';', $data);
			list(, $data) = explode(',', $data);
			$data = base64_decode($data);
			file_put_contents('../assets/resumes/'.$resumename, $data);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Resume';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 32
	Service Name : dashboard
	Purpose      : dashboard
	Create By    : Aavin seth
	Created On   : 18-12-2017
================================================================================================*/

if($_GET['servicename'] == 'dashboard')
{
	$cdate = date('Y-m-d H:i:s');

	$jsonarray['status'] = 'success';
	$jsonarray['message'] = 'Dashboard Available';

	$que = "SELECT COUNT(*) AS resume FROM resumes WHERE createdate BETWEEN DATE_SUB('$cdate', INTERVAL 7 DAY) AND '$cdate'";
	$exc = db($db,$que);
	$row = mysqli_fetch_assoc($exc);

	$jsonarray['resume'] = $row['resume'];

	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 33
	Service Name : addresume
	Purpose      : add resume
	Create By    : Aavin Seth
	Created On   : 18-12-2017
================================================================================================*/

if($_GET['servicename'] == 'addresume')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdid = $json['jdid'];
	$clientid = $json['clientid'];
	$jdname = $json['jdname'];
	$clientname = $json['clientname'];
	$candidatename = $json['candidatename'];
	$email = $json['email'];
	$isd = $json['isd'];
	$phone = $json['phone'];
	$country = $json['country'];
	$nationality = $json['nationality'];
	$industry = $json['industry'];
	$qualification = $json['qualification'];
	$skillsets = implode(',',$json['skillsets']);
	$resume = $json['resume'];
	$resumeext = $json['resumeext'];
	$mime = $json['mime'];
	$subject = $jdid.' / '.$jdname.' / '.$clientname;
	$cdate = date('Y-m-d H:i:s');
	$html = '';

	$jsonarray = array();
	
	if($resumeext=='pdf' || $resumeext=='doc' || $resumeext=='docx' || $resumeext=='')
	{
		$quer = "SELECT * FROM resumes WHERE email='$email'";
		$excr = db($db,$quer);
		if(mysqli_num_rows($excr) == 0)
		{
			if($resumeext!='')
			{
				$resumename = uniqid().".".$resumeext;
			} else {
				$resumename = uniqid().".pdf";
			}

			$quei = "INSERT INTO resumes SET name='$candidatename',email='$email',isdcode='$isd',phone='$phone',country='$country',nationality='$nationality',industryid='$industry',qualificationid='$qualification',skillsetsid='$skillsets',resume='$resumename',jobpostid='0',jdid='$jdid',clientid='$clientid',addedfrom='1',createdate='$cdate'";
			$exci = db($db,$quei);
			$resumeid = mysqli_insert_id($db);

			if($exci)
			{
				if($resumeext!='')
				{
					$data = $resume;
					list($type, $data) = explode(';', $data);
					list(, $data) = explode(',', $data);
					$data = base64_decode($data);
					file_put_contents('../assets/resumes/'.$resumename, $data);
				} else {
					require('../mpdf/mpdf/mpdf.php');
					$mpdf=new mPDF();
					$mpdf->WriteHTML($resume);
					$mpdf->Output('../assets/resumes/'.$resumename,'F');
				}

				$skillsetse = explode(',',$skillsets);
				$skillset = '';
				foreach ($skillsetse as $key => $value) {
					$quei = "INSERT INTO resumesskillsets SET resumeid='$resumeid',skillsetsid='$value',createdate='$cdate'";
					$exci = db($db,$quei);

					$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
					$exc2 = db($db,$que2);
					$row2 = mysqli_fetch_assoc($exc2);

					$skillset = $skillset.' '.$row2['skillsetsname'].',';
				}

				if($clientid!='0')
				{
					$que = "SELECT email FROM client WHERE clientid='$clientid'";
					$exc = db($db,$que);
					$row = mysqli_fetch_assoc($exc);
					$clientemail = $row['email'];

					$quedsn = "SELECT industryname FROM industry WHERE industryid='".$industry."'";
					$excdsn = mysqli_query($db,$quedsn);
					$rsdsn = mysqli_fetch_assoc($excdsn);

					$queqf = "SELECT qualificationname FROM qualification WHERE qualificationid='".$qualification."'";
					$excdf = mysqli_query($db,$queqf);
					$rsqf = mysqli_fetch_assoc($excdf);

					$html .='<!DOCTYPE html>'.
						'<html>'.
						'<body>'.
							'<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">'.
								'<tr>'.
									'<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">'.
										'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
											'<tr>'.
												'<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">'.
													'<a href="'.$sitelink.'"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>'.
												'</td>'.
											'</tr>'.
											'<tr>'.
												'<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">'.
													'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">'
																.$subject.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">'.
																'<hr size="1" color="#eeeff0">'.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>'.
														'</tr>'.
														'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Name:</strong> '.$candidatename.
															'</td>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Email:</strong> '.$email.
															'</td>'.
														'</tr>'.
							            				'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Phone:</strong> '.$isd.' '.$phone.
															'</td>'.
							              					'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Country:</strong> '.$country.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Industry:</strong> '.$rsdsn['industryname'].
															'</td>'.
							              					'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Qualification:</strong> '.$rsqf['qualificationname'].
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Skill Sets:</strong> '.$skillset.
															'</td>'.
														'</tr>'.
													'</table>'.
												'</td>'.
											'</tr>'.
											'<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">'.
												'<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">'.
													'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
														'<tr>'.
															'<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">'.
																'<b>CAREERSBAY</b>'.
															'</td>'.
														'</tr>'.
													'</table>'.
												'</td>'.
											'</tr>'.
										'</table>'.
									'</td>'.
								'</tr>'.
							'</table>'.
						'</body>'.
					'</html>';

					include('../sendmail.php');
					$sendmail = sendMail($html,$clientemail,'CAREERSBAY',$subject,$resumename,$resume,$mime);
				}

				// if($sendmail=='true')
				// {
					$jsonarray['status'] = 'success';
					$jsonarray['message'] = 'Application Submitted!';
				// }
				// else
				// {
				// 	$jsonarray['status'] = 'failure';
				// 	$jsonarray['attachment'] = 'success';
				// 	$jsonarray['message'] = 'Failed to Submit. Please Try again';
				// }
			}
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['attachment'] = 'success';
			$jsonarray['message'] = 'Application already Submitted!';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['attachment'] = 'failure';
		$jsonarray['message'] = 'Only pdf, doc &amp; docx are allowed';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 34
	Service Name : adddesignation
	Purpose      : add designation
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'adddesignation')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$designationname = mysqli_real_escape_string($db,$json['designationname']);
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO designation SET designationname='$designationname',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Designation Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add Designation';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 35
	Service Name : viewdesignation
	Purpose      : display designation in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'viewdesignation')
{
	$que = "SELECT * FROM designation WHERE accessed='1' ORDER BY designationname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['designation'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['designationid'] = $row['designationid'];
			$ary['designationname'] = $row['designationname'];

			array_push($jsonarray['designation'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Designation Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Designation Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 36
	Service Name : getdesignation
	Purpose      : get designation details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'getdesignation')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$designationid = $json['designationid'];

	$jsonarray = array();

	$que = "SELECT * FROM designation WHERE designationid='$designationid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Designation Detail Available';
		$jsonarray['designationid'] = $row['designationid'];
		$jsonarray['designationname'] = $row['designationname'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Designation Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 37
	Service Name : savedesignation
	Purpose      : update designation detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'savedesignation')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$designationid = $json['designationid'];
	$designationname = mysqli_real_escape_string($db,$json['designationname']);

	$jsonarray = array();

	$que = "UPDATE designation SET designationname='$designationname' WHERE designationid='$designationid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Designation update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Designation';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 38
	Service Name : deletedesignation
	Purpose      : delete designation
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'deletedesignation')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$designationid = $json['designationid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM designation WHERE designationid='$designationid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$qued = "UPDATE designation SET accessed='0',deletedate='$cdate' WHERE designationid='$designationid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Designation deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Designation';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Designation';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 39
	Service Name : addqualification
	Purpose      : add qualification
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'addqualification')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$qualificationname = mysqli_real_escape_string($db,$json['qualificationname']);
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO qualification SET qualificationname='$qualificationname',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Qualification Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add Qualification';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 40
	Service Name : viewqualification
	Purpose      : display qualification in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'viewqualification')
{
	$que = "SELECT * FROM qualification WHERE accessed='1' ORDER BY qualificationname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['qualification'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['qualificationid'] = $row['qualificationid'];
			$ary['qualificationname'] = $row['qualificationname'];

			array_push($jsonarray['qualification'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Qualification Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Qualification Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 41
	Service Name : getqualification
	Purpose      : get qualification details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'getqualification')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$qualificationid = $json['qualificationid'];

	$jsonarray = array();

	$que = "SELECT * FROM qualification WHERE qualificationid='$qualificationid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Qualification Detail Available';
		$jsonarray['qualificationid'] = $row['qualificationid'];
		$jsonarray['qualificationname'] = $row['qualificationname'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Qualification Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 42
	Service Name : savequalification
	Purpose      : update qualification detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'savequalification')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$qualificationid = $json['qualificationid'];
	$qualificationname = mysqli_real_escape_string($db,$json['qualificationname']);

	$jsonarray = array();

	$que = "UPDATE qualification SET qualificationname='$qualificationname' WHERE qualificationid='$qualificationid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Qualification update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Qualification';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 43
	Service Name : deletequalification
	Purpose      : delete qualification
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'deletequalification')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$qualificationid = $json['qualificationid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM qualification WHERE qualificationid='$qualificationid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$qued = "UPDATE qualification SET accessed='0',deletedate='$cdate' WHERE qualificationid='$qualificationid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Qualification deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Qualification';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Qualification';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 44
	Service Name : addskillsets
	Purpose      : add skillsets
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'addskillsets')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$skillsetsname = mysqli_real_escape_string($db,$json['skillsetsname']);
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO skillsets SET skillsetsname='$skillsetsname',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Skill Sets Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add Skill Sets';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 45
	Service Name : viewskillsets
	Purpose      : display skillsets in datatable
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'viewskillsets')
{
	$que = "SELECT * FROM skillsets WHERE accessed='1' ORDER BY skillsetsname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Skill Sets Data Available';
		$jsonarray['skillsets'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['skillsetsid'] = $row['skillsetsid'];
			$ary['skillsetsname'] = $row['skillsetsname'];

			array_push($jsonarray['skillsets'], $ary);
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Skill Sets Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 46
	Service Name : getskillsets
	Purpose      : get skillsets details in modal
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'getskillsets')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$skillsetsid = $json['skillsetsid'];

	$jsonarray = array();

	$que = "SELECT * FROM skillsets WHERE skillsetsid='$skillsetsid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Skill Sets Detail Available';
		$jsonarray['skillsetsid'] = $row['skillsetsid'];
		$jsonarray['skillsetsname'] = $row['skillsetsname'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Skill Sets Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 47
	Service Name : saveskillsets
	Purpose      : update skillsets detail
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'saveskillsets')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$skillsetsid = $json['skillsetsid'];
	$skillsetsname = mysqli_real_escape_string($db,$json['skillsetsname']);

	$jsonarray = array();

	$que = "UPDATE skillsets SET skillsetsname='$skillsetsname' WHERE skillsetsid='$skillsetsid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Skill Sets update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Skill Sets';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 48
	Service Name : deleteskillsets
	Purpose      : delete skillsets
	Create By    : Aavin Seth
	Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'deleteskillsets')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$skillsetsid = $json['skillsetsid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM skillsets WHERE skillsetsid='$skillsetsid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$qued = "UPDATE skillsets SET accessed='0',deletedate='$cdate' WHERE skillsetsid='$skillsetsid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Skill Sets deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Skill Sets';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Skill Sets';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 49
	Service Name : dqs
	Purpose      : append designation, qualification, skill set, industry, country name and country phone code
    Create By    : Aavin Seth
    Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'dqs')
{
	$jsonarray['designation'] = array();
	$jsonarray['qualification'] = array();
	$jsonarray['skillsets'] = array();
	$jsonarray['industry'] = array();
	$jsonarray['country'] = array();
	$jsonarray['countries'] = array();
	$jsonarray['email'] = array();

	$jsonarray['status'] = 'success';
	$jsonarray['message'] = 'Data Available';

	$qued = "SELECT * FROM designation WHERE accessed='1' ORDER BY designationname ASC";
	$excd = db($db,$qued);
	if(mysqli_num_rows($excd) > 0)
	{
		while($rowd = mysqli_fetch_assoc($excd))
		{
			$aryd = array();

			$aryd['designationid'] = $rowd['designationid'];
			$aryd['designationname'] = $rowd['designationname'];

			array_push($jsonarray['designation'], $aryd);
		}
	}

	$queq = "SELECT * FROM qualification WHERE accessed='1' ORDER BY qualificationname ASC";
	$excq = db($db,$queq);
	if(mysqli_num_rows($excq) > 0)
	{
		while($rowq = mysqli_fetch_assoc($excq))
		{
			$aryq = array();

			$aryq['qualificationid'] = $rowq['qualificationid'];
			$aryq['qualificationname'] = $rowq['qualificationname'];

			array_push($jsonarray['qualification'], $aryq);
		}
	}

	$ques = "SELECT * FROM skillsets WHERE accessed='1' ORDER BY skillsetsname ASC";
	$excs = db($db,$ques);
	if(mysqli_num_rows($excs) > 0)
	{
		while($rows = mysqli_fetch_assoc($excs))
		{
			$arys = array();

			$arys['skillsetsid'] = $rows['skillsetsid'];
			$arys['skillsetsname'] = $rows['skillsetsname'];

			array_push($jsonarray['skillsets'], $arys);
		}
	}

	$quecy = "SELECT * FROM countries ORDER BY countryname ASC";
	$exccy = db($db,$quecy);
	if(mysqli_num_rows($exccy) > 0)
	{
		while($rowcy = mysqli_fetch_assoc($exccy))
		{
			$aryc = array();

			$arycy['countryid'] = $rowcy['countryid'];
			$arycy['countryname'] = $rowcy['countryname'];
			$arycy['phonecode'] = $rowcy['phonecode'];

			array_push($jsonarray['country'], $arycy);
		}
	}

	$quec = "SELECT * FROM countries ORDER BY phonecode ASC";
	$excc = db($db,$quec);
	if(mysqli_num_rows($excc) > 0)
	{
		while($rowc = mysqli_fetch_assoc($excc))
		{
			$aryc = array();

			$aryc['countryid'] = $rowc['countryid'];
			$aryc['countryname'] = $rowc['countryname'];
			$aryc['phonecode'] = $rowc['phonecode'];

			array_push($jsonarray['countries'], $aryc);
		}
	}

	$quei = "SELECT * FROM industry WHERE accessed='1' ORDER BY industryname ASC";
	$exci = db($db,$quei);
	if(mysqli_num_rows($exci) > 0)
	{
		while($rowi = mysqli_fetch_assoc($exci))
		{
			$aryi = array();

			$aryi['industryid'] = $rowi['industryid'];
			$aryi['industryname'] = $rowi['industryname'];

			array_push($jsonarray['industry'], $aryi);
		}
	}

	$quee = "SELECT DISTINCT email FROM resumes ORDER BY email ASC";
	$exce = db($db,$quee);
	if(mysqli_num_rows($exce) > 0)
	{
		while($rowe = mysqli_fetch_assoc($exce))
		{
			$arye = array();

			$arye['email'] = $rowe['email'];

			array_push($jsonarray['email'], $arye);
		}
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 50
	Service Name : resumesfilter
	Purpose      : resumes filter
	Create By    : Aavin Seth
	Created On   : 24-01-2018
================================================================================================*/

if($_GET['servicename'] == 'resumesfilter')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$query = "1";
	$isd = $json['isd'];
	if($isd!='')
	{
		$query .=" AND isdcode='$isd'";
	}
	$country = $json['country'];
	if($country!='')
	{
		$query .= " AND country='$country'";
	}
	$email = $json['email'];
	if($email!='')
	{
		$query .= " AND email='$email'";
	}
	$clientid = $json['client'];
	if($clientid!='')
	{
		$query .= " AND clientid='$clientid'";
	}
	$jdid = $json['jd'];
	if($jdid!='')
	{
		$query .= " AND jdid='$jdid'";
	}
	$skillsets = $json['skillsets'];
	if($skillsets != null)
	{
		$skill = implode(',',$skillsets);
		if($skill!='')
		{
			$query .= " AND resumeid IN (SELECT rss.resumeid FROM resumesskillsets AS rss WHERE rss.skillsetsid IN ($skill))";
		}
	}
	$industryid = $json['industry'];
	if($industryid!='')
	{
		$query .= " AND industryid='$industryid'";
	}

	$jsonarray = array();

	$que = "SELECT * FROM resumes WHERE $query";
	// echo $que;
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume Data Available';
		$jsonarray['resumes'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$jobpostid = $row['jobpostid'];
			$jdid = $row['jdid'];
			$clientid = $row['clientid'];
			$skillsets = explode(',',$row['skillsetsid']);
			$skillset = '';

			if($jobpostid!=0)
			{
				$que1 = "SELECT jd.jdname, jp.jobpostname FROM jd AS jd, jobpost AS jp WHERE jd.jdid='$jdid' AND jp.jobpostid='$jobpostid'";
			} else {
				$que1 = "SELECT jdname FROM jd WHERE jdid='$jdid'";
			}
			$exc1 = db($db,$que1);
			$row1 = mysqli_fetch_assoc($exc1);

			$ary['resumeid'] = $row['resumeid'];
			$resumeid = $row['resumeid'];
			$ary['name'] = ucwords($row['name']);
			$ary['email'] = $row['email'];
			$ary['isd'] = $row['isdcode'];
			$ary['phone'] = $row['phone'];
			$ary['country'] = $row['country'];
			$ary['nationality'] = ucfirst($row['nationality']);
			$ary['resume'] = $row['resume'];

			if($clientid!=0)
			{
				$que5 = "SELECT clientname FROM client WHERE clientid='$clientid'";
				$exc5 = db($db,$que5);
				$row5 = mysqli_fetch_assoc($exc5);
				$ary['clientname'] = $row5['clientname'];
			} else {
				$ary['clientname'] = 'NA';
			}

			$que3 = "SELECT clientid FROM resumeshortlist WHERE resumeid='$resumeid' GROUP BY clientid";
			$exc3 = db($db,$que3);
			if(mysqli_num_rows($exc3) > 0)
			{
				$slc = array();

				while($row3 = mysqli_fetch_assoc($exc3)){
					$clientid = $row3['clientid'];

					$que4 = "SELECT clientname FROM client WHERE clientid='$clientid'";
					$exc4 = db($db,$que4);
					$row4 = mysqli_fetch_assoc($exc4);

					array_push($slc, $row4['clientname']);
				}

				$ary['shortlistclient'] = implode(', ', $slc);

			} else {
				$ary['shortlistclient'] = 'NA';
			}

			if($jobpostid!=0)
			{
				$ary['jobpostname'] = $row1['jobpostname'];
			} else {
				$ary['jobpostname'] = 'NA';
			}
			$ary['jdname'] = $row1['jdname'];
			foreach ($skillsets AS $key => $value) {
				$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
				$exc2 = db($db,$que2);
				$row2 = mysqli_fetch_assoc($exc2);

				$skillset = $skillset.' '.$row2['skillsetsname'].',';
			}
			$ary['skillsets'] = rtrim($skillset,',');
			$industryid = $row['industryid'];
			if($industryid!='0')
			{
				$que2 = "SELECT industryname FROM industry WHERE industryid='$industryid'";
				$exc2 = db($db,$que2);
				$row2 = mysqli_fetch_assoc($exc2);
				$ary['industryname'] = $row2['industryname'];
			} else {
				$ary['industryname'] = 'NA';
			}
			$ary['createdate'] = $row['createdate'];

			array_push($jsonarray['resumes'], $ary);
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No result Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 51
	Service Name : shortlist
	Purpose      : admin shortlist resume for clients
	Create By    : Aavin Seth
	Created On   : 25-01-2018
================================================================================================*/

if($_GET['servicename'] == 'shortlist')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$resumes = $json['resumes'];
	$clientid = $json['clientid'];
	$isdelete = 0;
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	for($i=0;$i<count($resumes);$i++)
	{
		$resumeid = $resumes[$i]['resumeid'];

		$que = "INSERT INTO resumeshortlist SET resumeid='$resumeid',clientid='$clientid',createdate='$cdate'";
		$exc = db($db,$que);
		if($exc)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Shortlist Created';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Failed to shortlist';
		}
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 52
	Service Name : viewshortlist
	Purpose      : display shortlist resume in datatable
	Create By    : Aavin Seth
	Created On   : 25-01-2018
================================================================================================*/

if($_GET['servicename'] == 'viewshortlist')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];

	$que = "SELECT resumeshortlistid,resumeid FROM resumeshortlist WHERE clientid='$clientid' AND isfinal='0' AND isdelete='0' ORDER BY resumeshortlistid DESC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume Data Available';
		$jsonarray['resumes'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$resumeid = $row['resumeid'];

			$que3 = "SELECT * FROM resumes WHERE resumeid='$resumeid'";
			$exc3 = db($db,$que3);
			if(mysqli_num_rows($exc3) > 0)
			{
				while($row3 = mysqli_fetch_assoc($exc3))
				{
					$ary = array();

					$jobpostid = $row3['jobpostid'];
					$jdid = $row3['jdid'];
					$clientid = $row3['clientid'];
					$skillsets = explode(',',$row3['skillsetsid']);
					$skillset = '';

					if($jobpostid!=0)
					{
						$que1 = "SELECT cl.clientname, jd.jdname, jp.jobpostname FROM client AS cl, jd AS jd, jobpost AS jp WHERE cl.clientid='$clientid' AND jd.jdid='$jdid' AND jp.jobpostid='$jobpostid'";
					} else {
						$que1 = "SELECT cl.clientname, jd.jdname FROM client AS cl, jd AS jd WHERE cl.clientid='$clientid' AND jd.jdid='$jdid'";
					}
					$exc1 = db($db,$que1);
					$row1 = mysqli_fetch_assoc($exc1);

					$ary['resumeshortlistid'] = $row['resumeshortlistid'];
					$ary['resumeid'] = $row3['resumeid'];
					$ary['name'] = ucwords($row3['name']);
					$ary['email'] = $row3['email'];
					$ary['isd'] = $row3['isdcode'];
					$ary['phone'] = $row3['phone'];
					$ary['country'] = $row3['country'];
					$ary['nationality'] = ucfirst($row3['nationality']);
					$ary['resume'] = $row3['resume'];
					$ary['clientname'] = $row1['clientname'];
					if($jobpostid!=0)
					{
						$ary['jobpostname'] = $row1['jobpostname'];
					} else {
						$ary['jobpostname'] = 'NA';
					}
					$ary['jdname'] = $row1['jdname'];
					foreach ($skillsets as $key => $value) {
						$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
						$exc2 = db($db,$que2);
						$row2 = mysqli_fetch_assoc($exc2);

						$skillset = $skillset.' '.$row2['skillsetsname'].',';
					}
					$ary['skillsets'] = rtrim($skillset,',');
					$industryid = $row3['industryid'];
					if($industryid!='0')
					{
						$que4 = "SELECT industryname FROM industry WHERE industryid='$industryid'";
						$exc4 = db($db,$que4);
						$row4 = mysqli_fetch_assoc($exc4);
						$ary['industryname'] = $row4['industryname'];
					} else {
						$ary['industryname'] = 'NA';
					}
					$ary['createdate'] = $row3['createdate'];

					array_push($jsonarray['resumes'], $ary);
				}
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['message'] = 'No Resume Found';
			}
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Resume Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 53
	Service Name : deleteshortlistresume
	Purpose      : deleteshortlistresume
	Create By    : Aavin Seth
	Created On   : 25-01-2018
================================================================================================*/

if($_GET['servicename'] == 'deleteshortlistresume')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$resumeshortlistid = $json['resumeshortlistid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$quecl = "SELECT * FROM resumeshortlist WHERE resumeshortlistid='$resumeshortlistid' AND isdelete='0'";
	$exccl = db($db,$quecl);
	if(mysqli_num_rows($exccl) > 0)
	{
		$quecld = "UPDATE resumeshortlist SET isdelete='1' WHERE resumeshortlistid='$resumeshortlistid'";
		$exccld = db($db,$quecld);
		if($exccld)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Resume removed Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To remove resume';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To remove resume';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 54
	Service Name : refershortlistresume
	Purpose      : refer shortlisted resume to client
	Create By    : Aavin Seth
	Created On   : 27-01-2018
================================================================================================*/

if($_GET['servicename'] == 'refershortlistresume')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];
	$subject = 'CAREERSBAY Application Reference updated';
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "UPDATE resumeshortlist SET isfinal='1',sharedate='$cdate' WHERE clientid='$clientid' AND isdelete='0'";
	$exc = db($db,$que);
	if($exc)
	{
		$que1 = "SELECT email, password, clientname FROM client WHERE clientid='$clientid'";
		$exc1 = db($db,$que1);
		if(mysqli_num_rows($exc1) > 0)
		{
			$row1 = mysqli_fetch_assoc($exc1);
			$clientname = $row1['clientname'];
			$email = $row1['email'];
			$password = $row1['password'];

			$content = '<!DOCTYPE html>
				<html>
				<body>
					<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
						<tr>
							<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
								<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
									<tr>
										<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
											<a href="'.$sitelink.'"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>
										</td>
									</tr>
									<tr>
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">
														'.$subject.'<br>
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">
														<hr size="1" color="#eeeff0">
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														Hello '.$clientname.',
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														Your Candidate list has been updated
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong><a href="'.$sitelink.'client/">Click Here</a></strong> to login or copy paste '.$sitelink.'client/ in browser.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
														<b>CAREERSBAY</b>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>';

			include('../sendmail.php');
			$refer = sendMail($content,$email,$clientname,$subject,'','','');
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume refered Successfully';
		$jsonarray['refer'] = $refer;
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To refer resume';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 55
	Service Name : addindustry
	Purpose      : add industry
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

if($_GET['servicename'] == 'addindustry')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$industryname = mysqli_real_escape_string($db,$json['industryname']);
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "INSERT INTO industry SET industryname='$industryname',createdate='$cdate'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Industry Added';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Failed to add Industry';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;	
}

/*================================================================================================
	No           : 56
	Service Name : viewindustry
	Purpose      : display industry in datatable
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

if($_GET['servicename'] == 'viewindustry')
{
	$que = "SELECT * FROM industry WHERE accessed='1' ORDER BY industryname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['industry'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['industryid'] = $row['industryid'];
			$ary['industryname'] = $row['industryname'];

			array_push($jsonarray['industry'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Industry Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Industry Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 57
	Service Name : getindustry
	Purpose      : get industry details in modal
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

if($_GET['servicename'] == 'getindustry')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$industryid = $json['industryid'];

	$jsonarray = array();

	$que = "SELECT * FROM industry WHERE industryid='$industryid'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$row = mysqli_fetch_assoc($exc);

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Industry Detail Available';
		$jsonarray['industryid'] = $row['industryid'];
		$jsonarray['industryname'] = $row['industryname'];
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Industry Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 58
	Service Name : saveindustry
	Purpose      : update industry detail
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

if($_GET['servicename'] == 'saveindustry')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$industryid = $json['industryid'];
	$industryname = mysqli_real_escape_string($db,$json['industryname']);

	$jsonarray = array();

	$que = "UPDATE industry SET industryname='$industryname' WHERE industryid='$industryid'";
	$exc = db($db,$que);
	if($exc)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Industry update Successfully';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To update Industry';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 59
	Service Name : deleteindustry
	Purpose      : delete industry
	Create By    : Aavin Seth
	Created On   : 07-02-2018
================================================================================================*/

if($_GET['servicename'] == 'deleteindustry')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$industryid = $json['industryid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$que = "SELECT * FROM industry WHERE industryid='$industryid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$qued = "UPDATE industry SET accessed='0',deletedate='$cdate' WHERE industryid='$industryid'";
		$excd = db($db,$qued);
		if($excd)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Industry deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Delete Industry';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To Delete Industry';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 60
	Service Name : appendcity
	Purpose      : append city based on country
    Create By    : Aavin Seth
    Created On   : 23-02-2018
================================================================================================*/

if($_GET['servicename'] == 'appendcity')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$countryid = $json['countryid'];
	$jsonarray = array();

	$que = "SELECT * FROM states WHERE countryid='$countryid'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Data Available';
		$jsonarray['cities'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$stateid = $row['stateid'];

			$que1 = "SELECT ct.cityid AS cityid, ct.name AS cityname FROM countries AS c,states AS s,cities AS ct WHERE c.countryid=s.countryid AND s.stateid=ct.stateid AND c.countryid = '$countryid' AND s.stateid='$stateid' ORDER BY ct.name";
			$exc1 = db($db,$que1);

			while($row1 = mysqli_fetch_assoc($exc1))
			{
				$ary = array();

				$ary['cityid'] = $row1['cityid'];
				$ary['cityname'] = $row1['cityname'];

				array_push($jsonarray['cities'], $ary);
			}
		}
		$ordered = array();
		foreach ($jsonarray['cities'] as $key => $row)
		{
		    $ordered[$key] = $row['cityname'];
		}
		array_multisort($ordered, SORT_ASC, $jsonarray['cities']);
		// $jsonarray = array_unique($jsonarray['cities']);
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Cities Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 61
	Service Name : getcity
	Purpose      : get city based on country
    Create By    : Aavin Seth
    Created On   : 23-02-2018
================================================================================================*/

if($_GET['servicename'] == 'getcity')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$country = $json['country'];
	$jsonarray = array();

	$que2 = "SELECT * FROM countries WHERE countryname='$country'";
	$exc2 = db($db,$que2);
	if(mysqli_num_rows($exc2) > 0)
	{
		$row2 = mysqli_fetch_assoc($exc2);
		$countryid = $row2['countryid'];

		$que = "SELECT * FROM states WHERE countryid='$countryid'";
		$exc = db($db,$que);
		if(mysqli_num_rows($exc) > 0)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Data Available';
			$jsonarray['cities'] = array();

			while($row = mysqli_fetch_assoc($exc))
			{
				$stateid = $row['stateid'];

				$que1 = "SELECT ct.cityid AS cityid, ct.name AS cityname FROM countries AS c,states AS s,cities AS ct WHERE c.countryid=s.countryid AND s.stateid=ct.stateid AND c.countryid = '$countryid' AND s.stateid='$stateid' ORDER BY ct.name";
				$exc1 = db($db,$que1);

				while($row1 = mysqli_fetch_assoc($exc1))
				{
					$ary = array();

					$ary['cityid'] = $row1['cityid'];
					$ary['cityname'] = $row1['cityname'];

					array_push($jsonarray['cities'], $ary);
				}
			}
			$ordered = array();
			foreach ($jsonarray['cities'] as $key => $row)
			{
			    $ordered[$key] = $row['cityname'];
			}
			array_multisort($ordered, SORT_ASC, $jsonarray['cities']);
			// $jsonarray = array_unique($jsonarray['cities']);
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'No Cities Found';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Country Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 62
	Service Name : addadmin
	Purpose      : add admin
	Create By    : Aavin Seth
	Created On   : 24-02-2018
================================================================================================*/

if($_GET['servicename'] == 'addadmin')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$adminid = $json['adminid'];
	$firstname = mysqli_real_escape_string($db,$json['firstname']);
	$lastname = mysqli_real_escape_string($db,$json['lastname']);
	$profilepic = $json['profilepic'];
	$isd = $json['isd'];
	$phone = $json['phone'];
	$email = $json['email'];
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = substr(str_shuffle($chars),0,8);
	$cdate = date('Y-m-d H:i:s');
	$subject = 'Welcome to Careers Bay';

	if($profilepic != "")
	{
		$type1 = substr($profilepic, 11, strpos($profilepic, ';')-11);
		$profilepicname = uniqid().".".$type1;
	} else {
		$profilepicname = '';
	}

	$jsonarray = array();

	$queuue = "SELECT email FROM admin WHERE email='$email' AND accessed='1'";
	$excuue = db($db,$queuue);
	if(mysqli_num_rows($excuue) == 0)
	{
		$emailflag = '1';
		$jsonarray['email'] = 'success';
	}
	else
	{
		$emailflag = '';
		$jsonarray['email'] = 'failure';
		$jsonarray['emailerror'] = 'Email Id Already Registered';
	}

	$queuum = "SELECT phone FROM admin WHERE isdcode='$isd' AND phone='$phone' AND accessed='1'";
	$excuum = db($db,$queuum);
	if(mysqli_num_rows($excuum) == 0)
	{
		$phoneflag = '1';
		$jsonarray['phone'] = 'success';
	}
	else
	{
		$phoneflag = '';
		$jsonarray['phone'] = 'failure';
		$jsonarray['phoneerror'] = 'Mobile Number Already Registered';
	}

	if($emailflag=='1' && $phoneflag=='1')
	{
		$quecli = "INSERT INTO admin SET firstname='$firstname',lastname='$lastname',profilepic='$profilepicname',isdcode='$isd',phone='$phone',email='$email',password='$password',addedby='$adminid',activation='0',createdate='$cdate'";
		$exccli = db($db,$quecli);

		if($exccli)
		{
			if($profilepic != '')
			{
				$data = $profilepic;
				list($type, $data) = explode(';', $data);
				list(, $data)      = explode(',', $data);
				$data = base64_decode($data);
				file_put_contents('../assets/images/profilepic/'.$profilepicname, $data);
			}

			$content = '<!DOCTYPE html>
				<html>
				<body>
					<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
						<tr>
							<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
								<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
									<tr>
										<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
											<a href="'.$sitelink.'"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>
										</td>
									</tr>
									<tr>
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">
														'.$subject.'<br>
														<span style="font-size: 12px;">Please find your login credentials for Careers Bay</span>
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">
														<hr size="1" color="#eeeff0">
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Email:</strong> '.$email.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Password:</strong> '.$password.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong><a href="'.$sitelink.'admin/">Click Here</a></strong> to login or copy paste '.$sitelink.'admin/ in browser.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
														<b>CAREERSBAY</b>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>';

			include('../sendmail.php');
			sendMail($content,$email,$firstname.' '.$lastname,$subject,'','','');

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Admin Added Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To Add Admin';
		}
	}
	else
	{
		$jsonarray['email'] = 'failure';
		$jsonarray['message'] = 'Check Error';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 63
	Service Name : viewadmin
	Purpose      : display admin in datatable
	Create By    : Aavin Seth
	Created On   : 24-02-2018
================================================================================================*/

if($_GET['servicename'] == 'viewadmin')
{
	$que = "SELECT * FROM admin WHERE accessed='1' ORDER BY firstname ASC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['admin'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['adminid'] = $row['adminid'];
			$ary['firstname'] = $row['firstname'];
			$ary['lastname'] = $row['lastname'];
			$ary['profilepic'] = $row['profilepic'];
			$ary['phone'] = '+'.$row['isdcode'].'-'.$row['phone'];
			$ary['email'] = $row['email'];
			$ary['admintype'] = $row['admintype'];

			array_push($jsonarray['admin'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Admin Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Admin Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 64
	Service Name : deleteadmin
	Purpose      : delete admin
	Create By    : Aavin Seth
	Created On   : 28-02-2018
================================================================================================*/

if($_GET['servicename'] == 'deleteadmin')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$adminid = $json['adminid'];
	$currentadminid = $json['currentadminid'];
	$cdate = date('Y-m-d H:i:s');

	$jsonarray = array();

	$quecl = "SELECT * FROM admin WHERE adminid='$adminid' AND accessed='1'";
	$exccl = db($db,$quecl);
	if(mysqli_num_rows($exccl) > 0)
	{
		$quecld = "UPDATE admin SET accessed='0',deleteby='$currentadminid' WHERE adminid='$adminid'";
		$exccld = db($db,$quecld);
		if($exccld)
		{
			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Admin deleted Successfully';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Not Able To delete Admin';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Not Able To delete Admin';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}
?>