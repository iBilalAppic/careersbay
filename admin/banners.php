<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- SUMMERNOTE -->
            <link href="../css/plugins/summernote/summernote.css" rel="stylesheet">
            <link href="../css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Banners</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Banners</a>
                        </li>
                        <li class="active">
                            <strong>View Banners</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewbanners">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Banner Image</th>
                                                <th>Banner Message</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Banner Image</th>
                                                <th>Banner Message</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Banner Modal -->
            <div class="modal inmodal" id="editbannersmodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="savebanner">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                <input type="hidden" id="oldbanner" name="oldbanner">
                                                <input type="file" id="banner" name="banner" onchange="saveBannerImage(this);">
                                            </span>
                                            <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                        </div>
                                        <span class="help-block m-b-none">The required dimensions is 2000 w x 1333 h</span>
                                    </div>
                                </div>
                                <div class="form-group" id="bannerpreviewbox">
                                    <label class="col-sm-2 control-label">&nbsp;</label>
                                    <div class="col-sm-10">
                                        <img width="150" src="" id="bannerpreview">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Banner Text</label>
                                    <div class="col-sm-10">
                                        <textarea name="bannertext" id="bannertext" class="summernote"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="bannerid" name="bannerid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <!-- Bootstrap Tooltip -->
            <script src="../js/plugins/bootstrap-tooltip/bootstrap-tooltip.js"></script>
            <!-- SUMMERNOTE -->
            <script src="../js/plugins/summernote/summernote.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Banners';
                $('#banners-nav').addClass('active');
                $('#view-banners-nav').addClass('active');
                viewBanners();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>