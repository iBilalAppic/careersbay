<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- Data picker -->
            <link href="../css/plugins/datapicker/datepicker3.css" rel="stylesheet">
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            .date {padding-right: 14px !important; padding-left: 14px !important;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Clients</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Clients</a>
                        </li>
                        <li class="active">
                            <strong>View Clients</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewclients">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Client Name</th>
                                                <th>Client Logo</th>
                                                <th>Contact Number</th>
                                                <th>Email</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Client Name</th>
                                                <th>Client Logo</th>
                                                <th>Contact Number</th>
                                                <th>Email</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Client Modal -->
            <div class="modal inmodal" id="editclientmodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="saveclient">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Client Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Client Name" id="clientname" name="clientname" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Logo</label>
                                    <div class="col-sm-10">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                <input type="hidden" id="oldclientlogo" name="oldclientlogo">
                                                <input type="file" id="clientlogo" name="clientlogo">
                                                <input type="hidden" id="clientlogoext" name="clientlogoext">
                                            </span>
                                            <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                        </div>
                                        <span class="help-block m-b-none">The required dimensions is 225 w x 90 h</span>
                                    </div>
                                </div>
                                <div class="form-group" id="clientpreviewbox">
                                    <label class="col-sm-2 control-label">&nbsp;</label>
                                    <div class="col-sm-10">
                                        <img width="150" src="" id="clientpreview">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address 1<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Address 1" id="address1" name="address1" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Address 2" id="address2" name="address2" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Country<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select a Country" id="country" name="country" class="chosen-select" onchange="appendCity(this);"></select>
                                        <span id="country-error" class="help-block m-b-none hide" style="font-weight: 700;color:#cc5965;">Please Select Country</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">City<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <!-- <input type="text" placeholder="Enter City" id="city" name="city" class="form-control" autocomplete="off"> -->
                                        <select data-placeholder="Select a City" id="city" name="city" class="chosen-select"><option value="">Select a City</option></select>
                                        <span id="city-error" class="help-block m-b-none hide" style="font-weight: 700;color:#cc5965;">Please Select City</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Contact Number<span class="text-danger">*</span></label>
                                    <div class="col-sm-4">
                                        <select data-placeholder="ISD" id="isd" name="isd" class="chosen-select"></select>
                                        <input type="hidden" id="oldisd" name="oldisd" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Enter Contact Number" id="phone" name="phone" class="form-control" autocomplete="off">
                                        <input type="hidden" id="oldphone" name="oldphone" class="form-control">
                                        <span class="help-block m-b-none phone-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email Id<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Email Id" id="email" name="email" class="form-control" autocomplete="off">
                                        <input type="hidden" id="oldemail" name="oldemail" class="form-control">
                                        <span class="help-block m-b-none email-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Website</label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Website" id="website" name="website" class="form-control" autocomplete="off">
                                        <span class="help-block m-b-none">E.g: http://www.careersbay.com</span>
                                    </div>
                                </div>
                                <div class="form-group" id="data_1">
                                    <label class="col-sm-2 control-label">Established Date</label>
                                    <div class="col-sm-10 input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="establisheddate" name="establisheddate" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="clientid" name="clientid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Data picker -->
            <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Clients';
                $('#clients-nav').addClass('active');
                $('#view-clients-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                $('#data_1 .input-group.date').datepicker({
                    todayBtn: false,
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: false,
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    multidate: false
                });
                dqs();
                viewClients();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>