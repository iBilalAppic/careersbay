<?php
session_start();
if((isset($_SESSION['adminprofile']) && $_SESSION['adminprofile']!=''))
{
    header('location:home.php');
}
?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Forgot Password | CAREERSBAY</title>

        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="../img/logo.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="../img/logo.png">
        <link rel="icon" href="../img/logo.png" sizes="32x32">
        <meta name="theme-color" content="#f3f3f4">

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/common.css" rel="stylesheet">
        <style>
            img {
                width: 35%;
                height: 35%;
            }
        </style>
    </head>

    <body class="gray-bg">
        <div class="passwordBox animated fadeInDown">
            <div class="m-b-md text-center">
                <img alt="image" class="" src="../img/careersbay-logo.jpg">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox-content">
                        <h2 class="font-bold">Forgot Password</h2>
                        <p>Enter your email address and your password will be reset and emailed to you.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="m-t" role="form" name="forgetpassword">
                                    <div class="form-group">
                                        <input type="email" placeholder="Email address" id="email" name="email" class="form-control" required="yes" autocomplete="off">
                                    </div>
                                    <button type="submit" class="btn btn-primary block full-width m-b">Reset Password</button>
                                    <a class="btn btn-sm btn-white btn-block" href="./">Login</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Jquery & bootstrap JS -->
        <script src="../js/jquery-3.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <!-- Mainly scripts -->
        <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Jquery Validation -->
        <script src="../js/plugins/validate/jquery.validate.min.js"></script>

        <!-- Custom Js -->
        <script src="admin-service.js"></script>
    </body>
</html>