<?php include('admin-header.php'); ?>

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Dashboard</h2>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Last 7 Days</span>
                                <h5>Resumes</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="resumesdash">&nbsp;</h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-file-text"></i></div>
                                <small>New Entries</small>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Transporters</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="transporterdash">&nbsp;</h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-building"></i></div>
                                <small>Total Transporters</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Annual</span>
                                <h5>Drivers</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="driverdash">&nbsp;</h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-group"></i></div>
                                <small>Total Drivers</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Today</span>
                                <h5>Vehicles</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="vehicledash">&nbsp;</h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-car"></i></div>
                                <small>Vehicles Registered</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Packages</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins" id="packagedash">&nbsp;</h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-cubes"></i></div>
                                <small>Packages Delivered</small>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- content end here -->

            <!-- Scripts Starts -->
            <script>
            $(document).ready(function() {
                document.title = 'Dashboard';
                $('#dashboard-nav').addClass('active');
                dashboard();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>