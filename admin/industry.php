<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            @media (min-width: 992px) {.modal-lg {width: 900px;}}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Industry</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Industry</a>
                        </li>
                        <li class="active">
                            <strong>View Industry</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewindustry">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Industry Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Industry Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Industry Modal -->
            <div class="modal inmodal" id="editindustrymodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="saveindustry">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Industry Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Industry Name" id="industryname" name="industryname" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="industryid" name="industryid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Bootstrap Modal -->
            <script src="../js/plugins/bootstrap-modal/bootstrap-modal.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Industry';
                $('#industry-nav').addClass('active');
                $('#view-industry-nav').addClass('active');
                viewIndustry();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>