<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- SUMMERNOTE -->
            <link href="../css/plugins/summernote/summernote.css" rel="stylesheet">
            <link href="../css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            @media (min-width: 992px) {.modal-lg {width: 900px;}}
            .truncate {width: 250px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Job Title</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Job Title</a>
                        </li>
                        <li class="active">
                            <strong>View Job Title</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewjd">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Job Title Name</th>
                                                <!-- <th>Description</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Job Title Name</th>
                                                <!-- <th>Description</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit JD Modal -->
            <div class="modal inmodal" id="editjdmodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="savejd">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Job Title Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Job Title Name" id="jdname" name="jdname" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Description<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <textarea name="description" id="description" class="summernote"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="jdid" name="jdid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Bootstrap Tooltip -->
            <script src="../js/plugins/bootstrap-tooltip/bootstrap-tooltip.js"></script>
            <!-- SUMMERNOTE -->
            <script src="../js/plugins/summernote/summernote.min.js"></script>
            <!-- Bootstrap Modal -->
            <script src="../js/plugins/bootstrap-modal/bootstrap-modal.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View JD';
                $('#jd-nav').addClass('active');
                $('#view-jd-nav').addClass('active');
                $('.summernote').summernote('code');
                viewJd();
                // var editor = $.summernote.eventHandler.getEditor(); 
                // var editable = $('#description');
                // editor.insertNode(editable, description);
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>