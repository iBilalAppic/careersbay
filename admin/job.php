<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- Data picker -->
            <link href="../css/plugins/datapicker/datepicker3.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            .date {padding-right: 14px !important; padding-left: 14px !important;}
            a.facebookshare, a.facebookshare:hover { background-color: #2b4a8b;color: #ffffff;}
            a.twittershare, a.twittershare:hover { background-color: #1f94d9;color: #ffffff;}
            a.googleshare, a.googleshare:hover { background-color: #d34836;color: #ffffff;}
            a.linkedinshare, a.linkedinshare:hover { background-color: #0077B5;color: #ffffff;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Jobs</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Jobs</a>
                        </li>
                        <li class="active">
                            <strong>View Jobs</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewjob">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Job Name</th>
                                                <th>Client Name</th>
                                                <th>Job Title</th>
                                                <th>Expire Date</th>
                                                <th>Status</th>
                                                <th>Share</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Job Name</th>
                                                <th>Client Name</th>
                                                <th>Job Title</th>
                                                <th>Expire Date</th>
                                                <th>Status</th>
                                                <th>Share</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Job Modal -->
            <div class="modal inmodal" id="editjobmodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="savejob">
                            <div class="modal-body">
                                <div class="form-group">
                                        <label class="col-sm-2 control-label">Job Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Job Name" id="jobpostname" name="jobpostname" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Client<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select Client" id="clientid" name="clientid" class="form-control m-b chosen-select">
                                            <option selected="selected" disabled>Select Client</option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                      Job Title<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select Job Title" id="jdid" name="jdid" class="form-control m-b chosen-select">
                                            <option selected="selected" disabled>Select Job Title</option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="data_1">
                                    <label class="col-sm-2 control-label">Expire Date<span class="text-danger">*</span></label>
                                    <div class="col-sm-10 input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" placeholder="Expire Date" id="expiredate" name="expiredate" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="jobpostid" name="jobpostid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Data picker -->
            <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Job';
                $('#job-nav').addClass('active');
                $('#view-job-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                $('#data_1 .input-group.date').datepicker({
                    todayBtn: false,
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: false,
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    multidate: false,
                    startDate: new Date()
                });
                clientJd();
                viewJob();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>