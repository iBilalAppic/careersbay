<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Profile</h2>
                    <!-- <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Customer</a>
                        </li>
                        <li class="active">
                            <strong>View Customer</strong>
                        </li>
                    </ol> -->
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <ul class="nav nav-tabs">
                                    <li id="nav-tab-1" class="active"><a data-toggle="tab" href="#tab-1">Edit Profile</a></li>
                                    <li id="nav-tab-2" class=""><a data-toggle="tab" href="#tab-2">Change Password</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab-1" class="tab-pane active">
                                        <div class="panel-body">
                                            <form class="form-horizontal" name="saveprofile">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">First Name<span class="text-danger">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" placeholder="Enter First Name" id="firstname" name="firstname" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Last Name<span class="text-danger">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" placeholder="Enter Last Name" id="lastname" name="lastname" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Profile Image</label>
                                                        <div class="col-sm-10">
                                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                <div class="form-control" data-trigger="fileinput">
                                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                                                </div>
                                                                <span class="input-group-addon btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                                    <input type="hidden" id="oldprofilepic" name="oldprofilepic">
                                                                    <input type="file" id="profilepic" name="profilepic">
                                                                </span>
                                                                <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                            </div>
                                                            <span class="help-block m-b-none">The required dimensions is 100px * 100px</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="previewbox">
                                                        <label class="col-sm-2 control-label">&nbsp;</label>
                                                        <div class="col-sm-10">
                                                            <img width="150" src="" id="preview">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Mobile<span class="text-danger">*</span></label>
                                                        <div class="col-sm-2">
                                                            <select data-placeholder="ISD" id="isd" name="isd" class="chosen-select"></select>
                                                            <input type="hidden" id="oldisd" name="oldisd" class="form-control">
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" placeholder="Enter Mobile Number" id="phone" name="phone" class="form-control" autocomplete="off">
                                                            <input type="hidden" id="oldphone" name="oldphone" class="form-control">
                                                            <span class="help-block m-b-none phone-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Email Id<span class="text-danger">*</span></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" placeholder="Enter Email Id" id="email" name="email" class="form-control" autocomplete="off">
                                                            <input type="hidden" placeholder="Enter Email Id" id="oldemail" name="oldemail" class="form-control">
                                                            <span class="help-block m-b-none email-error" style="display:none;font-weight: 700;color:#cc5965;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="hr-line-dashed"></div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-2">
                                                            <input type="hidden" id="adminid" name="adminid" class="form-control">
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div id="tab-2" class="tab-pane">
                                        <div class="panel-body">
                                            <form class="form-horizontal" name="changepassword">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Old Password<span class="text-danger">*</span></label>
                                                    <div class="col-sm-10">
                                                        <input type="password" placeholder="Enter Old Password" id="oldpassword" name="oldpassword" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">New Password<span class="text-danger">*</span></label>
                                                    <div class="col-sm-10">
                                                        <input type="password" placeholder="Enter New Password" id="newpassword" name="newpassword" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Confirm Password<span class="text-danger">*</span></label>
                                                    <div class="col-sm-10">
                                                        <input type="password" placeholder="Rewrite New Password" id="confirmpassword" name="confirmpassword" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4 col-sm-offset-2">
                                                        <input type="hidden" id="redirectto" name="redirectto" class="form-control" value="profile">
                                                        <input type="hidden" id="adminid" name="adminid" class="form-control" value="<?php echo $adminid; ?>">
                                                        <button type="submit" class="btn btn-primary">Change Password</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content end here -->

            <!-- Scripts Starts -->
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'Edit Profile';
                $('.chosen-select').chosen({width: "100%"});
                dqs();
                getProfile(<?php echo $adminid; ?>);
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>