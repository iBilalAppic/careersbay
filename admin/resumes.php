<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- Jasny input -->
            <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
            <!-- Awesome Bootstrap Checkbox -->
            <link href="../css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
            <!-- Custom Style -->
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Resumes</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li class="active">
                            <strong>Resumes</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content m-b-sm border-bottom">
                                <form name="resumesfilter">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="isd">ISD</label>
                                                <select data-placeholder="Select ISD" name="filterisd" id="filterisd" class="form-control chosen-select"><option value="">Select ISD</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="country">Country</label>
                                                <select data-placeholder="Choose a Country" name="filtercountry" id="filtercountry" class="form-control chosen-select"><option value="">Select Country</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="emaillist">Email</label>
                                                <select data-placeholder="Select a Email" id="filteremaillist" name="filteremaillist" class="chosen-select"><option value="">Select Email</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="clientslist">Clients</label>
                                                <select data-placeholder="Choose a Client" name="filterclientslist" id="filterclientslist" class="form-control chosen-select"><option value="">Select Client</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="jdlist">Job Title</label>
                                                <select data-placeholder="Choose a Job Title" name="filterjdlist" id="filterjdlist" class="form-control chosen-select"><option value="">Select Job Title</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="skillsets">Skill Sets</label>
                                                <select data-placeholder="Choose a Skill Sets" id="filterskillsets" name="filterskillsets" class="chosen-select" multiple></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="industrylist">Industry</label>
                                                <select data-placeholder="Choose a Industry" id="filterindustrylist" name="filterindustrylist" class="chosen-select"><option value="">Select Industry</option></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group pull-right">
                                                <button type="button" class="btn btn-sm btn-primary" name="resumesfilter" id="resumesfilter" onclick="resetResumesFilter();">Reset</button> &nbsp; <button type="button" class="btn btn-sm btn-primary" name="resumesfilter" id="resumesfilter" onclick="resumesFilter(this);">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <form class="form-horizontal" name="shortlist">
                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <select data-placeholder="Select Client" id="clientslist" name="clientslist" class="chosen-select"><option value="">Select Client</option></select>
                                                    <span id="shortlist-error" class="help-block m-b-none hide" style="font-weight: 700;color:#cc5965;"></span>
                                                </div>
                                                <div class="col-sm-4" style="padding-left:0px;">
                                                    <button type="button" class="btn btn-sm btn-primary" data-resumecount="0" id="resumecount" onclick="shortList(this);">Shortlist</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewresumes">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Prefered Client</th>
                                                <th>Shortlisted for</th>
                                                <th>Job Post</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Resume</th>
                                                <th>Applied Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Prefered Client</th>
                                                <th>Shortlisted for</th>
                                                <th>Job Post</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Resume</th>
                                                <th>Applied Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Resume Modal -->
            <div class="modal inmodal" id="editresumemodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="saveresume">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Applicant Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Candidate Name" id="candidatename" name="candidatename" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="email" placeholder="Email Address" id="email" name="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Phone<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Phone Number" id="phone" name="phone" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Country<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select a Country" id="country" name="country" class="chosen-select"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nationality<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select a Nationality" id="nationality" name="nationality" class="chosen-select">
                                            <option value="afghan">Afghan</option>
                                            <option value="albanian">Albanian</option>
                                            <option value="algerian">Algerian</option>
                                            <option value="american">American</option>
                                            <option value="andorran">Andorran</option>
                                            <option value="angolan">Angolan</option>
                                            <option value="antiguans">Antiguans</option>
                                            <option value="argentinean">Argentinean</option>
                                            <option value="armenian">Armenian</option>
                                            <option value="australian">Australian</option>
                                            <option value="austrian">Austrian</option>
                                            <option value="azerbaijani">Azerbaijani</option>
                                            <option value="bahamian">Bahamian</option>
                                            <option value="bahraini">Bahraini</option>
                                            <option value="bangladeshi">Bangladeshi</option>
                                            <option value="barbadian">Barbadian</option>
                                            <option value="barbudans">Barbudans</option>
                                            <option value="batswana">Batswana</option>
                                            <option value="belarusian">Belarusian</option>
                                            <option value="belgian">Belgian</option>
                                            <option value="belizean">Belizean</option>
                                            <option value="beninese">Beninese</option>
                                            <option value="bhutanese">Bhutanese</option>
                                            <option value="bolivian">Bolivian</option>
                                            <option value="bosnian">Bosnian</option>
                                            <option value="brazilian">Brazilian</option>
                                            <option value="british">British</option>
                                            <option value="bruneian">Bruneian</option>
                                            <option value="bulgarian">Bulgarian</option>
                                            <option value="burkinabe">Burkinabe</option>
                                            <option value="burmese">Burmese</option>
                                            <option value="burundian">Burundian</option>
                                            <option value="cambodian">Cambodian</option>
                                            <option value="cameroonian">Cameroonian</option>
                                            <option value="canadian">Canadian</option>
                                            <option value="cape verdean">Cape Verdean</option>
                                            <option value="central african">Central African</option>
                                            <option value="chadian">Chadian</option>
                                            <option value="chilean">Chilean</option>
                                            <option value="chinese">Chinese</option>
                                            <option value="colombian">Colombian</option>
                                            <option value="comoran">Comoran</option>
                                            <option value="congolese">Congolese</option>
                                            <option value="costa rican">Costa Rican</option>
                                            <option value="croatian">Croatian</option>
                                            <option value="cuban">Cuban</option>
                                            <option value="cypriot">Cypriot</option>
                                            <option value="czech">Czech</option>
                                            <option value="danish">Danish</option>
                                            <option value="djibouti">Djibouti</option>
                                            <option value="dominican">Dominican</option>
                                            <option value="dutch">Dutch</option>
                                            <option value="east timorese">East Timorese</option>
                                            <option value="ecuadorean">Ecuadorean</option>
                                            <option value="egyptian">Egyptian</option>
                                            <option value="emirian">Emirian</option>
                                            <option value="equatorial guinean">Equatorial Guinean</option>
                                            <option value="eritrean">Eritrean</option>
                                            <option value="estonian">Estonian</option>
                                            <option value="ethiopian">Ethiopian</option>
                                            <option value="fijian">Fijian</option>
                                            <option value="filipino">Filipino</option>
                                            <option value="finnish">Finnish</option>
                                            <option value="french">French</option>
                                            <option value="gabonese">Gabonese</option>
                                            <option value="gambian">Gambian</option>
                                            <option value="georgian">Georgian</option>
                                            <option value="german">German</option>
                                            <option value="ghanaian">Ghanaian</option>
                                            <option value="greek">Greek</option>
                                            <option value="grenadian">Grenadian</option>
                                            <option value="guatemalan">Guatemalan</option>
                                            <option value="guinea-bissauan">Guinea-Bissauan</option>
                                            <option value="guinean">Guinean</option>
                                            <option value="guyanese">Guyanese</option>
                                            <option value="haitian">Haitian</option>
                                            <option value="herzegovinian">Herzegovinian</option>
                                            <option value="honduran">Honduran</option>
                                            <option value="hungarian">Hungarian</option>
                                            <option value="icelander">Icelander</option>
                                            <option value="indian">Indian</option>
                                            <option value="indonesian">Indonesian</option>
                                            <option value="iranian">Iranian</option>
                                            <option value="iraqi">Iraqi</option>
                                            <option value="irish">Irish</option>
                                            <option value="israeli">Israeli</option>
                                            <option value="italian">Italian</option>
                                            <option value="ivorian">Ivorian</option>
                                            <option value="jamaican">Jamaican</option>
                                            <option value="japanese">Japanese</option>
                                            <option value="jordanian">Jordanian</option>
                                            <option value="kazakhstani">Kazakhstani</option>
                                            <option value="kenyan">Kenyan</option>
                                            <option value="kittian and nevisian">Kittian and Nevisian</option>
                                            <option value="kuwaiti">Kuwaiti</option>
                                            <option value="kyrgyz">Kyrgyz</option>
                                            <option value="laotian">Laotian</option>
                                            <option value="latvian">Latvian</option>
                                            <option value="lebanese">Lebanese</option>
                                            <option value="liberian">Liberian</option>
                                            <option value="libyan">Libyan</option>
                                            <option value="liechtensteiner">Liechtensteiner</option>
                                            <option value="lithuanian">Lithuanian</option>
                                            <option value="luxembourger">Luxembourger</option>
                                            <option value="macedonian">Macedonian</option>
                                            <option value="malagasy">Malagasy</option>
                                            <option value="malawian">Malawian</option>
                                            <option value="malaysian">Malaysian</option>
                                            <option value="maldivan">Maldivan</option>
                                            <option value="malian">Malian</option>
                                            <option value="maltese">Maltese</option>
                                            <option value="marshallese">Marshallese</option>
                                            <option value="mauritanian">Mauritanian</option>
                                            <option value="mauritian">Mauritian</option>
                                            <option value="mexican">Mexican</option>
                                            <option value="micronesian">Micronesian</option>
                                            <option value="moldovan">Moldovan</option>
                                            <option value="monacan">Monacan</option>
                                            <option value="mongolian">Mongolian</option>
                                            <option value="moroccan">Moroccan</option>
                                            <option value="mosotho">Mosotho</option>
                                            <option value="motswana">Motswana</option>
                                            <option value="mozambican">Mozambican</option>
                                            <option value="namibian">Namibian</option>
                                            <option value="nauruan">Nauruan</option>
                                            <option value="nepalese">Nepalese</option>
                                            <option value="new zealander">New Zealander</option>
                                            <option value="ni-vanuatu">Ni-Vanuatu</option>
                                            <option value="nicaraguan">Nicaraguan</option>
                                            <option value="nigerien">Nigerien</option>
                                            <option value="north korean">North Korean</option>
                                            <option value="northern irish">Northern Irish</option>
                                            <option value="norwegian">Norwegian</option>
                                            <option value="omani">Omani</option>
                                            <option value="pakistani">Pakistani</option>
                                            <option value="palauan">Palauan</option>
                                            <option value="panamanian">Panamanian</option>
                                            <option value="papua new guinean">Papua New Guinean</option>
                                            <option value="paraguayan">Paraguayan</option>
                                            <option value="peruvian">Peruvian</option>
                                            <option value="polish">Polish</option>
                                            <option value="portuguese">Portuguese</option>
                                            <option value="qatari">Qatari</option>
                                            <option value="romanian">Romanian</option>
                                            <option value="russian">Russian</option>
                                            <option value="rwandan">Rwandan</option>
                                            <option value="saint lucian">Saint Lucian</option>
                                            <option value="salvadoran">Salvadoran</option>
                                            <option value="samoan">Samoan</option>
                                            <option value="san marinese">San Marinese</option>
                                            <option value="sao tomean">Sao Tomean</option>
                                            <option value="saudi">Saudi</option>
                                            <option value="scottish">Scottish</option>
                                            <option value="senegalese">Senegalese</option>
                                            <option value="serbian">Serbian</option>
                                            <option value="seychellois">Seychellois</option>
                                            <option value="sierra leonean">Sierra Leonean</option>
                                            <option value="singaporean">Singaporean</option>
                                            <option value="slovakian">Slovakian</option>
                                            <option value="slovenian">Slovenian</option>
                                            <option value="solomon islander">Solomon Islander</option>
                                            <option value="somali">Somali</option>
                                            <option value="south african">South African</option>
                                            <option value="south korean">South Korean</option>
                                            <option value="spanish">Spanish</option>
                                            <option value="sri lankan">Sri Lankan</option>
                                            <option value="sudanese">Sudanese</option>
                                            <option value="surinamer">Surinamer</option>
                                            <option value="swazi">Swazi</option>
                                            <option value="swedish">Swedish</option>
                                            <option value="swiss">Swiss</option>
                                            <option value="syrian">Syrian</option>
                                            <option value="taiwanese">Taiwanese</option>
                                            <option value="tajik">Tajik</option>
                                            <option value="tanzanian">Tanzanian</option>
                                            <option value="thai">Thai</option>
                                            <option value="togolese">Togolese</option>
                                            <option value="tongan">Tongan</option>
                                            <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                                            <option value="tunisian">Tunisian</option>
                                            <option value="turkish">Turkish</option>
                                            <option value="tuvaluan">Tuvaluan</option>
                                            <option value="ugandan">Ugandan</option>
                                            <option value="ukrainian">Ukrainian</option>
                                            <option value="uruguayan">Uruguayan</option>
                                            <option value="uzbekistani">Uzbekistani</option>
                                            <option value="venezuelan">Venezuelan</option>
                                            <option value="vietnamese">Vietnamese</option>
                                            <option value="welsh">Welsh</option>
                                            <option value="yemenite">Yemenite</option>
                                            <option value="zambian">Zambian</option>
                                            <option value="zimbabwean">Zimbabwean</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-sm-2 control-label">Designation<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select a Designation" id="designation" name="designation" class="chosen-select"></select>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Industry</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Industry" id="industrylist" name="industrylist" class="chosen-select"><option value="">Choose Industry</option></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Qualification<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Select a Qualification" id="qualification" name="qualification" class="chosen-select"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Skill Sets<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Skill Sets" id="skillsets" name="skillsets" class="chosen-select" multiple></select>
                                        <span id="skillsets-error" class="help-block m-b-none hide" style="font-weight: 700;color:#cc5965;">Choose a Skill Sets</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Resume</label>
                                    <div class="col-sm-10">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i><span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                                <input type="hidden" id="oldresume" name="oldresume">
                                                <input type="file" id="resume" name="resume">
                                                <input type="hidden" id="resumebasecode" name="resumebasecode">
                                                <input type="hidden" id="resumeext" name="resumeext">
                                                <input type="hidden" id="mime" name="mime">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="resumeid" name="resumeid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Jasny -->
            <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Resumes';
                $('#resumes-nav').addClass('active');
                $('#view-resumes-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                dqs();
                viewClients();
                viewJd();
                viewIndustry();
                viewResumes();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>