<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Chosen -->
            <link href="../css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
            <!-- Awesome Bootstrap Checkbox -->
            <link href="../css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Custom Style -->
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Resumes</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li class="active">
                            <strong>Shortlist Resumes</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <form class="form-horizontal" name="clients">
                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <select data-placeholder="Select Client" id="clientslist" name="clientslist" class="chosen-select" onchange="viewShortListResumes(this);"><option value="">Select Client</option></select>
                                                    <span id="client-error" class="help-block m-b-none hide" style="font-weight: 700;color:#cc5965;">Please select Client</span>
                                                </div>
                                                <div class="col-sm-4" style="padding-left:0px;">
                                                    <button type="button" class="btn btn-sm btn-primary hide" id="referbtn" onclick="referShortlistResume();">Click to Refer</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive hide" id="shortlisttable">
                                    <table class="table table-striped table-bordered table-hover" id="viewshortlist">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Prefered Client</th>
                                                <th>Job Post</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Resume</th>
                                                <th>Applied Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Prefered Client</th>
                                                <th>Job Post</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Resume</th>
                                                <th>Applied Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Chosen -->
            <script src="../js/plugins/chosen/chosen.jquery.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'Shortlist Resumes';
                $('#resumes-nav').addClass('active');
                $('#resumes-shortlist-nav').addClass('active');
                $('.chosen-select').chosen({width: "100%"});
                viewClients();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>