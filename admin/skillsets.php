<?php include('admin-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <!-- Sweet Alert -->
            <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
            <!-- Custom Style -->
            <style>
            @media (min-width: 992px) {.modal-lg {width: 900px;}}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Skill Sets</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="home.php">Home</a>
                        </li>
                        <li>
                            <a>Skill Sets</a>
                        </li>
                        <li class="active">
                            <strong>View Skill Sets</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewskillsets">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Skill Sets Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Skill Sets Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Edit Skill Sets Modal -->
            <div class="modal inmodal" id="editskillsetsmodal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form class="form-horizontal" name="saveskillsets">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Skill Sets Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="Enter Skill Sets Name" id="skillsetsname" name="skillsetsname" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="skillsetsid" name="skillsetsid" class="form-control">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <!-- Sweet alert -->
            <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
            <!-- Bootstrap Modal -->
            <script src="../js/plugins/bootstrap-modal/bootstrap-modal.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'View Skill Sets';
                $('#skillsets-nav').addClass('active');
                $('#view-skillsets-nav').addClass('active');
                viewSkillsets();
            });
            </script>
            <!-- Scripts End -->

<?php include('admin-footer.php'); ?>