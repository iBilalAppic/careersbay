
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer">
                        <div class="pull-right">
                            <!-- Powered by <strong><a href="" target="_blank"><img src="../img/careersbay-logo.jpg" alt="careersbay" width="100"></a></strong> -->
                        </div>
                        <div>
                            <a href="http://careersbay.com" target="_blank"><img src="../img/careersbay-logo.jpg" alt="VPS" height="25">&nbsp;<strong class="careersbay-text"></strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>
    <!-- Toastr -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>
    <!-- Jquery Validation -->
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Custom Js -->
    <script src="client-service.js"></script>
</body>
</html>