<?php include('client-session.php'); ?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>CAREERSBAY</title>

    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="../img/cb-logo.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="../img/cb-logo.png">
    <link rel="icon" href="../img/cb-logo.png" sizes="32x32">
    <meta name="theme-color" content="#f3f3f4">

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr notification style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/common.css" rel="stylesheet">

    <!-- Jquery & bootstrap JS -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
    $(window).load(function(){
        $("#loaderbody").fadeOut("slow");
        $("#preloader").delay(500).fadeOut();
    });
    </script>

</head>

<body class="top-navigation">
    <div id="preloader">
        <div id="loaderbody">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg dashbard-1">