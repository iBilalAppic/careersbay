/*================================================================================================
	File Name : client-service.js
	Purpose   : It contains methods for careersbay client panel
	Create By : Aavin Seth
	Created On: 27-01-2018
================================================================================================*/
/*================================================================================================
	Default initialization
================================================================================================*/

// $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });

/*================================================================================================
 No           : 1                                                                               
 Service Name : client login
 Purpose      : for login to client panel
 Create By    : Aavin Seth
 Created On   : 27-01-2018
================================================================================================*/

$("form[name='clientlogin']").validate({
	rules: {
		email: {
			required: true,
			email: true
		},
		password: "required"
	},
	messages: {
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		},
		password: "<span class='text-red'>Please provide a Password</span>"
	},
	submitHandler: function(form) {
		var email = $("#email").val();
		var password = $("#password").val();
		var captcha = grecaptcha.getResponse();
		var datapass = {"email":email,"password":password,"captcha":captcha};

		$.ajax({
			type: 'POST',
			url: 'client-service.php?servicename=clientlogin',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('welcomenotification', 'yes');
					localStorage.setItem('clientname', value.name);
					window.location.href = 'home.php';
				}
				else
				{
					grecaptcha.reset();
					var msg = value.message;
					$(".alert #msg").html('');
					$(".alert").show();
					$(".alert #msg").append(msg);
				}
			}
		});
	}
});

/*================================================================================================
	No           : 2
	Service Name : dashboard
	Purpose      : displaying resumes in datatable
	Create By    : Aavin Seth
	Created On   : 27-01-2018
================================================================================================*/

function dashboard(clientid){
	var datapass = {"clientid":clientid};

	$.ajax({
        type: 'POST',
        url: 'client-service.php?servicename=dashboard',
        datatype: 'JSON',
		contentType: 'application/json',
		async: true,
		data: JSON.stringify(datapass),
        success:function(data)
        {
            var value = JSON.parse(data);

            if(value.status == 'success')
            {
                if(value.resumes.length>0){
                    var i = new Array();
                    var sr = 1;
                    for(var val=0;val<value.resumes.length;val++)
                    {
                        i[val] = new Array();
                        i[val][0] = sr;
                        i[val][1] = value.resumes[val].name;
                        i[val][2] = value.resumes[val].isd;
                        i[val][3] = value.resumes[val].phone;
                        i[val][4] = value.resumes[val].email;
                        i[val][5] = value.resumes[val].country;
                        i[val][6] = value.resumes[val].nationality;
                        i[val][7] = value.resumes[val].jdname;
                        i[val][8] = value.resumes[val].skillsets;
                        i[val][9] = value.resumes[val].industryname;
                        i[val][10] = '<a href="../assets/resumes/'+value.resumes[val].resume+'" download="'+value.resumes[val].resume+'" class="btn btn-primary"><i class="fa fa-download"></i></a>';
                        sr++;
                    }
                }
            }

            $('#viewresumes').DataTable({
                "aaData": i,
                "bDestroy": true,
                "paging": true,
                "responsive": true,
                "lengthChange": true,
                "pageLength": 10,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "order": [[ 0, "asc" ]],
                "language": {
                    "emptyTable": "No Resumes Found"
                }
            });
        }
    });
}

/*================================================================================================
	No           : 3
	Service Name : forgetpassword
	Purpose      : forget password for client
	Create By    : Aavin Seth
	Created On   : 24-02-2018
================================================================================================*/

$("form[name='forgetpassword']").validate({
	rules: {
		email: {
			required: true,
			email: true
		}
	},
	messages: {
		email: {
			required: "<span class='text-red'>Please provide a Email</span>",
			email: "<span class='text-red'>Please provide a valid Email</span>"
		}
	},
	submitHandler: function(form) {
		$("form[name='forgetpassword'] button[type=submit]").prop("disabled", true);

		var email = $("#email").val();
		var datapass = {"email":email};

		$.ajax({
			url: 'client-service.php?servicename=forgetpassword',
			type: 'POST',
			datatype: 'JSON',
			contentType: 'application/json',
			async: true,
			data: JSON.stringify(datapass),
			success: function(data)
			{
				var value = JSON.parse(data);

				if(value.status == 'success')
				{
					localStorage.setItem('forgetpassword', 'yes');
					window.location.href = './';
				}
				else
				{
					var msg = value.message;
					$(".alert #msg").html('');
					$(".alert").show();
					$(".alert #msg").append(msg);

					$("form[name='forgetpassword'] button[type=submit]").prop("disabled", false);
				}
			}
		});
	}
});

/*================================================================================================
	Functions: on ready functions
================================================================================================*/
$(document).ready(function() {
	var clientname = localStorage.getItem("clientname");

	$('body').addClass('fixed-sidebar');
	$(".footer").addClass('fixed');
    $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });

    //Login Welcome Message
    var welcomenotification = localStorage.getItem("welcomenotification");
    if (welcomenotification == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('Welcome to CAREERSBAY', clientname);
	        localStorage.removeItem('welcomenotification');
	    }, 1300);
	}

	//forget password confirmation message
	var forgetpassword = localStorage.getItem("forgetpassword");
    if (forgetpassword == 'yes') {
	    setTimeout(function() {
	        toastr.options = {
	            closeButton: true,
	            progressBar: true,
	            positionClass: 'toast-bottom-right',
	            showMethod: 'slideDown',
	            timeOut: 4000
	        };
	        toastr.success('New Password Send Successfully!','');
	        localStorage.removeItem('forgetpassword');
	    }, 1300);
	}
});