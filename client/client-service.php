<?php
session_start();
/*================================================================================================
	File Name : client-service.php
	Purpose   : It contains methods for careersbay client panel
	Create By : Aavin Seth
	Created On: 27-01-2018
================================================================================================*/

header("Access-Control-Allow-Origin:*");

/*================================================================================================
										Database Configuration
================================================================================================*/

include('../config.php');

/*================================================================================================
	No           : 1
	Service Name : clientlogin
	Purpose      : client login code
	Create By    : Aavin Seth
	Created On   : 27-01-2018
================================================================================================*/

if($_GET['servicename'] == 'clientlogin')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$email = $json['email'];
	$password = $json['password'];
	$response = $json["captcha"];
	$cdate = date('Y-m-d H:i:s');

	// Google reCaptcha verify
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$privatekey = '6Lc6tDwUAAAAAGWSZAKejwGpvGTAtr3eKAoPAa-P';
	$verifyurl = $url."?secret=".$privatekey."&response=".$response."&remoteip=".$_SERVER['REMOTE_ADDR'];
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $verifyurl
	));
	$resp = curl_exec($curl);
	curl_close($curl);
	$data = json_decode($resp);

	$jsonarray = array();

	if(isset($data->success) AND $data->success==true)
	{
		$quead = "SELECT * FROM client WHERE email='$email' AND accessed='1'";
		$excad = db($db,$quead);
		if(mysqli_num_rows($excad) > 0)
		{
			$rowad = mysqli_fetch_assoc($excad);

			if($rowad['password'] == $password)
			{
				$jsonarray['clientid'] = $rowad['clientid'];
				$jsonarray['email'] = $rowad['email'];
				$jsonarray['name'] = $rowad['clientname'];
				$jsonarray['profilepic'] = $rowad['profilepic'];
				$_SESSION['clientprofile'] = $jsonarray;
				$jsonarray['status'] = 'success';
				$jsonarray['message'] = 'Login Successfully';
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['message'] = 'Email ID/Password does not match!';
			}	
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Email ID/Password does not match!';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Please Verify Captcha';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 2
	Service Name : dashboard
	Purpose      : display resume in datatable
	Create By    : Aavin Seth
	Created On   : 27-01-2018
================================================================================================*/

if($_GET['servicename'] == 'dashboard')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$clientid = $json['clientid'];

	$que = "SELECT resumeshortlistid,resumeid FROM resumeshortlist WHERE clientid='$clientid' AND isfinal='1' AND isdelete='0' ORDER BY resumeshortlistid DESC";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Resume Data Available';
		$jsonarray['resumes'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$resumeid = $row['resumeid'];

			$que3 = "SELECT * FROM resumes WHERE resumeid='$resumeid'";
			$exc3 = db($db,$que3);
			if(mysqli_num_rows($exc3) > 0)
			{
				while($row3 = mysqli_fetch_assoc($exc3))
				{
					$ary = array();

					$jdid = $row3['jdid'];
					$skillsets = explode(',',$row3['skillsetsid']);
					$skillset = '';

					$que1 = "SELECT jdname FROM jd WHERE jdid='$jdid'";
					$exc1 = db($db,$que1);
					$row1 = mysqli_fetch_assoc($exc1);

					$ary['resumeshortlistid'] = $row['resumeshortlistid'];
					$ary['resumeid'] = $row3['resumeid'];
					$ary['name'] = ucwords($row3['name']);
					$ary['email'] = $row3['email'];
					$ary['isd'] = $row3['isdcode'];
					$ary['phone'] = $row3['phone'];
					$ary['country'] = $row3['country'];
					$ary['nationality'] = ucfirst($row3['nationality']);
					$ary['resume'] = $row3['resume'];
					$ary['jdname'] = $row1['jdname'];
					foreach ($skillsets as $key => $value) {
						$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
						$exc2 = db($db,$que2);
						$row2 = mysqli_fetch_assoc($exc2);

						$skillset = $skillset.' '.$row2['skillsetsname'].',';
					}
					$ary['skillsets'] = rtrim($skillset,',');
					$industryid = $row3['industryid'];
					if($industryid!='0')
					{
						$que4 = "SELECT industryname FROM industry WHERE industryid='$industryid'";
						$exc4 = db($db,$que4);
						$row4 = mysqli_fetch_assoc($exc4);
						$ary['industryname'] = $row4['industryname'];
					} else {
						$ary['industryname'] = 'NA';
					}

					array_push($jsonarray['resumes'], $ary);
				}
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['message'] = 'No Resume Found';
			}
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Resume Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 3
	Service Name : forgetpassword
	Purpose      : forgetpassword
	Create By    : Aavin seth
	Created On   : 24-02-2018
================================================================================================*/

if($_GET['servicename'] == 'forgetpassword')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$email = $json['email'];
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = substr(str_shuffle($chars),0,8);
	// $hashpassword = md5($password);
	$subject = 'Password Change Alert';

	$jsonarray = array();

	$quead = "SELECT * FROM client WHERE email='$email' AND accessed='1'";
	$excad = db($db,$quead);
	if(mysqli_num_rows($excad) > 0)
	{
		$rowad = mysqli_fetch_assoc($excad);
		if($rowad['email'] == $email)
		{
			$clientname = $rowad['clientname'];
			$clientid = $rowad['clientid'];
			$queadu = "UPDATE client SET password='$password' WHERE clientid='$clientid'";
			$excadu = db($db,$queadu);

			$content = '<!DOCTYPE html>
				<html>
				<body>
					<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
						<tr>
							<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
								<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
									<tr>
										<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
											<a href="http://careersbay.com/"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>
										</td>
									</tr>
									<tr>
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">
														'.$subject.'<br>
														<span style="font-size: 12px;">Please find your login credentials for Careers Bay</span>
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">
														<hr size="1" color="#eeeff0">
													</td>
												</tr>
												<tr>
													<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Email:</strong> '.$email.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong>Password:</strong> '.$password.'
													</td>
												</tr>
												<tr>
													<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">
														<strong><a href="'.$sitelink.'client/">Click Here</a></strong> to login or copy paste '.$sitelink.'client/ in browser.
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
										<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
											<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
												<tr>
													<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
														<b>CAREERSBAY</b>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</body>
			</html>';

			include('../sendmail.php');
			sendMail($content,$email,$clientname,$subject,'','','');

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Password sent to Register Email Id';
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Email Id does not match!';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Email Id does not match!';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}
?>