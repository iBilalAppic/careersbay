<?php include('client-header.php'); ?>
            <!-- body header start -->
            <!-- Datatable -->
            <link href="../css/plugins/dataTables/datatables.min.css" rel="stylesheet">
            <style>
            .white-bg .navbar-static-top {background: transparent;}
            .top-navigation .nav > li > a, .top-navigation .nav > li a:hover, .top-navigation .nav > li a:focus {padding: 15px 20px;color: #fff;background: transparent;}
            </style>
            <!-- body header end -->

            <!-- content starts here -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8 col-md-8 col-lg-10">
                    <h2>
                        <span>
                            <?php 
                            if($clientimg!=''){ ?>
                            <img src="../assets/images/profilepic/<?php echo $clientimg; ?>" class="img-circle" height="48" width="48"/>
                            <?php } else { ?>
                            <img src="../assets/images/default-profile.png" class="img-circle"/>
                            <?php }?>
                        </span>
                        <?php echo $clientname; ?>
                    </h2>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-2" style="margin-top: 20px;">
                    <nav class="navbar navbar-static-top">
                        <ul class="nav navbar-right">   
                            <li>
                                <a href="logout.php">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="viewresumes">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Applicant Name</th>
                                                <th>ISD</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Country</th>
                                                <th>Nationality</th>
                                                <th>Job Title</th>
                                                <th>Skill Sets</th>
                                                <th>Industry</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content end here -->

            <!-- Scripts Starts -->
            <!-- Datatable -->
            <script src="../js/plugins/dataTables/datatables.min.js"></script>
            <script>
            $(document).ready(function() {
                document.title = 'Dashboard';
                $('#dashboard-nav').addClass('active');
                dashboard(<?php echo $clientid; ?>);
            });
            </script>
            <!-- Scripts End -->

<?php include('client-footer.php'); ?>