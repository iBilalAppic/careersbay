<?php
session_start();
if((isset($_SESSION['clientprofile']) && $_SESSION['clientprofile']!=''))
{
    header('location:home.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Login | CAREERSBAY</title>

        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="../img/cb-logo.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="../img/cb-logo.png">
        <link rel="icon" href="../img/cb-logo.png" sizes="32x32">
        <meta name="theme-color" content="#f3f3f4">

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr notification style -->
        <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/common.css" rel="stylesheet">
        <style>
            img {
                width: 45%;
                height: 45%;
            }
        </style>
    </head>

    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div class="m-b-md">
                    <img alt="image" class="" src="../img/careersbay-logo.jpg">
                </div>
                <h3 class="careersbay-text">CAREERSBAY</h3>
                <!-- <p></p> -->
                <p class="careersbay-text">Login in. To see it in action.</p>
                <form class="m-t" role="form" name="clientlogin">
                    <div class="alert alert-danger" style="display:none;">
                        <span id="msg"></span>
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Email Id" id="email" name="email" class="form-control" required="yes" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" id="password" name="password" class="form-control" required="yes">
                    </div>
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6Lc6tDwUAAAAAICe2a1r158VVCd60pYjxXrNCGVI"></div>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    <a href="forgotpassword.php"><small>Forgot password?</small></a>
                </form>
            </div>
        </div>

        <!-- Jquery & bootstrap JS -->
        <script src="../js/jquery-3.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <!-- Mainly scripts -->
        <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Toastr -->
        <script src="../js/plugins/toastr/toastr.min.js"></script>

        <!-- Jquery Validation -->
        <script src="../js/plugins/validate/jquery.validate.min.js"></script>

        <!-- Custom Js -->
        <script src="client-service.js"></script>
        <!-- Google ReCaptcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>
