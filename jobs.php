﻿<?php
include ('config.php');
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url = explode('=', $actual_link);
$title = $url[1];
$jobpost = str_replace("-"," ",$title);
$cdate = date('Y-m-d');
$que = "SELECT * FROM jobpost WHERE jobpostname='$jobpost' AND status='1' AND expiredate>'$cdate'";
$exc = db($db,$que);
if(mysqli_num_rows($exc) > 0)
{
    $row = mysqli_fetch_assoc($exc);

    $clientid = $row['clientid'];

    $que1 = "SELECT profilepic FROM client WHERE clientid='$clientid'";
    $exc1 = db($db,$que1);
    $row1 = mysqli_fetch_assoc($exc1);

    $url = 'https://careersbay.com/jobs.php?title='.$title;
    $jobpostname = $title2 = $row['jobpostname'];
    $profilepic = 'https://careersbay.com/assets/images/profilepic/'.$row1['profilepic'];
}
else
{
    $url = '';
    $jobpostname = '';
    $profilepic = '';
    $title2 = 'Jobs';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Site made with alo Website Builder v4.5.1, https://alo.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="alo v4.5.1, alo.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta property="og:url" content="<?= $url; ?>" />
        <meta property="og:image" content="<?= $profilepic; ?>" />
        <meta property="og:title" content="<?= $jobpostname; ?>" />
        <title><?= $title2; ?></title>

        <!-- <link rel="stylesheet" href="/careersbay/assets/web/assets/alo-icons-bold/alo-icons-bold.css">
        <link rel="stylesheet" href="/careersbay/assets/web/assets/alo-icons/alo-icons.css">
        <link rel="stylesheet" href="/careersbay/assets/icons-mind/style.css">
        <link rel="stylesheet" href="/careersbay/assets/tether/tether.min.css">
        <link rel="stylesheet" href="/careersbay/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/careersbay/assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="/careersbay/assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="/careersbay/assets/socicon/css/styles.css">
        <link rel="stylesheet" href="/careersbay/assets/dropdown/css/style.css">
        <link rel="stylesheet" href="/careersbay/assets/theme/css/style.css">
        <link rel="stylesheet" href="/careersbay/assets/alo/css/mbr-additional.css" type="text/css"> -->
        <link rel="stylesheet" href="assets/web/assets/alo-icons-bold/alo-icons-bold.css">
        <link rel="stylesheet" href="assets/web/assets/alo-icons/alo-icons.css">
        <link rel="stylesheet" href="assets/icons-mind/style.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/alo/css/mbr-additional.css" type="text/css">
        <link href="css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
        <link href="css/plugins/chosenImage/chosenImage.css" rel="stylesheet">
        <link href="css/plugins/summernote/summernote.css" rel="stylesheet">
        <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
        <style type="text/css">
        /* Sticky footer styles */
        /*html {position: relative;min-height: 100%;}*/
        /*#footer2-w {position: absolute;bottom: 0;width: 100%;}*/

        .validation-error { color: red; display: block;font-size: 12px;}
        .jobs-info { border-bottom: 1px solid #f0f1f2; display: inline-block; margin: 0 0 30px 0; padding: 0 0 30px 0; width: 100%; }
        .jobs-info .cs-media { float: left; margin: 0 25px 0 0; padding-bottom: 20px;}
        .jobs-info .cs-media figure { border: 1px solid #e4e4e4; border-left-color: #f1f1f1; border-radius: 2px; border-right-color: #f1f1f1; width: 140px; }
        .jobs-info .cs-media figure img { width: 100%; }
        .jobs-info ul { margin: 0; }
        .jobs-info .post-options li:first-child { display: block; margin: 0 0 8px 0; }
        .jobs-info .post-options i { background-color: #cfd4db; }
        .jobs-info .cs-text { display: inline-block; vertical-align: middle; }
        .jobs-info .post-options li { margin: 0 20px 0 0; position: relative; }
        .jobs-info .post-options li:after { background-color: #e4e4e4; border-radius: 100%; content: ""; height: 3px; left: -12px; position: absolute; top: 10px; width: 3px; }
        .jobs-info .post-options li:nth-child(1):after,
        .jobs-info .post-options li:nth-child(2):after { display: none; }
        .jobs-info strong { color: #4e8ef3; display: block; font-size: 18px; font-weight: 400; line-height: 15px; margin: 0 0 10px; }
        .jobs-info a { outline: none; text-decoration: none; }
        .jobs-info ul { margin: 0 0 30px; padding: 0; }
        .jobs-info ul li { display: inline-block; font-size:12px; list-style: none; vertical-align: middle; }
        .jobs-info .post-options li { color: #616161; line-height: normal; margin: 0 10px 0 0; }
        .jobs-info .post-options li a { color: #616161; }
        .jobs-info .post-options span { color: #666; }
        .jobs-info .social-media { display: inline-block;padding: 0;text-align: right;vertical-align: middle;width: 60%; }
        .jobs-info .social-media ul { margin: 0;padding: 0;display: inline-block;vertical-align: middle; }
        .jobs-info .social-media li { display: inline-block;font-size: 18px;list-style: none;margin: 0 0 0 0 !important;padding: 0px !important; }
        .jobs-info .social-media li a#facebookshare { color: #2b4a8b;}
        .jobs-info .social-media li a#twittershare { color: #1f94d9;}
        .jobs-info .social-media li a#googleshare { color: #d34836;}
        .jobs-info .social-media li a#linkedinshare { color: #0077B5;}

        fieldset {border: medium none !important;margin: 0 0 10px;min-width: 100%;padding: 0;width: 100%;}
        #contact {background: #fcfcfc;padding: 15px;margin: 10px 0;border: 1px solid #f0f0f0;}
        #contact input[type="text"],#contact input[type="email"],#contact textarea,#contact input[type="file"],#contact button[type="submit"] {font: 400 12px/16px "Roboto", Helvetica, Arial, sans-serif;}
        #contact input[type="text"],#contact input[type="email"],#contact textarea,#contact input[type="file"] {width: 100%;background: #FFF;margin: 0 0 5px;padding: 10px;border: 1px solid #dcdce0;}
        #contact input[type="text"]:hover,#contact input[type="email"]:hover,#contact textarea:hover,#contact input[type="file"]:hover {  -webkit-transition: border-color 0.3s ease-in-out;  -moz-transition: border-color 0.3s ease-in-out;  transition: border-color 0.3s ease-in-out;  border: 1px solid #dcdce0;}
        #contact textarea {height: 100px;max-width: 100%;resize: none;}
        #contact button[type="submit"] {cursor: pointer;width: 100%;border: none;background: #4CAF50;color: #FFF;margin: 0 0 5px;padding: 10px;font-size: 15px;}
        #contact button[type="submit"]:hover {background: #43A047;-webkit-transition: background 0.3s ease-in-out;-moz-transition: background 0.3s ease-in-out;transition: background-color 0.3s ease-in-out;}
        #contact button[type="submit"]:active {box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);}
        #contact button[type="submit"]:disabled {opacity: 0.65;cursor:not-allowed;}
        .tnc {color: #999999;display: block;font-size: 12px;text-align: center;}
        .txt {color: #999999;display: block;font-size: 12px;}
        #contact input:focus,#contact textarea:focus {outline: 0;border: 1px solid #aaa;}
        ::-webkit-input-placeholder {color: #888;}
        :-moz-placeholder {color: #888;}
        ::-moz-placeholder {color: #888;}
        :-ms-input-placeholder {color: #888;}

        #errorbox{ background: #ffffff; }

        .cid-qBTaQU73k3 #jobdescription p { text-align: left !important; }
        @media screen and (max-width: 1024px){ #contact .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;} }
        .hide{display:none!important}
        .show{display:block!important}
        .alert{border-radius: 5px;line-height: 0.5;margin-bottom: 0rem;text-align: center;}
        .alert-danger {background-color: red;}
        .uppercase{text-transform: uppercase;} .lowercase{text-transform: lowercase;} .capitalize{text-transform: capitalize;}

        .cid-qBSjIn8983 .navbar.navbar-short .navbar-logo a img {height: 3.8rem !important;}
        </style>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <!-- <base href="https://careersbay.com/" /> -->

        <section class="menu cid-qBSjIn8983" once="menu" id="menu3-r" data-rv-view="77">
            <nav class="navbar navbar-dropdown beta-menu align-items-center navbar-fixed-top navbar-toggleable-sm">
                <div class="menu-content-top">
                    <div class="container">
                        <div class="row ">
                            <div class="content-left-side">
                                <p class="mbr-fonts-style content-text mbr-black display-4">Contact Us at</p>
                            </div>
                            <div class="content-right-side" data-app-modern-menu="true">
                                <a class="content-link link mbr-black display-4" href="tel: +9714 385 1635">
                                    <span class="imind-old-telephone mbr-iconfont mbr-iconfont-btn"></span>

                                    9714 385 1635
                                </a>
                                <a class="content-link link mbr-black display-4" href="mailto:marketing@careersbay.com?Subject=Online Enquiry">
                                    <span class="imind-email mbr-iconfont mbr-iconfont-btn"></span>
                                    marketing@careersbay.com
                                </a>
                                <a class="content-link link mbr-black display-4" href="mailto:jobs@careersbay.com?Subject=Online Enquiry">
                                    <span class="mbri-letter mbr-iconfont mbr-iconfont-btn "></span>jobs@careersbay.com
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menu-bottom">

                    <div class="menu-logo">
                        <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="index.html">
                                <img src="assets/images/careersbay-340x104.jpg" alt="" title="" media-simple="true" style="height: 3.8rem;">
                            </a>
                        </span>
                        <span class="navbar-caption-wrap"><a href="https://alo.com" class="brand-link mbr-black display-5"></a></span>
                        </div>
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <div class="hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-dropdown js-float-line nav-right" data-app-modern-menu="true">
                            <li class="nav-item dropdown">
                                <a class="nav-link link mbr-black dropdown-toggle display-4" href="https://alo.com" data-toggle="dropdown-submenu" aria-expanded="false">
                                    Important Tips
                                </a>
                                <div class="dropdown-menu">
                                    <a class="mbr-black dropdown-item display-4" href="resumetips.html">Resume tips</a>
                                    <a class="mbr-black dropdown-item display-4" href="interviewtips.html" aria-expanded="true">Interview tips</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link mbr-black display-4" href="index.html#testimonials-slider1-b">
                                    Our clients
                                </a>
                            </li>
                            <li class="nav-item"><a class="nav-link link mbr-black display-4" href="index.html#features8-4">
                                    Current openings
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link link mbr-black display-4" href="aboutus.html">
                                    About Us
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>

        <section class="header3 cid-qBTaCXx8J7 mbr-parallax-background" id="header3-s" data-rv-view="79">
            <div class="mbr-overlay" style="opacity: 0.6;"></div>
            <div class="container align-center">
                <div class="row justify-content-center">
                    <div class="media-container-column mbr-white col-md-10 offset-md-1">
                        <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-2"></h3>
                        <h1 class="mbr-section-title mbr-bold mbr-fonts-style display-2" id="jobname">&nbsp;</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="features2 cid-qBTaQU73k3" id="jobbox" data-rv-view="85">
            <div class="container">
                <div class="media-container-row">
                    <div class="card p-3 col-12 col-md-12 col-lg-12">
                        <div class="card-box">
                            <div class="jobs-info">
                                <div class="cs-media">
                                    <figure>
                                        <a href="#">
                                            <img src="" alt="" title="" id="profilepic">
                                        </a>
                                    </figure>
                                </div>
                                <div class="cs-text">
                                    <strong id="clientname">&nbsp;</strong>
                                    <ul class="post-options">
                                        <li class="mbr-text mbr-fonts-style display-7" id="joblocation"><a href="#">&nbsp;</a></li>
                                        <li class="mbr-text mbr-fonts-style display-7">Post Date: <span id="postdate">&nbsp;</span></li>
                                    </ul>
                                </div>
                                <div class="social-media">
                                    <span>Share</span>
                                    <ul>
                                        <li><a title="Facebook" href="#" id="facebookshare" target="_blank"><span class="px-2 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span></a></li>
                                        <li><a title="Twitter" href="#" id="twittershare" target="_blank"><span class="px-2 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span></a></li>
                                        <li><a title="Google" href="#" id="googleshare" target="_blank"><span class="px-2 socicon-google socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span></a></li>
                                        <li><a title="Linkedin" href="#" id="linkedinshare" target="_blank"><span class="px-2 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- <div class="row ">
                                <div class="col-md-3"><img src="assets/images/profilepic/5a27d8ff86b96.png"></div>
                                <div class="col-md-9">
                                    <p class="client-job-text mbr-text mbr-fonts-style display-7" id="jobname2">Job Name</p>
                                    <p class="client-job-text mbr-text mbr-fonts-style display-7" id="clientlocation">Borivali West, Mumbai, Maharashtra, India</p>
                                    <p class="client-job-text mbr-text mbr-fonts-style display-7">Post Date: <span id="postdate">Sep 01, 1993</span></p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="media-container-row">
                    <div class="card p-3 col-12 col-md-12 col-lg-8">
                        <div class="card-box">
                            <h4 class="card-title mbr-fonts-style display-4 text-left"><strong>JOB OVERVIEW</strong></h4>
                            <div class="mbr-fonts-style display-7" id="jobdescription"></div>
                        </div>
                    </div>
                    <div class="card p-3 col-12 col-md-12 col-lg-4">
                        <div class="card-box">
                            <h4 class="card-title mbr-fonts-style display-4 text-left"><strong>CONTACT</strong></h4>
                            <form id="contact" name="contactform">
                                <fieldset>
                                    <input placeholder="Enter your Name*" type="text" id="candidatename" name="candidatename" tabindex="1" class="capitalize" autocomplete="off" required>
                                </fieldset>
                                <fieldset>
                                    <input placeholder="Email Address*" type="email" id="email" name="email" tabindex="2" autocomplete="off" required>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Select ISD*</span>
                                    <select data-placeholder="ISD" id="isd" name="isd" tabindex="3" class="capitalize chosen-select"></select>
                                </fieldset>
                                <fieldset>
                                    <input placeholder="Phone Number*" type="text" id="phone" name="phone" tabindex="4" autocomplete="off" required>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Select Country*</span>
                                    <select data-placeholder="Select a Country" id="country" name="country" tabindex="5" class="capitalize chosen-select"></select>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Select Nationality*</span>
                                    <select data-placeholder="Select a Nationality" id="nationality" name="nationality" tabindex="6" class="chosen-select">
                                        <option value="afghan">Afghan</option>
                                        <option value="albanian">Albanian</option>
                                        <option value="algerian">Algerian</option>
                                        <option value="american">American</option>
                                        <option value="andorran">Andorran</option>
                                        <option value="angolan">Angolan</option>
                                        <option value="antiguans">Antiguans</option>
                                        <option value="argentinean">Argentinean</option>
                                        <option value="armenian">Armenian</option>
                                        <option value="australian">Australian</option>
                                        <option value="austrian">Austrian</option>
                                        <option value="azerbaijani">Azerbaijani</option>
                                        <option value="bahamian">Bahamian</option>
                                        <option value="bahraini">Bahraini</option>
                                        <option value="bangladeshi">Bangladeshi</option>
                                        <option value="barbadian">Barbadian</option>
                                        <option value="barbudans">Barbudans</option>
                                        <option value="batswana">Batswana</option>
                                        <option value="belarusian">Belarusian</option>
                                        <option value="belgian">Belgian</option>
                                        <option value="belizean">Belizean</option>
                                        <option value="beninese">Beninese</option>
                                        <option value="bhutanese">Bhutanese</option>
                                        <option value="bolivian">Bolivian</option>
                                        <option value="bosnian">Bosnian</option>
                                        <option value="brazilian">Brazilian</option>
                                        <option value="british">British</option>
                                        <option value="bruneian">Bruneian</option>
                                        <option value="bulgarian">Bulgarian</option>
                                        <option value="burkinabe">Burkinabe</option>
                                        <option value="burmese">Burmese</option>
                                        <option value="burundian">Burundian</option>
                                        <option value="cambodian">Cambodian</option>
                                        <option value="cameroonian">Cameroonian</option>
                                        <option value="canadian">Canadian</option>
                                        <option value="cape verdean">Cape Verdean</option>
                                        <option value="central african">Central African</option>
                                        <option value="chadian">Chadian</option>
                                        <option value="chilean">Chilean</option>
                                        <option value="chinese">Chinese</option>
                                        <option value="colombian">Colombian</option>
                                        <option value="comoran">Comoran</option>
                                        <option value="congolese">Congolese</option>
                                        <option value="costa rican">Costa Rican</option>
                                        <option value="croatian">Croatian</option>
                                        <option value="cuban">Cuban</option>
                                        <option value="cypriot">Cypriot</option>
                                        <option value="czech">Czech</option>
                                        <option value="danish">Danish</option>
                                        <option value="djibouti">Djibouti</option>
                                        <option value="dominican">Dominican</option>
                                        <option value="dutch">Dutch</option>
                                        <option value="east timorese">East Timorese</option>
                                        <option value="ecuadorean">Ecuadorean</option>
                                        <option value="egyptian">Egyptian</option>
                                        <option value="emirian">Emirian</option>
                                        <option value="equatorial guinean">Equatorial Guinean</option>
                                        <option value="eritrean">Eritrean</option>
                                        <option value="estonian">Estonian</option>
                                        <option value="ethiopian">Ethiopian</option>
                                        <option value="fijian">Fijian</option>
                                        <option value="filipino">Filipino</option>
                                        <option value="finnish">Finnish</option>
                                        <option value="french">French</option>
                                        <option value="gabonese">Gabonese</option>
                                        <option value="gambian">Gambian</option>
                                        <option value="georgian">Georgian</option>
                                        <option value="german">German</option>
                                        <option value="ghanaian">Ghanaian</option>
                                        <option value="greek">Greek</option>
                                        <option value="grenadian">Grenadian</option>
                                        <option value="guatemalan">Guatemalan</option>
                                        <option value="guinea-bissauan">Guinea-Bissauan</option>
                                        <option value="guinean">Guinean</option>
                                        <option value="guyanese">Guyanese</option>
                                        <option value="haitian">Haitian</option>
                                        <option value="herzegovinian">Herzegovinian</option>
                                        <option value="honduran">Honduran</option>
                                        <option value="hungarian">Hungarian</option>
                                        <option value="icelander">Icelander</option>
                                        <option value="indian">Indian</option>
                                        <option value="indonesian">Indonesian</option>
                                        <option value="iranian">Iranian</option>
                                        <option value="iraqi">Iraqi</option>
                                        <option value="irish">Irish</option>
                                        <option value="israeli">Israeli</option>
                                        <option value="italian">Italian</option>
                                        <option value="ivorian">Ivorian</option>
                                        <option value="jamaican">Jamaican</option>
                                        <option value="japanese">Japanese</option>
                                        <option value="jordanian">Jordanian</option>
                                        <option value="kazakhstani">Kazakhstani</option>
                                        <option value="kenyan">Kenyan</option>
                                        <option value="kittian and nevisian">Kittian and Nevisian</option>
                                        <option value="kuwaiti">Kuwaiti</option>
                                        <option value="kyrgyz">Kyrgyz</option>
                                        <option value="laotian">Laotian</option>
                                        <option value="latvian">Latvian</option>
                                        <option value="lebanese">Lebanese</option>
                                        <option value="liberian">Liberian</option>
                                        <option value="libyan">Libyan</option>
                                        <option value="liechtensteiner">Liechtensteiner</option>
                                        <option value="lithuanian">Lithuanian</option>
                                        <option value="luxembourger">Luxembourger</option>
                                        <option value="macedonian">Macedonian</option>
                                        <option value="malagasy">Malagasy</option>
                                        <option value="malawian">Malawian</option>
                                        <option value="malaysian">Malaysian</option>
                                        <option value="maldivan">Maldivan</option>
                                        <option value="malian">Malian</option>
                                        <option value="maltese">Maltese</option>
                                        <option value="marshallese">Marshallese</option>
                                        <option value="mauritanian">Mauritanian</option>
                                        <option value="mauritian">Mauritian</option>
                                        <option value="mexican">Mexican</option>
                                        <option value="micronesian">Micronesian</option>
                                        <option value="moldovan">Moldovan</option>
                                        <option value="monacan">Monacan</option>
                                        <option value="mongolian">Mongolian</option>
                                        <option value="moroccan">Moroccan</option>
                                        <option value="mosotho">Mosotho</option>
                                        <option value="motswana">Motswana</option>
                                        <option value="mozambican">Mozambican</option>
                                        <option value="namibian">Namibian</option>
                                        <option value="nauruan">Nauruan</option>
                                        <option value="nepalese">Nepalese</option>
                                        <option value="new zealander">New Zealander</option>
                                        <option value="ni-vanuatu">Ni-Vanuatu</option>
                                        <option value="nicaraguan">Nicaraguan</option>
                                        <option value="nigerien">Nigerien</option>
                                        <option value="north korean">North Korean</option>
                                        <option value="northern irish">Northern Irish</option>
                                        <option value="norwegian">Norwegian</option>
                                        <option value="omani">Omani</option>
                                        <option value="pakistani">Pakistani</option>
                                        <option value="palauan">Palauan</option>
                                        <option value="panamanian">Panamanian</option>
                                        <option value="papua new guinean">Papua New Guinean</option>
                                        <option value="paraguayan">Paraguayan</option>
                                        <option value="peruvian">Peruvian</option>
                                        <option value="polish">Polish</option>
                                        <option value="portuguese">Portuguese</option>
                                        <option value="qatari">Qatari</option>
                                        <option value="romanian">Romanian</option>
                                        <option value="russian">Russian</option>
                                        <option value="rwandan">Rwandan</option>
                                        <option value="saint lucian">Saint Lucian</option>
                                        <option value="salvadoran">Salvadoran</option>
                                        <option value="samoan">Samoan</option>
                                        <option value="san marinese">San Marinese</option>
                                        <option value="sao tomean">Sao Tomean</option>
                                        <option value="saudi">Saudi</option>
                                        <option value="scottish">Scottish</option>
                                        <option value="senegalese">Senegalese</option>
                                        <option value="serbian">Serbian</option>
                                        <option value="seychellois">Seychellois</option>
                                        <option value="sierra leonean">Sierra Leonean</option>
                                        <option value="singaporean">Singaporean</option>
                                        <option value="slovakian">Slovakian</option>
                                        <option value="slovenian">Slovenian</option>
                                        <option value="solomon islander">Solomon Islander</option>
                                        <option value="somali">Somali</option>
                                        <option value="south african">South African</option>
                                        <option value="south korean">South Korean</option>
                                        <option value="spanish">Spanish</option>
                                        <option value="sri lankan">Sri Lankan</option>
                                        <option value="sudanese">Sudanese</option>
                                        <option value="surinamer">Surinamer</option>
                                        <option value="swazi">Swazi</option>
                                        <option value="swedish">Swedish</option>
                                        <option value="swiss">Swiss</option>
                                        <option value="syrian">Syrian</option>
                                        <option value="taiwanese">Taiwanese</option>
                                        <option value="tajik">Tajik</option>
                                        <option value="tanzanian">Tanzanian</option>
                                        <option value="thai">Thai</option>
                                        <option value="togolese">Togolese</option>
                                        <option value="tongan">Tongan</option>
                                        <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                                        <option value="tunisian">Tunisian</option>
                                        <option value="turkish">Turkish</option>
                                        <option value="tuvaluan">Tuvaluan</option>
                                        <option value="ugandan">Ugandan</option>
                                        <option value="ukrainian">Ukrainian</option>
                                        <option value="uruguayan">Uruguayan</option>
                                        <option value="uzbekistani">Uzbekistani</option>
                                        <option value="venezuelan">Venezuelan</option>
                                        <option value="vietnamese">Vietnamese</option>
                                        <option value="welsh">Welsh</option>
                                        <option value="yemenite">Yemenite</option>
                                        <option value="zambian">Zambian</option>
                                        <option value="zimbabwean">Zimbabwean</option>
                                    </select>
                                </fieldset>
                                <!-- <fieldset>
                                    <span class="txt">Select Designation*</span>
                                    <select data-placeholder="Select a Designation" id="designation" name="designation" tabindex="6" class="capitalize chosen-select"></select>
                                </fieldset> -->
                                <fieldset>
                                    <span class="txt">Select Industry*</span>
                                    <select data-placeholder="Select a Industry" id="industry" name="industry" tabindex="7" class="capitalize chosen-select"></select>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Select Qualification*</span>
                                    <select data-placeholder="Select a Qualification" id="qualification" name="qualification" tabindex="8" class="capitalize chosen-select"></select>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Select Skill Sets*</span>
                                    <select data-placeholder="Choose a Skill Sets" id="skillsets" name="skillsets" tabindex="9" class="capitalize chosen-select" multiple></select>
                                    <label id="skillsets-error" class="hide" for="skillsets"><span class="validation-error">Please Select Skill Sets</span></label>
                                </fieldset>
                                <fieldset>
                                    <input placeholder="Enter Year of Experience*" type="text" id="yearexperience" name="yearexperience" tabindex="10" class="capitalize" autocomplete="off" required>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Paste your CV*</span>
                                    <textarea name="cv" id="cv" class="summernote"></textarea>
                                </fieldset>
                                <fieldset>
                                    <span class="txt" style="text-align:center;">OR</span>
                                </fieldset>
                                <fieldset>
                                    <span class="txt">Upload Resume*</span>
                                    <input type="file" class="form-control-file" id="resume" name="resume" placeholder="Upload Resume" tabindex="11">
                                    <label id="resume-error1" class="hide" for="resume"><span class="validation-error">Only pdf, doc &amp; docx are allowed</span></label>
                                    <input type="hidden" id="resumebasecode" name="resumebasecode">
                                    <input type="hidden" id="resumeext" name="resumeext">
                                    <input type="hidden" id="mime" name="mime">
                                </fieldset>
                                <fieldset>
                                    <div class="g-recaptcha" data-sitekey="6Lc6tDwUAAAAAICe2a1r158VVCd60pYjxXrNCGVI"></div>
                                </fieldset>
                                <fieldset>
                                    <input type="hidden" id="jdid" name="jdid">
                                    <input type="hidden" id="clientid" name="clientid">
                                    <input type="hidden" id="jobpostid" name="jobpostid">
                                    <input type="hidden" id="jdname" name="jdname">
                                    <button name="submit" type="submit" id="contact-submit">Submit</button>
                                </fieldset>
                                <div class="alert hide" role="alert">&nbsp;</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="features2 cid-qBTaQU73k3" id="errorbox" data-rv-view="85">
            <div class="container">
                <h2 class="mbr-fonts-style mb-4 align-center display-2">This page isn't <strong>Available</strong></h2>
                <h3 class="mbr-section-subtitle align-center mbr-fonts-style mb-4 display-5">The link you followed may be broken, or the page may have been removed.</h3>
            </div>
        </section>

        <section class="cid-qBTdOvTnJW" id="footer2-w" data-rv-view="88">
            <div class="container">
                <div class="mbr-section-btn mb-3 align-center">
                    <a class="btn-underline mbr-white display-7" href="disclaimer.html">Disclaimers</a>
                    <a class="btn-underline mbr-white display-7" href="privacypolicy.html">Privacy Policy</a>
                </div>
                <div class="social-list align-center social-btns mb-3">
                    <a href="https://twitter.com/careersbay" target="_blank">
                        <span class="px-2 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                    </a>
                    <a href="https://www.facebook.com/Careersbay-505175102907815/" target="_blank">
                        <span class="px-2 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                    </a>
                    <a href="https://www.linkedin.com/company/9445660/" target="_blank">
                        <span class="px-2 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                    </a>
                    <!-- <a href="https://www.youtube.com/c/alo" target="_blank">
                        
                    </a>
                    <a href="https://plus.google.com/u/0/+alo" target="_blank">
                        
                    </a>
                    <a href="https://www.behance.net/alo" target="_blank">
                        
                    </a> -->
                </div>
                <div class="copyright align-center mb-3">
                    <p class="mb-0 text-copyright mbr-fonts-style mbr-white display-7">
                        © Copyright 2017 careersbay- All Rights Reserved
                    </p>
                </div>
            </div>
        </section>

        <!-- <script src="/careersbay/assets/web/assets/jquery/jquery.min.js"></script>
        <script src="/careersbay/assets/popper/popper.min.js"></script>
        <script src="/careersbay/assets/tether/tether.min.js"></script>
        <script src="/careersbay/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/careersbay/assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="/careersbay/assets/parallax/jarallax.min.js"></script>
        <script src="/careersbay/assets/smoothscroll/smooth-scroll.js"></script>
        <script src="/careersbay/assets/dropdown/js/script.min.js"></script>
        <script src="/careersbay/assets/theme/js/script.js"></script>
        <script src="/careersbay/js/plugins/validate/jquery.validate.min.js"></script>
        <script src="/careersbay/user-service.js"></script> -->
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/script.min.js"></script>
        <script src="assets/theme/js/script.js"></script>
        <script src="js/plugins/validate/jquery.validate.min.js"></script>
        <script src="js/plugins/chosen/chosen.jquery.js"></script>
        <script src="js/plugins/chosenImage/chosenImage.jquery.js"></script>
        <script src="js/plugins/summernote/summernote.min.js"></script>
        <script src="user-service.js"></script>
        <script>
        $(document).ready(function() {
            $('.chosen-select').chosen({width: "100%"});
            $('.summernote').summernote({
                placeholder: 'Paste your CV',
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            // $('#isd').chosenImage({disable_search_threshold: 10});
            dqs();
            viewJobPost();
        });
        </script>
    </body>
</html>