<?php
// sendMail($content=html content, $email=receiver emailid, $name=receiver name, $subject, $resumeext=attachment extension, $attach=attachment in base64, $mime=attachment application/type)
function sendMail($content, $email, $name, $subject, $resumename, $attach, $mime){
	require_once "PHPMailer/PHPMailerAutoload.php";

	//PHPMailer Object
	$mail = new PHPMailer;

	// $mail->SMTPDebug = 1;
	$mail->SMTPDebug = false;
	$mail->isSMTP();
	$mail->Host = "smtp.gmail.com";
	$mail->SMTPAuth = true;
	$mail->Username = "careersbayjob@gmail.com";
	$mail->Password = "careersbayjobs";
	$mail->SMTPSecure = "tls";

	//Set TCP port to connect to 
	$mail->Port = 587;

	// Remove ssl verification
	$mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	    )
	);

	$mail->From = "careersbayjob@gmail.com";
	$mail->FromName = "CAREERSBAY";

	// Attachment
	if($attach!='')
	{
		// $data = substr($attach, strpos($attach, ","));
		// $mail->addStringAttachment(base64_decode($data), $resumename, "base64", $mime);
		$mail->addAttachment('assets/resumes/'.$resumename);
	}

	//To address and name
	$mail->addAddress($email,$name);

	//CC and BCC
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $content;

	if(!$mail->send()) 
	{
		// return "Mailer Error: " . $mail->ErrorInfo;
		return 'false';
	} 
	else 
	{
		return 'true';
	}
}
?>