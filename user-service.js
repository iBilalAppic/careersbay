/*================================================================================================
	File Name : user-service.js
	Purpose   : It contains methods for careersbay user side
	Create By : Aavin Seth
	Created On: 09-12-2017
================================================================================================*/
/*================================================================================================
	Default initialization
================================================================================================*/



/*================================================================================================
	No           : 1
	Service Name : viewJobPost
	Purpose      : view job post on jobs page
	Create By    : Aavin Seth
	Created On   : 08-12-2017
================================================================================================*/

function viewJobPost(){
    var jobpost = '';
    if (localStorage.getItem("jobpostid") === null) {
    	// For unclean URl
    	var url = window.location.search;
        url = url.split("=");
        if(url[1]!=undefined) {
            jobpost = decodeURIComponent(url[1]);
        } else {
            jobpost = '';
        }

    	// For Clean URl, do not delete below comment lines
        // var url = window.location.pathname;
        // url = url.split("/");
        // if(url[3]!=undefined) {
        //     jobpost = url[3];
        // } else {
        //     jobpost = '';
        // }
        var type = '2';
    } else {
        jobpost = localStorage.getItem("jobpostid");
        var type = '1';
    }

    var url1 = window.location.search;
    url1 = url1.split("=");
    if(url1[1]!=undefined) {
        jobposturl = url1[1];
    } else {
        jobposturl = '';
    }

    if(jobpost != '')
    {
        var datapass = {"jobpost":jobpost,"type":type};
        $.ajax({
            type: 'POST',
            url: 'user-service.php?servicename=viewjobpost',
            datatype: 'JSON',
            contentType: 'application/json',
            async: false,
            data: JSON.stringify(datapass),
            success:function(data)
            {
                var value = JSON.parse(data);

                if(value.status == 'success')
                {
                    $('#errorbox').hide();
                    $('#jobbox').show();
                    $('#jobname').text(value.jobpostname);
                    $('#profilepic').attr('src','assets/images/profilepic/'+value.profilepic);
                    $('#profilepic').attr('alt',value.clientname);
                    $('#profilepic').attr('title',value.clientname);
                    $('#clientname').text(value.clientname);
                    $('#joblocation').text(value.joblocation);
                    $('#postdate').text(value.postdate);
                    $('#jobdescription').html(value.description);
                    $('#jdid').val(value.jdid);
                    $('#clientid').val(value.clientid);
                    $('#jobpostid').val(value.jobpostid);
                    $('#jdname').val(value.jdname); 
                    $('#facebookshare').attr('href','https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=https://careersbay.com/jobs.php?title='+jobposturl+'&pubid=ra-55810ee8392537bf&ct=1&title='+value.jobpostname+'&pco=tbxnj-1.0');
                    $('#twittershare').attr('href','https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=https://careersbay.com/jobs.php?title='+jobposturl+'&screenshot=https://careersbay.com/assets/images/careersbay-340x104.jpg&pco=tbxnj-1.0');
                    $('#googleshare').attr('href','https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=https://careersbay.com/jobs.php?title='+jobposturl+'&ct=1&title='+value.jobpostname+'&pco=tbxnj-1.0');
                    $('#linkedinshare').attr('href','https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=https://careersbay.com/jobs.php?title='+jobposturl+'&pubid=ra-55810ee8392537bf&ct=1&title='+value.jobpostname+'&pco=tbxnj-1.0');
                    $("meta[property='og:image']").attr('content', 'https://careersbay.com/assets/images/profilepic/'+value.profilepic);
                    $("meta[property='og:title']").attr('content', 'Job for '+value.jobpostname);
                    $("meta[property='og:url']").attr('content', 'https://careersbay.com/jobs.html?title='+jobposturl);

                    if (localStorage.getItem("jobpostid") !== null) {
                        localStorage.removeItem('jobpostid');
                    }

                    // document.title = value.jobpostname;
                }
                else
                {
                    $('#jobbox').hide();
                    $('#jobname').text('oops!');
                    $('#errorbox').show();
                }
            }
        });
    }
    else
    {
        $('#jobbox').hide();
        $('#jobname').text('oops!');
        $('#errorbox').show();
    }
}

/*================================================================================================
	No           : 2
	Service Name : contactform
	Purpose      : contact form for candidate
	Create By    : Aavin Seth
	Created On   : 09-12-2017
================================================================================================*/

$("form[name='contactform']").validate({
    rules: {
        candidatename: {
            required: true,
            lettersonly: true
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            digits: true
            // minlength: 8,
            // maxlength: 8
        },
        country: {
            required: true,
            lettersonly: true
        },
        yearexperience: {
            required: true,
            digits: true
        }
    },
    messages: {
        candidatename: {
            required: "<span class='validation-error'>Please provide a Name</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Name</span>"
        },
        email: {
            required: "<span class='validation-error'>Please provide a Email</span>",
            email: "<span class='validation-error'>Please provide a valid Email</span>"
        },
        phone: {
            required: "<span class='validation-error'>Please provide a Mobile Number</span>",
            digits: "<span class='validation-error'>Please provide a valid Mobile Number</span>"
            // minlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>",
            // maxlength: "<span class='validation-error'>Please provide 8 digit Mobile Number</span>"
        },
        country: {
            required: "<span class='validation-error'>Please provide a Country</span>",
            lettersonly: "<span class='validation-error'>Please provide a valid Country</span>"
        },
        yearexperience: {
            required: "<span class='validation-error'>Please provide Year of Experience</span>",
            digits: "<span class='validation-error'>Please provide a valid Year of Experience</span>"
        }
    },
    submitHandler: function(form) {
        $("#contact-submit").prop("disabled", true);
        $(".alert").addClass('hide');
        $("#skillsets-error").addClass('hide');
        $(".alert").html('');
        $(".alert").removeClass('alert-danger');
        $('#contact-submit').text('Submitting...');

        var jdid = $("#jdid").val();
        var jdname = $("#jdname").val();
        var jobpostid = $("#jobpostid").val();
        var clientid = $("#clientid").val();
        var clientname = $("#clientname").text();
        var candidatename = $("#candidatename").val();
        var email = $("#email").val();
        var isd = $("#isd").val();
        var phone = $("#phone").val();
        var country = $("#country").val();
        var nationality = $("#nationality").val();
        var industry = $("#industry").val();
        var qualification = $("#qualification").val();
        var skillsets = $("#skillsets").val();
        var skillflag = 1
        if(skillsets==null)
        {
            skillflag = 0;
        }
        var yearexperience = $("#yearexperience").val();
        var cv = $("#cv").val();
        var resumes = $("#resumebasecode").val();
        if(resumes!='')
        {
            var resume = $("#resumebasecode").val();
        } else {
            var resume = $("#cv").val();
        }
        var resumeext = $("#resumeext").val();
        var mime = $("#mime").val();
        var captcha = grecaptcha.getResponse();

        if(skillflag==1)
        {
            var datapass = {"jdid":jdid,"jdname":jdname,"jobpostid":jobpostid,"clientid":clientid,"clientname":clientname,"candidatename":candidatename,"email":email,"isd":isd,"phone":phone,"country":country,"nationality":nationality,"industry":industry,"qualification":qualification,"skillsets":skillsets,"yearexperience":yearexperience,"resume":resume,"resumeext":resumeext,"mime":mime,"captcha":captcha};
            
            $.ajax({
                type: 'POST',
                url: 'user-service.php?servicename=sendapplication',
                datatype: 'JSON',
                contentType: 'application/json',
                async: true,
                data: JSON.stringify(datapass),
                success: function(data)
                {
                    var value = JSON.parse(data);

                    if(value.status == 'success')
                    {
                        $("#candidatename").val('');
                        $("#email").val('');
                        $("#phone").val('');
                        $("#txtmsg").val('');
                        $("#resume").val('');
                        $("#email").val('');
                        $('#skillsets').val('').trigger("chosen:updated");
                        $('#cv').summernote('reset');
                        $('#contact-submit').text('Submit');
                        grecaptcha.reset();
                        $(".alert").html('');
                        $(".alert").addClass('alert-success');
                        $(".alert").html(value.message);
                        $(".alert").removeClass('hide');

                        setTimeout(function() {
    					    $(".alert").addClass('hide');
    					    $(".alert").html('&nbsp;');
    					    $(".alert").removeClass('alert-success');
    					}, 10000);
                    }
                    else
                    {
                        $('#contact-submit').text('Submit');
                        var msg = value.message;
                        if(value.attachment == 'failure')
                    	{
                    		$('#resume-error1').html('<span class="validation-error">Only pdf, doc &amp; docx are allowed</span>');
                    	}
                    	else
                    	{
                    		$(".alert").html('');
    	                    $(".alert").addClass('alert-danger');
    	                    $(".alert").html(msg);
    	                    $(".alert").removeClass('hide');
                    	}
                    }
                    $("#contact-submit").prop("disabled", false);
                }
            });
        }
        else
        {
            $("#skillsets-error").removeClass('hide');
            $("#contact-submit").prop("disabled", false);
            $('#contact-submit').text('Submit');
        }
    }
});

$("#resume").change(function()
{
	var baseCode = "";
	var file = document.querySelector('#resume').files[0];
	var mime = document.querySelector('#resume').files[0].type;
	var resume = $('#resume').val();
	var ext = resume.split(".").pop();
	var reader = new FileReader();

	if(ext=='pdf' || ext=='doc' || ext=='docx')
	{
		$('#resume-error1').addClass('hide');
		reader.addEventListener("load", function ()
		{
			baseCode = reader.result;
			$('#resumebasecode').val(baseCode);
			$('#mime').val(mime);
		}, false);

		if(file){
			reader.readAsDataURL(file);
		}
		$('#resumeext').val(ext);
	} else {
		$('#resume-error1').removeClass('hide');
	}
});

/*================================================================================================
    No           : 3
    Service Name : dqs
    Purpose      : append designation, qualification and skill set
    Create By    : Aavin Seth
    Created On   : 23-01-2018
================================================================================================*/

function dqs()
{
    $.ajax({
        type: 'POST',
        url : 'user-service.php?servicename=dqs',
        datatype: 'JSON',
        contentType: 'application/json',
        async: true,
        success: function(data)
        {
            var value = JSON.parse(data);
            var designationlist;
            var qualificationlist;
            var skillsetslist;
            var industrylist;
            var countrieslist;
            var phonecodelist;

            if(value.status == 'success')
            {
                if(value.designation.length>0)
                {
                    for(var val=0;val<value.designation.length;val++)
                    {
                        designationlist += '<option value="'+value.designation[val].designationid+'">'+value.designation[val].designationname+'</option>';
                    }
                    $("#designation").append(designationlist);
                    $('#designation').trigger("chosen:updated");
                }

                if(value.qualification.length>0)
                {
                    for(var val=0;val<value.qualification.length;val++)
                    {
                        qualificationlist += '<option value="'+value.qualification[val].qualificationid+'">'+value.qualification[val].qualificationname+'</option>';
                    }
                    $("#qualification").append(qualificationlist);
                    $('#qualification').trigger("chosen:updated");
                }

                if(value.skillsets.length>0)
                {
                    for(var val=0;val<value.skillsets.length;val++)
                    {
                        skillsetslist += '<option value="'+value.skillsets[val].skillsetsid+'">'+value.skillsets[val].skillsetsname+'</option>';
                    }
                    $("#skillsets").append(skillsetslist);
                    $('#skillsets').trigger("chosen:updated");
                }

                if(value.industry.length>0)
                {
                    for(var val=0;val<value.industry.length;val++)
                    {
                        industrylist += '<option value="'+value.industry[val].industryid+'">'+value.industry[val].industryname+'</option>';
                    }
                    $("#industry").append(industrylist);
                    $('#industry').trigger("chosen:updated");
                }

                if(value.countries.length>0)
                {
                    for(var val=0;val<value.countries.length;val++)
                    {
                        countrieslist += '<option value="'+value.countries[val].countryname+'">'+value.countries[val].countryname+'</option>';
                    }
                    $("#country").append(countrieslist);
                    $('#country').trigger("chosen:updated");
                }

                if(value.countries.length>0)
                {
                    for(var val=0;val<value.countries.length;val++)
                    {
                        phonecodelist += '<option data-img-src="http://localhost/careersbay/img/flags/32/'+value.countries[val].countryname+'.png" value="'+value.countries[val].phonecode+'">'+value.countries[val].countryname+' ('+value.countries[val].phonecode+')</option>';
                    }
                    $("#isd").append(phonecodelist);
                    $('#isd').trigger("chosen:updated");
                }
            }
        }
    });
}

/*================================================================================================
	Functions: on ready functions
================================================================================================*/
$(document).ready(function() {
	
});

//Textfield validation for letters and space
jQuery.validator.addMethod("lettersonly", function(value, element) 
{
  return this.optional(element) || /^[a-z ']+$/i.test(value);
}, "Letters and spaces only");

//Textfield validation for alphanumeric and space
jQuery.validator.addMethod("alphanumeric", function(value, element) 
{
  return this.optional(element) || /^[a-z0-9 ']+$/i.test(value);
}, "Letters, Numbers and spaces only");