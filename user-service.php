<?php
session_start();
/*================================================================================================
	File Name : admin-service.php
	Purpose   : It contains methods for careersbay admin panel
	Create By : Aavin Seth
	Created On: 02-12-2017
================================================================================================*/

header("Access-Control-Allow-Origin:*");

/*================================================================================================
										Database Configuration
================================================================================================*/

include('config.php');

/*================================================================================================
	No           : 1
	Service Name : viewbanners
	Purpose      : display banners in slider
	Create By    : Aavin Seth
	Created On   : 07-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewbanners')
{
	$que = "SELECT * FROM banners";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['banners'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['image'] = $row['image'];
			$ary['message'] = $row['message'];

			array_push($jsonarray['banners'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Banners Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Banners Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 2
	Service Name : viewjob
	Purpose      : display job in datatable
	Create By    : Aavin Seth
	Created On   : 07-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewjob')
{
	$cdate = date('Y-m-d');

	$que = "SELECT * FROM jobpost WHERE status='1' AND expiredate>'$cdate'";
	$exc = db($db,$que);
	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['job'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$clientid = $row['clientid'];
			$jdid = $row['jdid'];

			$que1 = "SELECT cl.clientname, cl.profilepic, jd.description FROM client AS cl, jd AS jd WHERE cl.clientid='$clientid' AND jd.jdid='$jdid'";
			$exc1 = db($db,$que1);
			$row1 = mysqli_fetch_assoc($exc1);

			$ary['jobpostid'] = $row['jobpostid'];
			$ary['jobpostname'] = $row['jobpostname'];
			$ary['joblink'] = strtolower(str_replace(' ', '-', $row['jobpostname']));
			$ary['clientname'] = $row1['clientname'];
			$ary['profilepic'] = $row1['profilepic'];
			$description = strip_tags($row1['description']);
			$ary['description'] = substr($description, 0, 100);

			array_push($jsonarray['job'], $ary);
		}

		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job Data Available';
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'No Job Found';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 3
	Service Name : viewjobpost
	Purpose      : get job post detail in job.html
	Create By    : Aavin Seth
	Created On   : 08-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewjobpost')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$type = $json['type'];
	if($type=='1')
	{
		$jobpost = $json['jobpost'];
		$colname = 'jobpostid';
	} else {
		$jobpost = str_replace("-"," ",$json['jobpost']);
		$colname = 'jobpostname';
	}
	$cdate = date('Y-m-d');

	$jsonarray = array();

	if($jobpost!='')
	{
		$que = "SELECT * FROM jobpost WHERE $colname='$jobpost' AND status='1' AND expiredate>'$cdate'";
		$exc = db($db,$que);

		if(mysqli_num_rows($exc) > 0)
		{
			$row = mysqli_fetch_assoc($exc);

			$clientid = $row['clientid'];
			$jdid = $row['jdid'];

			$que1 = "SELECT cl.clientname, cl.profilepic, cl.address1, cl.address2, cl.city, jd.jdname, jd.description FROM client AS cl, jd AS jd WHERE cl.clientid='$clientid' AND jd.jdid='$jdid'";
			$exc1 = db($db,$que1);
			$row1 = mysqli_fetch_assoc($exc1);

			$jsonarray['status'] = 'success';
			$jsonarray['message'] = 'Job Data Available';
			$jsonarray['jobpostid'] = $row['jobpostid'];
			$jsonarray['jobpostname'] = $row['jobpostname'];
			$jsonarray['clientid'] = $clientid;
			$jsonarray['clientname'] = $row1['clientname'];
			$jsonarray['profilepic'] = $row1['profilepic'];
			$jsonarray['joblocation'] = $row1['address1'].', '.$row1['address2'].', '.$row1['city'];
			$jsonarray['jdid'] = $jdid;
			$jsonarray['jdname'] = $row1['jdname'];
			$jsonarray['description'] = $row1['description'];
			$jsonarray['postdate'] = date('M d ,Y', strtotime($row['createdate']));
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['message'] = 'Job Detail Not Available';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Job Detail Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 4
	Service Name : sendapplication
	Purpose      : send resume submitted to email
	Create By    : Aavin Seth
	Created On   : 09-12-2017
================================================================================================*/

if($_GET['servicename'] == 'sendapplication')
{
	$jsondata = file_get_contents('php://input');
	$json = json_decode($jsondata,true);

	$jdid = $json['jdid'];
	$clientid = $json['clientid'];
	$jobpostid = $json['jobpostid'];
	$jdname = $json['jdname'];
	$clientname = $json['clientname'];
	$candidatename = $json['candidatename'];
	$email = $json['email'];
	$isd = $json['isd'];
	$phone = $json['phone'];
	$country = $json['country'];
	$nationality = $json['nationality'];
	$industry = $json['industry'];
	$qualification = $json['qualification'];
	$skillsets = implode(',',$json['skillsets']);
	$yearexperience = $json['yearexperience'];
	$resume = $json['resume'];
	$resumeext = $json['resumeext'];
	$mime = $json['mime'];
	$response = $json["captcha"];
	$subject = $jdid.' / '.$jdname.' / '.$clientname;
	$cdate = date('Y-m-d H:i:s');
	$html = '';

	// Google reCaptcha verify
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$privatekey = '6Lc6tDwUAAAAAGWSZAKejwGpvGTAtr3eKAoPAa-P';
	$verifyurl = $url."?secret=".$privatekey."&response=".$response."&remoteip=".$_SERVER['REMOTE_ADDR'];
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $verifyurl
	));
	$resp = curl_exec($curl);
	curl_close($curl);
	$data = json_decode($resp);

	$jsonarray = array();
	
	if($resumeext=='pdf' || $resumeext=='doc' || $resumeext=='docx' || $resumeext=='')
	{
		if(isset($data->success) AND $data->success==true)
		{
			$quer = "SELECT * FROM resumes WHERE email='$email' AND jobpostid='$jobpostid'";
			$excr = db($db,$quer);
			if(mysqli_num_rows($excr) == 0)
			{
				$que = "SELECT email FROM client WHERE clientid='$clientid'";
				$exc = db($db,$que);
				$row = mysqli_fetch_assoc($exc);
				$clientemail = $row['email'];

				$quedsn = "SELECT industryname FROM industry WHERE industryid='".$industry."'";
				$excdsn = mysqli_query($db,$quedsn);
				$rsdsn = mysqli_fetch_assoc($excdsn);

				$queqf = "SELECT qualificationname FROM qualification WHERE qualificationid='".$qualification."'";
				$excdf = mysqli_query($db,$queqf);
				$rsqf = mysqli_fetch_assoc($excdf);
				
				if($resumeext!='')
				{
					$resumename = uniqid().".".$resumeext;
				} else {
					$resumename = uniqid().".pdf";
				}

				$quei = "INSERT INTO resumes SET name='$candidatename',email='$email',isdcode='$isd',phone='$phone',country='$country',nationality='$nationality',industryid='$industry',qualificationid='$qualification',skillsetsid='$skillsets',yearexperience='$yearexperience',resume='$resumename',jobpostid='$jobpostid',jdid='$jdid',clientid='$clientid',createdate='$cdate'";
				$exci = db($db,$quei);
				$resumeid = mysqli_insert_id($db);

				if($exci)
				{
					if($resumeext!='')
					{
						$data = $resume;
						list($type, $data) = explode(';', $data);
						list(, $data) = explode(',', $data);
						$data = base64_decode($data);
						file_put_contents('assets/resumes/'.$resumename, $data);
					} else {
						require('mpdf/mpdf/mpdf.php');
						$mpdf=new mPDF();
						$mpdf->WriteHTML($resume);
						$mpdf->Output('assets/resumes/'.$resumename,'F');
					}

					$skillsetse = explode(',',$skillsets);
					$skillset = '';
					foreach ($skillsetse as $key => $value) {
						$quei = "INSERT INTO resumesskillsets SET resumeid='$resumeid',skillsetsid='$value',createdate='$cdate'";
						$exci = db($db,$quei);

						$que2 = "SELECT skillsetsname FROM skillsets WHERE skillsetsid='$value'";
						$exc2 = db($db,$que2);
						$row2 = mysqli_fetch_assoc($exc2);

						$skillset = $skillset.' '.$row2['skillsetsname'].',';
					}

					$html .='<!DOCTYPE html>'.
						'<html>'.
						'<body>'.
							'<table align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">'.
								'<tr>'.
									'<td style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">'.
										'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
											'<tr>'.
												'<td colspan="4" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">'.
													'<a href="http://careersbay.com/"><img src="https://careersbay.com/img/careersbay-logo.jpg" style="line-height: 1;width: 250px;" alt="careersbay"></a>'.
												'</td>'.
											'</tr>'.
											'<tr>'.
												'<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">'.
													'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;" colspan="2">'
																.$subject.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 21px;" colspan="2">'.
																'<hr size="1" color="#eeeff0">'.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;" colspan="2"></td>'.
														'</tr>'.
														'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Name:</strong> '.$candidatename.
															'</td>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Email:</strong> '.$email.
															'</td>'.
														'</tr>'.
							            				'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Phone:</strong> '.$isd.' '.$phone.
															'</td>'.
							              					'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Country:</strong> '.$country.
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Industry:</strong> '.$rsdsn['industryname'].
															'</td>'.
							              					'<td style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Qualification:</strong> '.$rsqf['qualificationname'].
															'</td>'.
														'</tr>'.
														'<tr>'.
															'<td colspan="2" style="width:50%;border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:left;">'.
																'<strong>Skill Sets:</strong> '.$skillset.
															'</td>'.
														'</tr>'.
													'</table>'.
												'</td>'.
											'</tr>'.
											'<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">'.
												'<td valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">'.
													'<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">'.
														'<tr>'.
															'<td align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">'.
																'<b>CAREERSBAY</b>'.
															'</td>'.
														'</tr>'.
													'</table>'.
												'</td>'.
											'</tr>'.
										'</table>'.
									'</td>'.
								'</tr>'.
							'</table>'.
						'</body>'.
					'</html>';

					include('sendmail.php');
					$sendmail = sendMail($html,$clientemail,'CAREERSBAY',$subject,$resumename,$resume,$mime);

					// if($sendmail=='true')
					// {
						$jsonarray['status'] = 'success';
						$jsonarray['message'] = 'Application Submitted!';
						// $jsonarray['email'] = $sendmail;
					// }
					// else
					// {
					// 	$jsonarray['status'] = 'failure';
					// 	$jsonarray['attachment'] = 'success';
					// 	$jsonarray['message'] = 'Failed to Submit. Please Try again';
					// }
				}
			}
			else
			{
				$jsonarray['status'] = 'failure';
				$jsonarray['attachment'] = 'success';
				$jsonarray['message'] = 'Application already Submitted!';
			}
		}
		else
		{
			$jsonarray['status'] = 'failure';
			$jsonarray['attachment'] = 'success';
			$jsonarray['message'] = 'Please Verify Captcha';
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['attachment'] = 'failure';
		$jsonarray['message'] = 'Only pdf, doc &amp; docx are allowed';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 5
	Service Name : viewclients
	Purpose      : view clients
	Create By    : Aavin Seth
	Created On   : 13-12-2017
================================================================================================*/

if($_GET['servicename'] == 'viewclients')
{
	$jsonarray = array();

	$que = "SELECT * FROM client WHERE status='1' AND accessed='1'";
	$exc = db($db,$que);

	if(mysqli_num_rows($exc) > 0)
	{
		$jsonarray['status'] = 'success';
		$jsonarray['message'] = 'Job Data Available';
		$jsonarray['client'] = array();

		while($row = mysqli_fetch_assoc($exc))
		{
			$ary = array();

			$ary['clientid'] = $row['clientid'];
			$ary['clientname'] = $row['clientname'];
			$ary['profilepic'] = $row['profilepic'];
			$ary['website'] = $row['website'];

			array_push($jsonarray['client'], $ary);
		}
	}
	else
	{
		$jsonarray['status'] = 'failure';
		$jsonarray['message'] = 'Clients Not Available';
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}

/*================================================================================================
	No           : 6
	Service Name : dqs
	Purpose      : append designation, qualification and skill set
    Create By    : Aavin Seth
    Created On   : 23-01-2018
================================================================================================*/

if($_GET['servicename'] == 'dqs')
{
	$jsonarray['designation'] = array();
	$jsonarray['qualification'] = array();
	$jsonarray['skillsets'] = array();
	$jsonarray['industry'] = array();
	$jsonarray['countries'] = array();

	$jsonarray['status'] = 'success';
	$jsonarray['message'] = 'Data Available';

	$qued = "SELECT * FROM designation WHERE accessed='1' ORDER BY designationname ASC";
	$excd = db($db,$qued);
	if(mysqli_num_rows($excd) > 0)
	{
		while($rowd = mysqli_fetch_assoc($excd))
		{
			$aryd = array();

			$aryd['designationid'] = $rowd['designationid'];
			$aryd['designationname'] = $rowd['designationname'];

			array_push($jsonarray['designation'], $aryd);
		}
	}

	$queq = "SELECT * FROM qualification WHERE accessed='1' ORDER BY qualificationname ASC";
	$excq = db($db,$queq);
	if(mysqli_num_rows($excq) > 0)
	{
		while($rowq = mysqli_fetch_assoc($excq))
		{
			$aryq = array();

			$aryq['qualificationid'] = $rowq['qualificationid'];
			$aryq['qualificationname'] = $rowq['qualificationname'];

			array_push($jsonarray['qualification'], $aryq);
		}
	}

	$ques = "SELECT * FROM skillsets WHERE accessed='1' ORDER BY skillsetsname ASC";
	$excs = db($db,$ques);
	if(mysqli_num_rows($excs) > 0)
	{
		while($rows = mysqli_fetch_assoc($excs))
		{
			$arys = array();

			$arys['skillsetsid'] = $rows['skillsetsid'];
			$arys['skillsetsname'] = $rows['skillsetsname'];

			array_push($jsonarray['skillsets'], $arys);
		}
	}

	$quec = "SELECT * FROM countries ORDER BY countryname ASC";
	$excc = db($db,$quec);
	if(mysqli_num_rows($excc) > 0)
	{
		while($rowc = mysqli_fetch_assoc($excc))
		{
			$aryc = array();

			$aryc['countryid'] = $rowc['countryid'];
			$aryc['countryname'] = $rowc['countryname'];
			$aryc['phonecode'] = $rowc['phonecode'];

			array_push($jsonarray['countries'], $aryc);
		}
	}

	$quei = "SELECT * FROM industry WHERE accessed='1' ORDER BY industryname ASC";
	$exci = db($db,$quei);
	if(mysqli_num_rows($exci) > 0)
	{
		while($rowi = mysqli_fetch_assoc($exci))
		{
			$aryi = array();

			$aryi['industryid'] = $rowi['industryid'];
			$aryi['industryname'] = $rowi['industryname'];

			array_push($jsonarray['industry'], $aryi);
		}
	}
	print_r(json_encode($jsonarray,JSON_UNESCAPED_SLASHES));
	exit;
}
?>